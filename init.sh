#!/bin/bash

cp -rf scripts/* ../meta-semidrive/scripts/
if [ `grep -c "{BSPDIR}/meta-semidrive-x9" ../base/sd_setup.sh` -eq '1' ]; then
    echo "already added meta-customize and meta-semidrive-x9 into sd_setup.sh!"
else
    sed -i '/echo BSPDIR=$BSPDIR/i\echo "BBLAYERS += \\" \\${BSPDIR}/meta-customize \\"" >> $BUILD_DIR/conf/bblayers.conf\
echo "BBLAYERS += \\" \\${BSPDIR}/meta-semidrive-x9 \\"" >> $BUILD_DIR/conf/bblayers.conf\
' ../base/sd_setup.sh
fi

