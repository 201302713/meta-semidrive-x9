LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

SAFETYCUSTOMIZEDIR := "${THISDIR}/safety-customize-files"
SAFETYSRCDIR := "${TOPDIR}/../source/lk_safety"
do_customize() {
	bbplain "safety.bbappend will copy project mk files to ${SAFETYSRCDIR}/project"
	cp -f ${SAFETYCUSTOMIZEDIR}/project/*.mk ${SAFETYSRCDIR}/project
	bbplain "safety.bbappend will copy lcm support files to ${SAFETYSRCDIR}/exdev/lcm"
	cp -rf ${SAFETYCUSTOMIZEDIR}/lcm/* ${SAFETYSRCDIR}/exdev/lcm
}
do_customize[nostamp] += "1"
addtask do_customize before do_compile


