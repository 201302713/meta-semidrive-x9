##########################
#safety image build
##########################


#1. SOC definition
LOCAL_DIR := $(GET_LOCAL_DIR)
CHIPVERSION := x9_mid
DOMAIN := safety
TARGET := yuenki_x9m
PROJECT := yuenki

DEBUG :=0
EXECUTION_PLACE := iram

##########################
#chipdev module define
##########################
SUPPORT_WDG_SDDRV := false
SUPPORT_UART_DWDRV := true
SUPPORT_MBOX_SDDRV := true
SUPPORT_TIMER_SDDRV := true
SUPPORT_ARM_GIC_SDDRV := true
SUPPORT_SCR_SDDRV := true
SUPPORT_RSTGEN_SDDRV := true
SUPPORT_PMU_SDDRV :=  false
SUPPORT_PORT_SDDRV := true
SUPPORT_DIO_SDDRV := true
SUPPORT_SPINOR_SDDRV := true
SUPPORT_CLKGEN_SDDRV := true
SUPPORT_I2C_SDDRV := true
SUPPORT_PLL_SDDRV := true

##########################
#library define
##########################

include $(LOCAL_DIR)/common/ospi_handover.mk
