##########################
#safety image build
##########################


#1. SOC definition
LOCAL_DIR := $(GET_LOCAL_DIR)
CHIPVERSION := x9_high
TARGET := iot_x9h
PROJECT := iot

DEBUG := 0

EXECUTION_PLACE := ddr


##########################
#chipdev module define
##########################
SUPPORT_WDG_SDDRV := false
SUPPORT_UART_DWDRV := true
SUPPORT_MBOX_SDDRV := true
SUPPORT_TIMER_SDDRV := true
SUPPORT_ARM_GIC_SDDRV := true
SUPPORT_SCR_SDDRV := true
SUPPORT_RSTGEN_SDDRV := true
SUPPORT_VPU_CODAJ12_DRV :=true
SUPPORT_DISP_SDDRV := true
SUPPORT_PMU_SDDRV := false
SUPPORT_PORT_SDDRV := true
SUPPORT_DIO_SDDRV := true
SUPPORT_CLKGEN_SDDRV := true
SUPPORT_DMA_SDDRV := true
SUPPORT_SPINOR_SDDRV := true
SUPPORT_I2C_SDDRV := true
SUPPORT_CSI_SDDRV := true
SUPPORT_I2S_SDDRV := true
SUPPORT_PLL_SDDRV := true
SUPPORT_RTC_SDDRV := true
SUPPORT_PMU_SDDRV := true
SUPPORT_CAN_SDDRV := true
SUPPORT_I2S_SDDRV_2_0 := true
ENABLE_PIN_DELTA_CONFIG ?= true
SUPPORT_VDSP_SDDRV := true
SUPPORT_GPIOIRQ_SDDRV := true
##########################
#library define
##########################
SUPPORT_DCF := true
SUPPORT_3RD_RPMSG_LITE := true
SUPPORT_RPC := false

##########################
#framework define
##########################
SUPPORT_POSIX := true
SUPPORT_CAN_PROXY ?= true

SUPPORT_SEM_MONITOR := true
SUPPORT_KERNEL_MONITOR := true
SUPPORT_LVGL_GUI := true
SUPPORT_G2DLITE_SDDRV := true
SUPPORT_EARLY_APP ?= false
ENABLE_BOOT_ANIMATION ?= false
ENABLE_FASTAVM ?= false
ENABLE_CLUSTER ?= false
ENABLE_CONTROLPANEL ?= false
ENABLE_PIN_DELTA_CONFIG ?= true

include $(LOCAL_DIR)/common/safety.mk

ifeq ($(SUPPORT_DISP_SDDRV),true)
GLOBAL_DEFINES += ENABLE_SERDES=1
CONFIG_PANELS = "x9h_iot"
CONFIG_CUSTOM_LCM = "lvds_yuenki_1920x1080_lcd"
endif
