/*
* lvds_atk10_1_1280x800_lcd.c
*
* Copyright (c) 2019 Semidrive Semiconductor.
* All rights reserved.
*
* Description:
*
* Revision History:
* -----------------
* 011, 12/01/2020 BI create this file
*/
#include "sdm_panel.h"
#include "disp_panels.h"

static int panel_post_begin(void)
{
	printf("lvds_yuenki_1920x1080_lcd %s enter \n", __func__);
  
    printf("lvds_yuenki_1920x1080_lcd %s down\n", __func__);
    return 0;
}

static int panel_post_end(void)
{
    printf("lvds_yuenki_1920x1080_lcd %s enter \n", __func__);
  
    printf("lvds_yuenki_1920x1080_lcd %s down\n", __func__);

    return 0;
}

static struct display_timing timing = {
    .hactive = 1920,
    .hfront_porch = 40,
    .hback_porch = 220,
    .hsync_len = 80,
    .vactive = 1080,
    .vfront_porch = 19,
    .vback_porch = 21,
    .vsync_len = 10,

    .dsp_clk_pol = LCM_POLARITY_RISING,
    .de_pol      = LCM_POLARITY_RISING,
    .vsync_pol   = LCM_POLARITY_RISING,
    .hsync_pol   = LCM_POLARITY_RISING,

    .map_format = LVDS_MAP_FORMAT_SWPG,
};


struct sdm_panel lvds_yuenki_1920x1080_lcd = {
    .panel_name = "lvds_yuenki_1920x1080_lcd",
    .if_type = IF_TYPE_LVDS,
    .cmd_intf = IF_TYPE_NONE, // or CMD_INTF_NONE
    .cmd_data = NULL,
    .width_mm =121,
    .height_mm = 68,
    .fps = 60,
    .display_timing = &timing,
    .sis_num = 0,
    .sis = NULL,
    .rtos_screen = NULL,
	
	.panel_post_begin = panel_post_begin,
    .panel_post_end = panel_post_end,
};


