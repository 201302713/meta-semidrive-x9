LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

BAREMETALCUSTOMIZEDIR := "${THISDIR}/baremetal-customize-files"
BAREMETALSRCDIR := "${TOPDIR}/../source/BareMetal_Suite"
do_customize() {
	echo "baremetal.bbappend will copy baremetal board support files to ${BAREMETALSRCDIR}/board"
	cp -rf ${BAREMETALCUSTOMIZEDIR}/board/* ${BAREMETALSRCDIR}/board
}
do_customize[nostamp] += "1"
addtask do_customize before do_compile
