LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

FILESEXTRAPATHS_prepend := "${THISDIR}/lk-customize-files:"

LKCUSTOMIZEDIR := "${THISDIR}/lk-customize-files"
LKSRCDIR := "${TOPDIR}/../source/lk_customize"
do_customize() {
	bbplain "lk.bbappend will copy project mk files to ${LKSRCDIR}/project"
	cp -f ${LKCUSTOMIZEDIR}/project/*.mk ${LKSRCDIR}/project
	bbplain "lk.bbappend will copy chipcfg files to ${LKSRCDIR}/chipcfg"
	cp -rf ${LKCUSTOMIZEDIR}/chipcfg/* ${LKSRCDIR}/chipcfg
	bbplain "lk.bbappend will copy target files to ${LKSRCDIR}/target"
	cp -rf ${LKCUSTOMIZEDIR}/target/* ${LKSRCDIR}/target
	
	bbplain "lk.bbappend will do modify lk_customize/platform/kunlun/safety/scr_init.c for machine:${MACHAINE_NAME}!"
	if [ "${MACHAINE_NAME}" = "x9m_yuenki" ]; then
		if [ `grep -c 'rval = (rval & (~0x38)) | (0x4 << 3);'  ${LKSRCDIR}/platform/kunlun/safety/scr_init.c` -eq '1' ]; then
			bbnote "scr_init.c already changed to 100M!"
		else
			bbnote "will do modify for lk_customize/platform/kunlun/safety/scr_init.c change to 100M!"
			sed -i '/rval = readl(SCR_SEC_BASE + (0x614 << 10))/{n;s/0x8;/(0x4 << 3);/}' ${LKSRCDIR}/platform/kunlun/safety/scr_init.c
		fi
	elif [ "${MACHAINE_NAME}" = "x9h_iot" ]; then
		bbnote "will resume lk_customize/platform/kunlun/safety/scr_init.c, originally on rgmii 1000M!"
		cd ${LKSRCDIR}/platform && git checkout -- kunlun/safety/scr_init.c
	fi
}
do_customize[nostamp] += "1"
addtask do_customize before do_compile


