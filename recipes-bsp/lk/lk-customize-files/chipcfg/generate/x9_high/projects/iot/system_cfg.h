//*****************************************************************************
//
// system_cfg.h - Prototypes for the Watchdog resource capability
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//
//*****************************************************************************

#ifndef __RES_CAPABILITY_H__
#define __RES_CAPABILITY_H__
#include "chip_res.h"
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define watchdog_res_capability_def {.version = 0x10000, \
                                                            .res_category = "watchdog",   \
                                                            .res_max_num = 8, \
                                                            .res_cap[0].res_glb_idx = RES_WATCHDOG_WDT1,  \
                                                            .res_cap[0].enableIntReset = true, \
                                                            .res_cap[0].enableExtReset = true, \
                                                            .res_cap[1].res_glb_idx = RES_WATCHDOG_WDT2,  \
                                                            .res_cap[1].enableIntReset = true, \
                                                            .res_cap[1].enableExtReset = false, \
                                                            .res_cap[2].res_glb_idx = RES_WATCHDOG_WDT3,  \
                                                            .res_cap[2].enableIntReset = true, \
                                                            .res_cap[2].enableExtReset = true, \
                                                            .res_cap[3].res_glb_idx = RES_WATCHDOG_WDT4,  \
                                                            .res_cap[3].enableIntReset = true, \
                                                            .res_cap[3].enableExtReset = true, \
                                                            .res_cap[4].res_glb_idx = RES_WATCHDOG_WDT5,  \
                                                            .res_cap[4].enableIntReset = true, \
                                                            .res_cap[4].enableExtReset = true, \
                                                            .res_cap[5].res_glb_idx = RES_WATCHDOG_WDT6,  \
                                                            .res_cap[5].enableIntReset = true, \
                                                            .res_cap[5].enableExtReset = true, \
                                                            .res_cap[6].res_glb_idx = RES_WATCHDOG_WDT7,  \
                                                            .res_cap[6].enableIntReset = true, \
                                                            .res_cap[6].enableExtReset = false, \
                                                            .res_cap[7].res_glb_idx = RES_WATCHDOG_WDT8,  \
                                                            .res_cap[7].enableIntReset = false, \
                                                            .res_cap[7].enableExtReset = false, \
                                                            }

#define rstgen_res_capability_def {.version = 0x10000, \
                                                        .res_category = "rstgen", \
                                                        .res_max_num = 2, \
                                                        .res_cap[0].res_glb_idx = RES_GLOBAL_RST_SEC_RST_EN, \
                                                        .res_cap[0].glb_self_rst_en = false, \
                                                        .res_cap[0].glb_sem_rst_en = false, \
                                                        .res_cap[0].glb_dbg_rst_en = false, \
                                                        .res_cap[0].glb_wdg1_rst_en = false, \
                                                        .res_cap[0].glb_wdg2_rst_en = false, \
                                                        .res_cap[0].glb_wdg3_rst_en = false, \
                                                        .res_cap[0].glb_wdg4_rst_en = false, \
                                                        .res_cap[0].glb_wdg5_rst_en = false, \
                                                        .res_cap[0].glb_wdg6_rst_en = false, \
                                                        .res_cap[0].glb_wdg7_rst_en = false, \
                                                        .res_cap[0].glb_wdg8_rst_en = false, \
                                                        .res_cap[1].res_glb_idx = RES_GLOBAL_RST_SAF_RST_EN, \
                                                        .res_cap[1].glb_self_rst_en = true, \
                                                        .res_cap[1].glb_sem_rst_en = true, \
                                                        .res_cap[1].glb_dbg_rst_en = true, \
                                                        .res_cap[1].glb_wdg1_rst_en = false, \
                                                        .res_cap[1].glb_wdg2_rst_en = false, \
                                                        .res_cap[1].glb_wdg3_rst_en = false, \
                                                        .res_cap[1].glb_wdg4_rst_en = false, \
                                                        .res_cap[1].glb_wdg5_rst_en = false, \
                                                        .res_cap[1].glb_wdg6_rst_en = false, \
                                                        .res_cap[1].glb_wdg7_rst_en = false, \
                                                        .res_cap[1].glb_wdg8_rst_en = false, \
                                                        }
#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif