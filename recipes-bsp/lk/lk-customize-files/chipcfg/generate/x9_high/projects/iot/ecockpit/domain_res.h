
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef _DOMAIN_RES_H
#define _DOMAIN_RES_H
#include "domain_res_cnt.h"

/* Generated domain resource declarations.
 * DO NOT MODIFY!
 */

// domain res define from Resource Allocation sheet
const domain_res_t g_iram_res =
{
	.res_id[0] = RES_IRAM_IRAM4,
	.res_id[1] = RES_IRAM_IRAM4_ECC,
	.res_id[2] = RES_IRAM_IRAM3,
	.res_id[3] = RES_IRAM_IRAM3_ECC,
	.res_id[4] = RES_IRAM_IRAM2,
	.res_id[5] = RES_IRAM_IRAM2_ECC,
	.res_num = IRAM_RES_NUM,
 };

const domain_res_t g_mb_res =
{
	.res_id[0] = RES_MB_MB_MEM,
	.res_num = MB_RES_NUM,
 };

const domain_res_t g_gpu_res =
{
	.res_id[0] = RES_GPU_GPU2,
	.res_id[1] = RES_GPU_GPU1,
	.res_num = GPU_RES_NUM,
 };

const domain_res_t g_ddr_res =
{
	.res_id[0] = RES_DDR_DDR_MEM_SYS_CFG,
	.res_id[1] = RES_DDR_DDR_MEM_VBMETA,
	.res_id[2] = RES_DDR_DDR_MEM_DIL_IMAGES,
	.res_id[3] = RES_DDR_DDR_MEM_VDSP,
	.res_id[4] = RES_DDR_DDR_MEM_VDSP_SHARE,
	.res_id[5] = RES_DDR_DDR_MEM_SERVICES,
	.res_id[6] = RES_DDR_DDR_MEM_SAF_ECO,
	.res_id[7] = RES_DDR_DDR_MEM_SEC_ECO,
	.res_id[8] = RES_DDR_DDR_MEM_ECO_ATF,
	.res_id[9] = RES_DDR_DDR_MEM_ECO_TEE,
	.res_id[10] = RES_DDR_DDR_MEM_ECO_HYP,
	.res_id[11] = RES_DDR_DDR_MEM_ECO_DEV,
	.res_id[12] = RES_DDR_DDR_MEM_ECO_REE,
	.res_id[13] = RES_DDR_DDR_MEM_CLU,
	.res_num = DDR_RES_NUM,
 };

const domain_res_t g_pcie_res =
{
	.res_id[0] = RES_PCIE_PCIE2,
	.res_id[1] = RES_PCIE_PCIE1,
	.res_id[2] = RES_PCIE_PCIE2_8,
	.res_id[3] = RES_PCIE_PCIE2_7,
	.res_id[4] = RES_PCIE_PCIE2_6,
	.res_id[5] = RES_PCIE_PCIE2_5,
	.res_id[6] = RES_PCIE_PCIE2_4,
	.res_id[7] = RES_PCIE_PCIE2_3,
	.res_id[8] = RES_PCIE_PCIE2_2,
	.res_id[9] = RES_PCIE_PCIE2_1,
	.res_id[10] = RES_PCIE_PCIE1_8,
	.res_id[11] = RES_PCIE_PCIE1_7,
	.res_id[12] = RES_PCIE_PCIE1_6,
	.res_id[13] = RES_PCIE_PCIE1_5,
	.res_id[14] = RES_PCIE_PCIE1_4,
	.res_id[15] = RES_PCIE_PCIE1_3,
	.res_id[16] = RES_PCIE_PCIE1_2,
	.res_id[17] = RES_PCIE_PCIE1_1,
	.res_num = PCIE_RES_NUM,
 };

const domain_res_t g_gic_res =
{
	.res_id[0] = RES_GIC_GIC4,
	.res_num = GIC_RES_NUM,
 };

const domain_res_t g_ospi_res =
{
	.res_id[0] = RES_OSPI_OSPI2,
	.res_num = OSPI_RES_NUM,
 };

const domain_res_t g_ce_mem_res =
{
	.res_id[0] = RES_CE_MEM_CE2_VCE2,
	.res_id[1] = RES_CE_MEM_CE2_VCE3,
	.res_id[2] = RES_CE_MEM_CE2_VCE4,
	.res_id[3] = RES_CE_MEM_CE2_VCE5,
	.res_num = CE_MEM_RES_NUM,
 };

const domain_res_t g_vdsp_res =
{
	.res_id[0] = RES_VDSP_VDSP,
	.res_num = VDSP_RES_NUM,
 };

const domain_res_t g_dma_res =
{
	.res_id[0] = RES_DMA_DMA8,
	.res_id[1] = RES_DMA_DMA7,
	.res_id[2] = RES_DMA_DMA6,
	.res_id[3] = RES_DMA_DMA5,
	.res_id[4] = RES_DMA_DMA4,
	.res_id[5] = RES_DMA_DMA3,
	.res_num = DMA_RES_NUM,
 };

const domain_res_t g_mshc_res =
{
	.res_id[0] = RES_MSHC_SD4,
	.res_id[1] = RES_MSHC_SD3,
	.res_id[2] = RES_MSHC_SD2,
	.res_id[3] = RES_MSHC_SD1,
	.res_num = MSHC_RES_NUM,
 };

const domain_res_t g_spi_res =
{
	.res_id[0] = RES_SPI_SPI8,
	.res_id[1] = RES_SPI_SPI7,
	.res_id[2] = RES_SPI_SPI6,
	.res_num = SPI_RES_NUM,
 };

const domain_res_t g_ce_reg_res =
{
	.res_id[0] = RES_CE_REG_CE2_VCE2,
	.res_id[1] = RES_CE_REG_CE2_VCE3,
	.res_id[2] = RES_CE_REG_CE2_VCE4,
	.res_id[3] = RES_CE_REG_CE2_VCE5,
	.res_num = CE_REG_RES_NUM,
 };

const domain_res_t g_i2s_sc_res =
{
	.res_id[0] = RES_I2S_SC_I2S_SC8,
	.res_id[1] = RES_I2S_SC_I2S_SC7,
	.res_id[2] = RES_I2S_SC_I2S_SC6,
	.res_id[3] = RES_I2S_SC_I2S_SC5,
	.res_id[4] = RES_I2S_SC_I2S_SC4,
	.res_id[5] = RES_I2S_SC_I2S_SC3,
	.res_num = I2S_SC_RES_NUM,
 };

const domain_res_t g_pvt_sens_res =
{
	.res_id[0] = RES_PVT_SENS_PVT_SENS_SEC,
	.res_num = PVT_SENS_RES_NUM,
 };

const domain_res_t g_pll_res =
{
	.res_id[0] = RES_PLL_PLL7,
	.res_id[1] = RES_PLL_PLL6,
	.res_id[2] = RES_PLL_PLL_LVDS4,
	.res_id[3] = RES_PLL_PLL_CPU1B,
	.res_id[4] = RES_PLL_PLL_CPU1A,
	.res_id[5] = RES_PLL_PLL_GPU2,
	.res_id[6] = RES_PLL_PLL_GPU1,
	.res_num = PLL_RES_NUM,
 };

const domain_res_t g_iram_reg_res =
{
	.res_id[0] = RES_IRAM_REG_IRAM4,
	.res_id[1] = RES_IRAM_REG_IRAM3,
	.res_id[2] = RES_IRAM_REG_IRAM2,
	.res_num = IRAM_REG_RES_NUM,
 };

const domain_res_t g_dma_mux_res =
{
	.res_id[0] = RES_DMA_MUX_DMA_MUX8,
	.res_id[1] = RES_DMA_MUX_DMA_MUX7,
	.res_id[2] = RES_DMA_MUX_DMA_MUX6,
	.res_id[3] = RES_DMA_MUX_DMA_MUX5,
	.res_id[4] = RES_DMA_MUX_DMA_MUX4,
	.res_id[5] = RES_DMA_MUX_DMA_MUX3,
	.res_num = DMA_MUX_RES_NUM,
 };

const domain_res_t g_watchdog_res =
{
	.res_id[0] = RES_WATCHDOG_WDT2,
	.res_id[1] = RES_WATCHDOG_WDT5,
	.res_num = WATCHDOG_RES_NUM,
 };

const domain_res_t g_pwm_res =
{
	.res_id[0] = RES_PWM_PWM8,
	.res_id[1] = RES_PWM_PWM7,
	.res_id[2] = RES_PWM_PWM6,
	.res_num = PWM_RES_NUM,
 };

const domain_res_t g_timer_res =
{
	.res_id[0] = RES_TIMER_TIMER8,
	.res_id[1] = RES_TIMER_TIMER6,
	.res_id[2] = RES_TIMER_TIMER5,
	.res_id[3] = RES_TIMER_TIMER4,
	.res_num = TIMER_RES_NUM,
 };

const domain_res_t g_enet_qos_res =
{
	.res_id[0] = RES_ENET_QOS_ENET_QOS2,
	.res_num = ENET_QOS_RES_NUM,
 };

const domain_res_t g_uart_res =
{
	.res_id[0] = RES_UART_UART16,
	.res_id[1] = RES_UART_UART15,
	.res_id[2] = RES_UART_UART14,
	.res_id[3] = RES_UART_UART13,
	.res_id[4] = RES_UART_UART12,
	.res_id[5] = RES_UART_UART11,
	.res_id[6] = RES_UART_UART10,
	.res_id[7] = RES_UART_UART9,
	.res_num = UART_RES_NUM,
 };

const domain_res_t g_i2c_res =
{
	.res_id[0] = RES_I2C_I2C16,
	.res_id[1] = RES_I2C_I2C15,
	.res_id[2] = RES_I2C_I2C14,
	.res_id[3] = RES_I2C_I2C13,
	.res_id[4] = RES_I2C_I2C12,
	.res_id[5] = RES_I2C_I2C11,
	.res_id[6] = RES_I2C_I2C10,
	.res_id[7] = RES_I2C_I2C9,
	.res_id[8] = RES_I2C_I2C8,
	.res_id[9] = RES_I2C_I2C7,
	.res_id[10] = RES_I2C_I2C6,
	.res_id[11] = RES_I2C_I2C5,
	.res_num = I2C_RES_NUM,
 };

const domain_res_t g_ospi_reg_res =
{
	.res_id[0] = RES_OSPI_REG_OSPI2,
	.res_num = OSPI_REG_RES_NUM,
 };

const domain_res_t g_gpio_res =
{
	.res_id[0] = RES_GPIO_GPIO4,
	.res_num = GPIO_RES_NUM,
 };

const domain_res_t g_i2s_mc_res =
{
	.res_id[0] = RES_I2S_MC_I2S_MC2,
	.res_id[1] = RES_I2S_MC_I2S_MC1,
	.res_num = I2S_MC_RES_NUM,
 };

const domain_res_t g_spdif_res =
{
	.res_id[0] = RES_SPDIF_SPDIF4,
	.res_id[1] = RES_SPDIF_SPDIF3,
	.res_id[2] = RES_SPDIF_SPDIF2,
	.res_id[3] = RES_SPDIF_SPDIF1,
	.res_num = SPDIF_RES_NUM,
 };

const domain_res_t g_dc_res =
{
	.res_id[0] = RES_DC_DC5,
	.res_id[1] = RES_DC_DC4,
	.res_id[2] = RES_DC_DC3,
	.res_id[3] = RES_DC_DC2,
	.res_id[4] = RES_DC_DC1,
	.res_num = DC_RES_NUM,
 };

const domain_res_t g_dp_res =
{
	.res_id[0] = RES_DP_DP3,
	.res_id[1] = RES_DP_DP2,
	.res_id[2] = RES_DP_DP1,
	.res_num = DP_RES_NUM,
 };

const domain_res_t g_mipi_csi_res =
{
	.res_id[0] = RES_MIPI_CSI_MIPI_CSI2,
	.res_id[1] = RES_MIPI_CSI_MIPI_CSI1,
	.res_num = MIPI_CSI_RES_NUM,
 };

const domain_res_t g_csi_res =
{
	.res_id[0] = RES_CSI_CSI3,
	.res_id[1] = RES_CSI_CSI2,
	.res_id[2] = RES_CSI_CSI1,
	.res_num = CSI_RES_NUM,
 };

const domain_res_t g_g2d_res =
{
	.res_id[0] = RES_G2D_G2D1,
	.res_num = G2D_RES_NUM,
 };

const domain_res_t g_lvds_res =
{
	.res_id[0] = RES_LVDS_LVDS3,
	.res_num = LVDS_RES_NUM,
 };

const domain_res_t g_dma_ch_mux_pcie_res =
{
	.res_id[0] = RES_DMA_CH_MUX_PCIE_DMA_CH_MUX_PCIE2,
	.res_id[1] = RES_DMA_CH_MUX_PCIE_DMA_CH_MUX_PCIE1,
	.res_num = DMA_CH_MUX_PCIE_RES_NUM,
 };

const domain_res_t g_usbphy_res =
{
	.res_id[0] = RES_USBPHY_USBPHY2,
	.res_id[1] = RES_USBPHY_USBPHY1,
	.res_num = USBPHY_RES_NUM,
 };

const domain_res_t g_usb_res =
{
	.res_id[0] = RES_USB_USB2,
	.res_id[1] = RES_USB_USB1,
	.res_num = USB_RES_NUM,
 };

const domain_res_t g_pcie_phy_res =
{
	.res_id[0] = RES_PCIE_PHY_PCIE_PHY,
	.res_num = PCIE_PHY_RES_NUM,
 };

const domain_res_t g_vpu_res =
{
	.res_id[0] = RES_VPU_VPU1,
	.res_id[1] = RES_VPU_VPU2,
	.res_num = VPU_RES_NUM,
 };

const domain_res_t g_mjpeg_res =
{
	.res_id[0] = RES_MJPEG_MJPEG,
	.res_num = MJPEG_RES_NUM,
 };

const domain_res_t g_sys_cnt_res =
{
	.res_id[0] = RES_SYS_CNT_SYS_CNT_RO,
	.res_num = SYS_CNT_RES_NUM,
 };

const domain_res_t g_rtc_res =
{
	.res_id[0] = RES_RTC_RTC2,
	.res_num = RTC_RES_NUM,
 };

const domain_res_t g_ftbu_res =
{
	.res_id[0] = RES_FTBU_FTBU8_4,
	.res_id[1] = RES_FTBU_FTBU8_3,
	.res_id[2] = RES_FTBU_FTBU8_2,
	.res_id[3] = RES_FTBU_FTBU7_4,
	.res_id[4] = RES_FTBU_FTBU7_3,
	.res_id[5] = RES_FTBU_FTBU7_2,
	.res_id[6] = RES_FTBU_FTBU6_2,
	.res_num = FTBU_RES_NUM,
 };

const domain_res_t g_scr4k_ssid_res =
{
	.res_id[0] = RES_SCR4K_SSID_SCR4K_SSID,
	.res_num = SCR4K_SSID_RES_NUM,
 };

const domain_res_t g_scr4k_sid_res =
{
	.res_id[0] = RES_SCR4K_SID_SCR4K_SID,
	.res_num = SCR4K_SID_RES_NUM,
 };

const domain_res_t g_smmu_tcu_res =
{
	.res_id[0] = RES_SMMU_TCU_SMMU_TCU,
	.res_num = SMMU_TCU_RES_NUM,
 };

const domain_res_t g_pcie_reg_res =
{
	.res_id[0] = RES_PCIE_REG_PCIE2_REG,
	.res_id[1] = RES_PCIE_REG_PCIE1_REG,
	.res_num = PCIE_REG_RES_NUM,
 };

const domain_res_t g_pcie_io_res =
{
	.res_id[0] = RES_PCIE_IO_PCIE_IO2,
	.res_id[1] = RES_PCIE_IO_PCIE_IO1,
	.res_num = PCIE_IO_RES_NUM,
 };


// domain res define from Rpc sheet
const domain_res_t g_iomuxc_res =
{
	.res_id[0] = RES_INPUT_SELECT_SEC_I2S_MC1_SDI3_SDO3,
	.res_id[1] = RES_INPUT_SELECT_SEC_I2S_MC1_SDI2_SDO2,
	.res_id[2] = RES_INPUT_SELECT_SEC_I2S_MC1_SDI1_SDO1,
	.res_id[3] = RES_INPUT_SELECT_SEC_I2S_MC1_SDI0_SDO0,
	.res_id[4] = RES_INPUT_SELECT_SEC_I2S_MC1_WSI,
	.res_id[5] = RES_INPUT_SELECT_SEC_I2S_MC1_SCKI,
	.res_id[6] = RES_INPUT_SELECT_SEC_I2S_MC1_SDI7_SDO7,
	.res_id[7] = RES_INPUT_SELECT_SEC_I2S_SC7_SDI_SDO,
	.res_id[8] = RES_INPUT_SELECT_SEC_I2S_MC1_SDI6_SDO6,
	.res_id[9] = RES_INPUT_SELECT_SEC_I2S_SC8_WS,
	.res_id[10] = RES_INPUT_SELECT_SEC_I2S_MC1_SDI5_SDO5,
	.res_id[11] = RES_INPUT_SELECT_SEC_I2S_SC8_SCK,
	.res_id[12] = RES_INPUT_SELECT_SEC_I2S_MC1_SDI4_SDO4,
	.res_id[13] = RES_INPUT_SELECT_SEC_I2S_SC8_SDI_SDO,
	.res_id[14] = RES_INPUT_SELECT_SEC_I2S_MC1_WSO,
	.res_id[15] = RES_INPUT_SELECT_SEC_I2S_SC7_WS,
	.res_id[16] = RES_INPUT_SELECT_SEC_I2S_MC1_SCKO,
	.res_id[17] = RES_INPUT_SELECT_SEC_I2S_SC7_SCK,
	.res_id[18] = RES_INPUT_SELECT_SEC_I2S_SC5_SDI_SDO,
	.res_id[19] = RES_INPUT_SELECT_SEC_I2S_SC6_SDI_SDO,
	.res_id[20] = RES_INPUT_SELECT_SEC_I2S_SC4_SDI_SDO,
	.res_id[21] = RES_INPUT_SELECT_SEC_I2S_SC4_WS,
	.res_id[22] = RES_INPUT_SELECT_SEC_I2S_SC4_SCK,
	.res_id[23] = RES_INPUT_SELECT_SEC_I2S_SC3_SDI_SDO,
	.res_id[24] = RES_INPUT_SELECT_SEC_I2S_SC3_WS,
	.res_id[25] = RES_INPUT_SELECT_SEC_I2S_SC3_SCK,
	.res_id[26] = RES_INPUT_SELECT_SEC_I2S_MC2_SCKI,
	.res_id[27] = RES_INPUT_SELECT_SEC_I2S_MC2_WSI,
	.res_id[28] = RES_INPUT_SELECT_SEC_OSPI2_DQS,
	.res_id[29] = RES_INPUT_SELECT_SEC_I2S_MC2_SDI7_SDO7,
	.res_id[30] = RES_INPUT_SELECT_SEC_I2S_MC2_SDI6_SDO6,
	.res_id[31] = RES_INPUT_SELECT_SEC_I2S_MC2_SDI5_SDO5,
	.res_id[32] = RES_INPUT_SELECT_SEC_I2S_MC2_SDI4_SDO4,
	.res_id[33] = RES_INPUT_SELECT_SEC_I2S_MC2_SDI3_SDO3,
	.res_id[34] = RES_INPUT_SELECT_SEC_I2S_MC2_SDI2_SDO2,
	.res_id[35] = RES_INPUT_SELECT_SEC_I2S_MC2_SDI1_SDO1,
	.res_id[36] = RES_INPUT_SELECT_SEC_I2S_MC2_SDI0_SDO0,
	.res_id[37] = RES_INPUT_SELECT_SEC_I2S_MC2_WSO,
	.res_id[38] = RES_INPUT_SELECT_SEC_I2S_MC2_SCKO,
	.res_id[39] = RES_INPUT_SELECT_SEC_I2C16_SDA,
	.res_id[40] = RES_INPUT_SELECT_SEC_I2C16_SCL,
	.res_id[41] = RES_INPUT_SELECT_SEC_I2C15_SDA,
	.res_id[42] = RES_INPUT_SELECT_SEC_I2C15_SCL,
	.res_id[43] = RES_INPUT_SELECT_SEC_TIMER6_CH3,
	.res_id[44] = RES_INPUT_SELECT_SEC_UART16_RX,
	.res_id[45] = RES_INPUT_SELECT_SEC_SPI7_SS,
	.res_id[46] = RES_INPUT_SELECT_SEC_TIMER6_CH2,
	.res_id[47] = RES_INPUT_SELECT_SEC_SPI7_MOSI,
	.res_id[48] = RES_INPUT_SELECT_SEC_UART16_CTS,
	.res_id[49] = RES_INPUT_SELECT_SEC_UART15_RX,
	.res_id[50] = RES_INPUT_SELECT_SEC_SPI7_MISO,
	.res_id[51] = RES_INPUT_SELECT_SEC_SPI7_SCLK,
	.res_id[52] = RES_INPUT_SELECT_SEC_I2C12_SDA,
	.res_id[53] = RES_INPUT_SELECT_SEC_UART14_RX,
	.res_id[54] = RES_INPUT_SELECT_SEC_SPDIF4_IN,
	.res_id[55] = RES_INPUT_SELECT_SEC_I2C12_SCL,
	.res_id[56] = RES_INPUT_SELECT_SEC_UART14_CTS,
	.res_id[57] = RES_INPUT_SELECT_SEC_UART13_RX,
	.res_id[58] = RES_INPUT_SELECT_SEC_SPI8_SS,
	.res_id[59] = RES_INPUT_SELECT_SEC_SPDIF3_IN,
	.res_id[60] = RES_INPUT_SELECT_SEC_SPI8_MOSI,
	.res_id[61] = RES_INPUT_SELECT_SEC_SPI8_MISO,
	.res_id[62] = RES_INPUT_SELECT_SEC_SPI8_SCLK,
	.res_id[63] = RES_INPUT_SELECT_SEC_PCIE_SS_PCIEX2_WAKE_N,
	.res_id[64] = RES_INPUT_SELECT_SEC_CANFD8_RX,
	.res_id[65] = RES_INPUT_SELECT_SEC_I2C14_SDA,
	.res_id[66] = RES_INPUT_SELECT_SEC_PCIE_SS_PCIEX2_PERST_N,
	.res_id[67] = RES_INPUT_SELECT_SEC_I2C14_SCL,
	.res_id[68] = RES_INPUT_SELECT_SEC_PCIE_SS_PCIEX1_WAKE_N,
	.res_id[69] = RES_INPUT_SELECT_SEC_CANFD7_RX,
	.res_id[70] = RES_INPUT_SELECT_SEC_I2C13_SDA,
	.res_id[71] = RES_INPUT_SELECT_SEC_PCIE_SS_PCIEX1_PERST_N,
	.res_id[72] = RES_INPUT_SELECT_SEC_I2C13_SCL,
	.res_id[73] = RES_INPUT_SELECT_SEC_UART12_RX,
	.res_id[74] = RES_INPUT_SELECT_SEC_SPI5_SS,
	.res_id[75] = RES_INPUT_SELECT_SEC_SPI5_MOSI,
	.res_id[76] = RES_INPUT_SELECT_SEC_UART11_RX,
	.res_id[77] = RES_INPUT_SELECT_SEC_SPI5_MISO,
	.res_id[78] = RES_INPUT_SELECT_SEC_SPI5_SCLK,
	.res_id[79] = RES_INPUT_SELECT_SEC_I2C8_SDA,
	.res_id[80] = RES_INPUT_SELECT_SEC_MSHC3_VOLT_SW,
	.res_id[81] = RES_INPUT_SELECT_SEC_ENET2_MDIO,
	.res_id[82] = RES_INPUT_SELECT_SEC_I2C8_SCL,
	.res_id[83] = RES_INPUT_SELECT_SEC_PCIE_SS_PCIEX1_CLKREQ_N,
	.res_id[84] = RES_INPUT_SELECT_SEC_MSHC3_WP,
	.res_id[85] = RES_INPUT_SELECT_SEC_I2C7_SDA,
	.res_id[86] = RES_INPUT_SELECT_SEC_PCIE_SS_PCIEX2_CLKREQ_N,
	.res_id[87] = RES_INPUT_SELECT_SEC_MSHC3_CARD_DET_N,
	.res_id[88] = RES_INPUT_SELECT_SEC_I2C7_SCL,
	.res_id[89] = RES_INPUT_SELECT_SEC_USB_SS_USB2_OC,
	.res_id[90] = RES_INPUT_SELECT_SEC_CANFD6_RX,
	.res_id[91] = RES_INPUT_SELECT_SEC_SPI6_SS,
	.res_id[92] = RES_INPUT_SELECT_SEC_I2C6_SDA,
	.res_id[93] = RES_INPUT_SELECT_SEC_SPI6_MOSI,
	.res_id[94] = RES_INPUT_SELECT_SEC_I2C6_SCL,
	.res_id[95] = RES_INPUT_SELECT_SEC_USB_SS_USB1_OC,
	.res_id[96] = RES_INPUT_SELECT_SEC_MSHC4_WP,
	.res_id[97] = RES_INPUT_SELECT_SEC_CANFD5_RX,
	.res_id[98] = RES_INPUT_SELECT_SEC_SPI6_MISO,
	.res_id[99] = RES_INPUT_SELECT_SEC_I2C5_SDA,
	.res_id[100] = RES_INPUT_SELECT_SEC_MSHC4_CARD_DET_N,
	.res_id[101] = RES_INPUT_SELECT_SEC_SPI6_SCLK,
	.res_id[102] = RES_INPUT_SELECT_SEC_I2C5_SCL,
	.res_id[103] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO107,
	.res_id[104] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO106,
	.res_id[105] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO105,
	.res_id[106] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO104,
	.res_id[107] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO103,
	.res_id[108] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO102,
	.res_id[109] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO101,
	.res_id[110] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO100,
	.res_id[111] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO99,
	.res_id[112] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO98,
	.res_id[113] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO97,
	.res_id[114] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO96,
	.res_id[115] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO95,
	.res_id[116] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO94,
	.res_id[117] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO93,
	.res_id[118] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO92,
	.res_id[119] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO91,
	.res_id[120] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO90,
	.res_id[121] = RES_MUX_CONTROL_SEC_I2C5_SDA,
	.res_id[122] = RES_MUX_CONTROL_SEC_I2C5_SCL,
	.res_id[123] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO87,
	.res_id[124] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO86,
	.res_id[125] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO85,
	.res_id[126] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO84,
	.res_id[127] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO107,
	.res_id[128] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO106,
	.res_id[129] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO105,
	.res_id[130] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO104,
	.res_id[131] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO103,
	.res_id[132] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO102,
	.res_id[133] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO101,
	.res_id[134] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO100,
	.res_id[135] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO99,
	.res_id[136] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO98,
	.res_id[137] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO97,
	.res_id[138] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO96,
	.res_id[139] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO95,
	.res_id[140] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO94,
	.res_id[141] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO93,
	.res_id[142] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO92,
	.res_id[143] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO91,
	.res_id[144] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO90,
	.res_id[145] = RES_PAD_CONTROL_SEC_I2C5_SDA,
	.res_id[146] = RES_PAD_CONTROL_SEC_I2C5_SCL,
	.res_id[147] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO87,
	.res_id[148] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO86,
	.res_id[149] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO85,
	.res_id[150] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO84,
	.res_id[151] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO83,
	.res_id[152] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO82,
	.res_id[153] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO81,
	.res_id[154] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO80,
	.res_id[155] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO79,
	.res_id[156] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO78,
	.res_id[157] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO77,
	.res_id[158] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO76,
	.res_id[159] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO75,
	.res_id[160] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO74,
	.res_id[161] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO73,
	.res_id[162] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO72,
	.res_id[163] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO71,
	.res_id[164] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO70,
	.res_id[165] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO69,
	.res_id[166] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO68,
	.res_id[167] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO67,
	.res_id[168] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO66,
	.res_id[169] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO65,
	.res_id[170] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO64,
	.res_id[171] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO63,
	.res_id[172] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO62,
	.res_id[173] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO61,
	.res_id[174] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO60,
	.res_id[175] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO59,
	.res_id[176] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO58,
	.res_id[177] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO57,
	.res_id[178] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO56,
	.res_id[179] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO55,
	.res_id[180] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO54,
	.res_id[181] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO53,
	.res_id[182] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO52,
	.res_id[183] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO51,
	.res_id[184] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO50,
	.res_id[185] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO49,
	.res_id[186] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO48,
	.res_id[187] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO47,
	.res_id[188] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO46,
	.res_id[189] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO45,
	.res_id[190] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO44,
	.res_id[191] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO43,
	.res_id[192] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO42,
	.res_id[193] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO41,
	.res_id[194] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO40,
	.res_id[195] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO39,
	.res_id[196] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO38,
	.res_id[197] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO37,
	.res_id[198] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO36,
	.res_id[199] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO35,
	.res_id[200] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO34,
	.res_id[201] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO33,
	.res_id[202] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO32,
	.res_id[203] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO31,
	.res_id[204] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO30,
	.res_id[205] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO29,
	.res_id[206] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO28,
	.res_id[207] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO27,
	.res_id[208] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO25,
	.res_id[209] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO24,
	.res_id[210] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO23,
	.res_id[211] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO22,
	.res_id[212] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO21,
	.res_id[213] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO20,
	.res_id[214] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO19,
	.res_id[215] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO18,
	.res_id[216] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO17,
	.res_id[217] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO16,
	.res_id[218] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO15,
	.res_id[219] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO14,
	.res_id[220] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO13,
	.res_id[221] = RES_PAD_CONTROL_SEC_GPIO_MUX2_IO12,
	.res_id[222] = RES_PAD_CONTROL_SEC_UART10_RX,
	.res_id[223] = RES_PAD_CONTROL_SEC_UART10_TX,
	.res_num = IOMUXC_RES_NUM,
 };

const domain_res_t g_rstgen_res =
{
	.res_id[0] = RES_GLOBAL_RST_SEC_RST_EN,
	.res_id[1] = RES_MODULE_RST_SEC_GPU2_SS,
	.res_id[2] = RES_MODULE_RST_SEC_GPU2_CORE,
	.res_id[3] = RES_MODULE_RST_SEC_GPU1_SS,
	.res_id[4] = RES_MODULE_RST_SEC_GPU1_CORE,
	.res_id[5] = RES_MODULE_RST_SEC_G2D2,
	.res_id[6] = RES_MODULE_RST_SEC_G2D1,
	.res_id[7] = RES_MODULE_RST_SEC_CSI1,
	.res_id[8] = RES_MODULE_RST_SEC_DP3,
	.res_id[9] = RES_MODULE_RST_SEC_DP2,
	.res_id[10] = RES_MODULE_RST_SEC_DP1,
	.res_id[11] = RES_MODULE_RST_SEC_MIPI_CSI2,
	.res_id[12] = RES_MODULE_RST_SEC_MIPI_CSI1,
	.res_id[13] = RES_MODULE_RST_SEC_USB2,
	.res_id[14] = RES_MODULE_RST_SEC_USB1,
	.res_id[15] = RES_MODULE_RST_SEC_PCIEPHY,
	.res_id[16] = RES_MODULE_RST_SEC_PCIE1,
	.res_id[17] = RES_MODULE_RST_SEC_PCIE2,
	.res_id[18] = RES_MODULE_RST_SEC_CPU1_SS,
	.res_id[19] = RES_MODULE_RST_SEC_VPU2,
	.res_id[20] = RES_MODULE_RST_SEC_VPU1,
	.res_id[21] = RES_MODULE_RST_SEC_VDSP_DRESET,
	.res_id[22] = RES_MODULE_RST_SEC_GIC5,
	.res_id[23] = RES_MODULE_RST_SEC_GIC4,
	.res_id[24] = RES_MODULE_RST_SEC_CPU1_SCU_WARM,
	.res_id[25] = RES_MODULE_RST_SEC_CPU1_CORE5_WARM,
	.res_id[26] = RES_MODULE_RST_SEC_CPU1_CORE4_WARM,
	.res_id[27] = RES_MODULE_RST_SEC_CPU1_CORE3_WARM,
	.res_id[28] = RES_MODULE_RST_SEC_CPU1_CORE2_WARM,
	.res_id[29] = RES_MODULE_RST_SEC_CPU1_CORE1_WARM,
	.res_id[30] = RES_MODULE_RST_SEC_CPU1_CORE0_WARM,
	.res_id[31] = RES_MODULE_RST_SEC_MSHC4,
	.res_id[32] = RES_MODULE_RST_SEC_MSHC3,
	.res_id[33] = RES_MODULE_RST_SEC_MSHC2,
	.res_id[34] = RES_MODULE_RST_SEC_MSHC1,
	.res_id[35] = RES_MODULE_RST_SEC_ENET2,
	.res_id[36] = RES_MODULE_RST_SEC_I2S_MC2,
	.res_id[37] = RES_MODULE_RST_SEC_I2S_MC1,
	.res_id[38] = RES_MODULE_RST_SEC_I2S_SC8,
	.res_id[39] = RES_MODULE_RST_SEC_I2S_SC7,
	.res_id[40] = RES_MODULE_RST_SEC_I2S_SC6,
	.res_id[41] = RES_MODULE_RST_SEC_I2S_SC5,
	.res_id[42] = RES_MODULE_RST_SEC_I2S_SC4,
	.res_id[43] = RES_MODULE_RST_SEC_I2S_SC3,
	.res_id[44] = RES_MODULE_RST_SEC_OSPI2,
	.res_id[45] = RES_ISO_EN_SEC_GPU1,
	.res_id[46] = RES_ISO_EN_SEC_CPU1,
	.res_id[47] = RES_ISO_EN_SEC_USB,
	.res_id[48] = RES_ISO_EN_SEC_PCIE,
	.res_id[49] = RES_GENERAL_REG_SEC_GENERAL_REG8,
	.res_id[50] = RES_GENERAL_REG_SEC_GENERAL_REG7,
	.res_id[51] = RES_GENERAL_REG_SEC_GENERAL_REG6,
	.res_id[52] = RES_GENERAL_REG_SEC_GENERAL_REG5,
	.res_id[53] = RES_GENERAL_REG_SEC_GENERAL_REG4,
	.res_id[54] = RES_GENERAL_REG_SEC_GENERAL_REG3,
	.res_id[55] = RES_CORE_RST_SEC_CPU1_CORE_ALL_SW,
	.res_id[56] = RES_CORE_RST_SEC_CPU1_CORE_ALL_EN,
	.res_id[57] = RES_CORE_RST_SEC_VDSP_SW,
	.res_id[58] = RES_CORE_RST_SEC_VDSP_EN,
	.res_num = RSTGEN_RES_NUM,
 };

const domain_res_t g_scr_res =
{
	.res_id[0] = RES_SCR_R16W16_SEC_VDSP,
	.res_id[1] = RES_SCR_R16W16_SEC_USB2,
	.res_id[2] = RES_SCR_R16W16_SEC_USB1,
	.res_id[3] = RES_SCR_R16W16_SEC_PCIE_SS_I_PCIEX2,
	.res_id[4] = RES_SCR_R16W16_SEC_PCIE_SS_I_PCIEX1,
	.res_id[5] = RES_SCR_R16W16_SEC_GPU2,
	.res_id[6] = RES_SCR_R16W16_SEC_GPU1,
	.res_id[7] = RES_SCR_R16W16_SEC_ENTE2,
	.res_id[8] = RES_SCR_L31_SEC_CPU1_RVBARADDR5_39_30,
	.res_id[9] = RES_SCR_L31_SEC_CPU1_RVBARADDR5_29_2,
	.res_id[10] = RES_SCR_L31_SEC_CPU1_RVBARADDR4_39_30,
	.res_id[11] = RES_SCR_L31_SEC_CPU1_RVBARADDR4_29_2,
	.res_id[12] = RES_SCR_L31_SEC_VDSP_ALTRESETVEC_31_4,
	.res_id[13] = RES_SCR_L31_SEC_REMAP_OSPI2_IMAGD_2ND_UP_LIMIT_19_0,
	.res_id[14] = RES_SCR_L31_SEC_REMAP_OSPI2_IMAGD_2ND_LOW_LIMIT_19_0,
	.res_id[15] = RES_SCR_L31_SEC_REMAP_OSPI2_IMAGD_2ND_OFFSET_19_0,
	.res_id[16] = RES_SCR_L31_SEC_CPU1_RVBARADDR3_39_30,
	.res_id[17] = RES_SCR_L31_SEC_CPU1_RVBARADDR3_29_2,
	.res_id[18] = RES_SCR_L31_SEC_CPU1_RVBARADDR2_39_30,
	.res_id[19] = RES_SCR_L31_SEC_CPU1_RVBARADDR2_29_2,
	.res_id[20] = RES_SCR_L31_SEC_CPU1_RVBARADDR1_39_30,
	.res_id[21] = RES_SCR_L31_SEC_CPU1_RVBARADDR1_29_2,
	.res_id[22] = RES_SCR_L31_SEC_CPU1_RVBARADDR0_39_30,
	.res_id[23] = RES_SCR_L31_SEC_CPU1_RVBARADDR0_29_2,
	.res_id[24] = RES_SCR_L16_SEC_GPIO_MUX2_GPIO_SEL_107_96,
	.res_id[25] = RES_SCR_L16_SEC_GPIO_MUX2_GPIO_SEL_95_80,
	.res_id[26] = RES_SCR_L16_SEC_GPIO_MUX2_GPIO_SEL_79_64,
	.res_id[27] = RES_SCR_L16_SEC_GPIO_MUX2_GPIO_SEL_63_48,
	.res_id[28] = RES_SCR_L16_SEC_GPIO_MUX2_GPIO_SEL_47_32,
	.res_id[29] = RES_SCR_L16_SEC_GPIO_MUX2_GPIO_SEL_31_16,
	.res_id[30] = RES_SCR_L16_SEC_GPIO_MUX2_GPIO_SEL_15_0,
	.res_id[31] = RES_SCR_RW_SEC_IO_AP_PREFGEN_VDDIO,
	.res_id[32] = RES_SCR_RW_SEC_ENET2_PPS_STRETCH_CFG,
	.res_id[33] = RES_SCR_RW_SEC_VPU2_I_LINEBUFFER_CFG,
	.res_id[34] = RES_SCR_RW_SEC_TIMER8_LP_MODE,
	.res_id[35] = RES_SCR_RW_SEC_TIMER6_LP_MODE,
	.res_id[36] = RES_SCR_RW_SEC_TIMER5_LP_MODE,
	.res_id[37] = RES_SCR_RW_SEC_TIMER4_LP_MODE,
	.res_id[38] = RES_SCR_RW_SEC_MSHC4,
	.res_id[39] = RES_SCR_RW_SEC_MSHC3,
	.res_id[40] = RES_SCR_RW_SEC_MSHC2,
	.res_id[41] = RES_SCR_RW_SEC_MSHC1,
	.res_id[42] = RES_SCR_RW_SEC_PIO_I2CSC_SDO_SDI_CTRL,
	.res_num = SCR_RES_NUM,
 };

const domain_res_t g_ckgen_res =
{
	.res_id[0] = RES_GATING_EN_SEC_WDT5,
	.res_id[1] = RES_GATING_EN_SEC_GPIO4,
	.res_id[2] = RES_GATING_EN_SEC_GPIO3,
	.res_id[3] = RES_GATING_EN_SEC_I2S_SC8,
	.res_id[4] = RES_GATING_EN_SEC_I2S_SC7,
	.res_id[5] = RES_GATING_EN_SEC_I2S_SC6,
	.res_id[6] = RES_GATING_EN_SEC_I2S_SC5,
	.res_id[7] = RES_GATING_EN_SEC_I2S_SC4,
	.res_id[8] = RES_GATING_EN_SEC_I2S_SC3,
	.res_id[9] = RES_GATING_EN_SEC_I2S_MC2,
	.res_id[10] = RES_GATING_EN_SEC_I2S_MC1,
	.res_id[11] = RES_GATING_EN_SEC_PWM8,
	.res_id[12] = RES_GATING_EN_SEC_PWM7,
	.res_id[13] = RES_GATING_EN_SEC_PWM6,
	.res_id[14] = RES_GATING_EN_SEC_PWM5,
	.res_id[15] = RES_GATING_EN_SEC_PWM4,
	.res_id[16] = RES_GATING_EN_SEC_TIMER8,
	.res_id[17] = RES_GATING_EN_SEC_TIMER6,
	.res_id[18] = RES_GATING_EN_SEC_TIMER5,
	.res_id[19] = RES_GATING_EN_SEC_TIMER4,
	.res_id[20] = RES_GATING_EN_SEC_SPDIF4,
	.res_id[21] = RES_GATING_EN_SEC_SPDIF3,
	.res_id[22] = RES_GATING_EN_SEC_SPDIF2,
	.res_id[23] = RES_GATING_EN_SEC_SPDIF1,
	.res_id[24] = RES_GATING_EN_SEC_UART14,
	.res_id[25] = RES_GATING_EN_SEC_UART12,
	.res_id[26] = RES_GATING_EN_SEC_UART10,
	.res_id[27] = RES_GATING_EN_SEC_SPI8,
	.res_id[28] = RES_GATING_EN_SEC_SPI7,
	.res_id[29] = RES_GATING_EN_SEC_SPI6,
	.res_id[30] = RES_GATING_EN_SEC_I2C16,
	.res_id[31] = RES_GATING_EN_SEC_I2C15,
	.res_id[32] = RES_GATING_EN_SEC_I2C14,
	.res_id[33] = RES_GATING_EN_SEC_I2C13,
	.res_id[34] = RES_GATING_EN_SEC_I2C12,
	.res_id[35] = RES_GATING_EN_SEC_I2C11,
	.res_id[36] = RES_GATING_EN_SEC_I2C10,
	.res_id[37] = RES_GATING_EN_SEC_I2C9,
	.res_id[38] = RES_GATING_EN_SEC_I2C8,
	.res_id[39] = RES_GATING_EN_SEC_I2C7,
	.res_id[40] = RES_GATING_EN_SEC_I2C6,
	.res_id[41] = RES_GATING_EN_SEC_I2C5,
	.res_id[42] = RES_GATING_EN_SEC_IRAM4,
	.res_id[43] = RES_GATING_EN_SEC_IRAM3,
	.res_id[44] = RES_GATING_EN_SEC_IRAM2,
	.res_id[45] = RES_GATING_EN_SEC_OSPI2,
	.res_id[46] = RES_GATING_EN_SEC_ENET2_TIMER_SEC,
	.res_id[47] = RES_GATING_EN_SEC_ENET2_TX,
	.res_id[48] = RES_GATING_EN_SEC_EMMC4,
	.res_id[49] = RES_GATING_EN_SEC_EMMC3,
	.res_id[50] = RES_GATING_EN_SEC_EMMC2,
	.res_id[51] = RES_GATING_EN_SEC_MSHC_TIMER,
	.res_id[52] = RES_GATING_EN_SEC_EMMC1,
	.res_id[53] = RES_IP_SLICE_SEC_GIC4_GIC5,
	.res_id[54] = RES_IP_SLICE_SEC_CSI_MCLK2,
	.res_id[55] = RES_IP_SLICE_SEC_CSI_MCLK1,
	.res_id[56] = RES_IP_SLICE_SEC_I2S_SC8,
	.res_id[57] = RES_IP_SLICE_SEC_I2S_SC7,
	.res_id[58] = RES_IP_SLICE_SEC_I2S_SC6,
	.res_id[59] = RES_IP_SLICE_SEC_I2S_SC5,
	.res_id[60] = RES_IP_SLICE_SEC_I2S_SC4,
	.res_id[61] = RES_IP_SLICE_SEC_I2S_SC3,
	.res_id[62] = RES_IP_SLICE_SEC_I2S_MC2,
	.res_id[63] = RES_IP_SLICE_SEC_I2S_MC1,
	.res_id[64] = RES_IP_SLICE_SEC_I2S_MCLK3,
	.res_id[65] = RES_IP_SLICE_SEC_I2S_MCLK2,
	.res_id[66] = RES_IP_SLICE_SEC_PWM8,
	.res_id[67] = RES_IP_SLICE_SEC_PWM7,
	.res_id[68] = RES_IP_SLICE_SEC_PWM6,
	.res_id[69] = RES_IP_SLICE_SEC_PWM5,
	.res_id[70] = RES_IP_SLICE_SEC_PWM4,
	.res_id[71] = RES_IP_SLICE_SEC_TIMER8,
	.res_id[72] = RES_IP_SLICE_SEC_TIMER6,
	.res_id[73] = RES_IP_SLICE_SEC_TIMER5,
	.res_id[74] = RES_IP_SLICE_SEC_TIMER4,
	.res_id[75] = RES_IP_SLICE_SEC_OSPI2,
	.res_id[76] = RES_IP_SLICE_SEC_SPDIF4,
	.res_id[77] = RES_IP_SLICE_SEC_SPDIF3,
	.res_id[78] = RES_IP_SLICE_SEC_SPDIF2,
	.res_id[79] = RES_IP_SLICE_SEC_SPDIF1,
	.res_id[80] = RES_IP_SLICE_SEC_ENET2_TIMER_SEC,
	.res_id[81] = RES_IP_SLICE_SEC_ENET2_PHY_REF,
	.res_id[82] = RES_IP_SLICE_SEC_ENET2_RMII,
	.res_id[83] = RES_IP_SLICE_SEC_ENET2_TX,
	.res_id[84] = RES_IP_SLICE_SEC_EMMC4,
	.res_id[85] = RES_IP_SLICE_SEC_EMMC3,
	.res_id[86] = RES_IP_SLICE_SEC_EMMC2,
	.res_id[87] = RES_IP_SLICE_SEC_EMMC1,
	.res_id[88] = RES_GATING_EN_DISP_MIPI_CSI3_PIX,
	.res_id[89] = RES_GATING_EN_DISP_MIPI_CSI2_PIX,
	.res_id[90] = RES_GATING_EN_DISP_MIPI_CSI1_PIX,
	.res_id[91] = RES_GATING_EN_DISP_MIPI_CSI3_PIX_SCI,
	.res_id[92] = RES_GATING_EN_DISP_MIPI_CSI2_PIX_CSI,
	.res_id[93] = RES_GATING_EN_DISP_MIPI_CSI1_PIX_CSI,
	.res_id[94] = RES_GATING_EN_DISP_SPARE2,
	.res_id[95] = RES_GATING_EN_DISP_SPARE1,
	.res_id[96] = RES_GATING_EN_DISP_DP3,
	.res_id[97] = RES_GATING_EN_DISP_DP2,
	.res_id[98] = RES_GATING_EN_DISP_DP1,
	.res_id[99] = RES_GATING_EN_DISP_DC5,
	.res_id[100] = RES_GATING_EN_DISP_CKGEN_DISP,
	.res_id[101] = RES_GATING_EN_DISP_XTAL_AP,
	.res_id[102] = RES_GATING_EN_DISP_IRAM5,
	.res_id[103] = RES_GATING_EN_DISP_DISP_BUS,
	.res_id[104] = RES_BUS_SLICE_DISP_DISP_BUS_GASKET,
	.res_id[105] = RES_BUS_SLICE_DISP_DISP_BUS_CTL,
	.res_id[106] = RES_IP_SLICE_DISP_EXT_AUD4,
	.res_id[107] = RES_IP_SLICE_DISP_EXT_AUD3,
	.res_id[108] = RES_IP_SLICE_DISP_EXT_AUD2,
	.res_id[109] = RES_IP_SLICE_DISP_EXT_AUD1,
	.res_id[110] = RES_IP_SLICE_DISP_SPARE2,
	.res_id[111] = RES_IP_SLICE_DISP_SPARE1,
	.res_id[112] = RES_IP_SLICE_DISP_DC5,
	.res_id[113] = RES_IP_SLICE_DISP_DP3,
	.res_id[114] = RES_IP_SLICE_DISP_DP2,
	.res_id[115] = RES_IP_SLICE_DISP_DP1,
	.res_id[116] = RES_IP_SLICE_DISP_MIPI_CSI3_PIX,
	.res_id[117] = RES_IP_SLICE_DISP_MIPI_CSI2_PIX,
	.res_id[118] = RES_IP_SLICE_DISP_MIPI_CSI1_PIX,
	.res_id[119] = RES_UUU_WRAP_SOC_HIS_BUS,
	.res_id[120] = RES_UUU_WRAP_SOC_MJPEG,
	.res_id[121] = RES_UUU_WRAP_SOC_VPU1,
	.res_id[122] = RES_UUU_WRAP_SOC_GPU2,
	.res_id[123] = RES_UUU_WRAP_SOC_GPU1,
	.res_id[124] = RES_UUU_WRAP_SOC_CPU1B,
	.res_id[125] = RES_UUU_WRAP_SOC_CPU1A,
	.res_id[126] = RES_GATING_EN_SOC_CPU1A_0_CORE_CLK5,
	.res_id[127] = RES_GATING_EN_SOC_CPU1A_0_CORE_CLK4,
	.res_id[128] = RES_GATING_EN_SOC_CPU1A_0_CORE_CLK3,
	.res_id[129] = RES_GATING_EN_SOC_CPU1A_0_CORE_CLK2,
	.res_id[130] = RES_GATING_EN_SOC_CPU1A_0_CORE_CLK1,
	.res_id[131] = RES_GATING_EN_SOC_CPU1A_0_CORE_CLK0,
	.res_id[132] = RES_GATING_EN_SOC_CPU1A_1,
	.res_id[133] = RES_GATING_EN_SOC_CPU1A_2_PCLK_ATCLK_GICCLK,
	.res_id[134] = RES_GATING_EN_SOC_CPU1B_0,
	.res_id[135] = RES_GATING_EN_SOC_HIS_BUS_3_USB2_REF_CLK,
	.res_id[136] = RES_GATING_EN_SOC_HIS_BUS_3_USB1_REF_CLK,
	.res_id[137] = RES_GATING_EN_SOC_HIS_BUS_3_USB2_PCLK,
	.res_id[138] = RES_GATING_EN_SOC_HIS_BUS_2_USB2_XM_ACLK,
	.res_id[139] = RES_GATING_EN_SOC_HIS_BUS_3_USB2_PHY_PCLK,
	.res_id[140] = RES_GATING_EN_SOC_HIS_BUS_3_USB2_CTRL_PCLK,
	.res_id[141] = RES_GATING_EN_SOC_HIS_BUS_3_USB1_PHY_PCLK,
	.res_id[142] = RES_GATING_EN_SOC_HIS_BUS_3_USB1_CTRL_PCLK,
	.res_id[143] = RES_GATING_EN_SOC_HIS_BUS_3_USB1_PCLK,
	.res_id[144] = RES_GATING_EN_SOC_HIS_BUS_2_USB1_XM_ACLK,
	.res_id[145] = RES_GATING_EN_SOC_HIS_BUS_1,
	.res_id[146] = RES_GATING_EN_SOC_HIS_BUS_3_PCIE_PHY_PCLK,
	.res_id[147] = RES_GATING_EN_SOC_HIS_BUS_3_PCIE1_PCLK,
	.res_id[148] = RES_GATING_EN_SOC_HIS_BUS_2_PCIE1_MSTR_ACLK,
	.res_id[149] = RES_GATING_EN_SOC_HIS_BUS_3_PCIE2_PLK,
	.res_id[150] = RES_GATING_EN_SOC_HIS_BUS_2_PCIE2_MSTR_ACLK,
	.res_id[151] = RES_GATING_EN_SOC_HIS_BUS_3_NOC_HIS_PERCLK,
	.res_id[152] = RES_GATING_EN_SOC_HIS_BUS_2_NOC_HIS_MAINCLK,
	.res_id[153] = RES_GATING_EN_SOC_VPU_BUS_1_PLL_VPU,
	.res_id[154] = RES_GATING_EN_SOC_VPU_BUS_1_MJPEG_PCLK,
	.res_id[155] = RES_GATING_EN_SOC_VPU_BUS_0_MJPEG_ACLK,
	.res_id[156] = RES_GATING_EN_SOC_MJPEG,
	.res_id[157] = RES_GATING_EN_SOC_VPU_BUS_1_VPU2_PCLK,
	.res_id[158] = RES_GATING_EN_SOC_VPU_BUS_0_VPU2_ACLK,
	.res_id[159] = RES_GATING_EN_SOC_VPU_BUS_1_VPU1_PCLK,
	.res_id[160] = RES_GATING_EN_SOC_VPU_BUS_0_VPU1_ACLK,
	.res_id[161] = RES_GATING_EN_SOC_VPU1,
	.res_id[162] = RES_GATING_EN_SOC_GPU2_2,
	.res_id[163] = RES_GATING_EN_SOC_GPU2_1,
	.res_id[164] = RES_GATING_EN_SOC_GPU2_0,
	.res_id[165] = RES_GATING_EN_SOC_GPU1_2,
	.res_id[166] = RES_GATING_EN_SOC_GPU1_1,
	.res_id[167] = RES_GATING_EN_SOC_GPU1_0,
	.res_id[168] = RES_GATING_EN_SOC_CPU2_PCLK_ATCLK_GICCLK,
	.res_id[169] = RES_GATING_EN_SOC_CPU2_1,
	.res_id[170] = RES_GATING_EN_SOC_CPU2_0,
	.res_id[171] = RES_GATING_EN_SOC_CPU1A_2_PLL_CPU1A_PLL_CPU1B_PCLK,
	.res_id[172] = RES_GATING_EN_SOC_GIC4,
	.res_id[173] = RES_GATING_EN_SOC_GIC4_GIC5,
	.res_id[174] = RES_GATING_EN_SOC_SCR4K_SSID,
	.res_id[175] = RES_GATING_EN_SOC_SCR4K_SID,
	.res_id[176] = RES_CORE_SLICE_SOC_GPU2,
	.res_id[177] = RES_CORE_SLICE_SOC_GPU1,
	.res_id[178] = RES_CORE_SLICE_SOC_CPU1B,
	.res_id[179] = RES_CORE_SLICE_SOC_CPU1A,
	.res_id[180] = RES_BUS_SLICE_SOC_HIS_BUS_GASKET,
	.res_id[181] = RES_BUS_SLICE_SOC_HIS_BUS_CTL,
	.res_id[182] = RES_IP_SLICE_SOC_VPU1,
	.res_num = CKGEN_RES_NUM,
 };

static const domain_res_t * g_res_cat[] = {
	&g_iram_res,
	&g_mb_res,
	&g_gpu_res,
	&g_ddr_res,
	&g_pcie_res,
	&g_gic_res,
	&g_ospi_res,
	NULL, /*romc*/
	NULL, /*platform*/
	&g_ce_mem_res,
	&g_vdsp_res,
	&g_dma_res,
	&g_mshc_res,
	NULL, /*mac*/
	NULL, /*gpv*/
	&g_spi_res,
	&g_ce_reg_res,
	NULL, /*sem*/
	&g_i2s_sc_res,
	NULL, /*bipc*/
	NULL, /*eic*/
	&g_pvt_sens_res,
	NULL, /*pwrctrl*/
	&g_pll_res,
	NULL, /*rc24m*/
	NULL, /*xtal*/
	NULL, /*romc_reg*/
	&g_iram_reg_res,
	&g_dma_mux_res,
	&g_watchdog_res,
	&g_pwm_res,
	&g_timer_res,
	&g_enet_qos_res,
	&g_uart_res,
	&g_i2c_res,
	NULL, /*can*/
	&g_ospi_reg_res,
	NULL, /*efuse*/
	&g_gpio_res,
	NULL, /*gpv_sec_m_reg*/
	&g_i2s_mc_res,
	&g_spdif_res,
	NULL, /*adc*/
	&g_dc_res,
	&g_dp_res,
	NULL, /*mipi_dsi*/
	NULL, /*disp_mux*/
	&g_mipi_csi_res,
	&g_csi_res,
	&g_g2d_res,
	&g_lvds_res,
	&g_dma_ch_mux_pcie_res,
	&g_usbphy_res,
	&g_usb_res,
	&g_pcie_phy_res,
	NULL, /*scr_hpi*/
	&g_vpu_res,
	&g_mjpeg_res,
	&g_sys_cnt_res,
	NULL, /*sec_storage*/
	NULL, /*rstgen_rtc*/
	NULL, /*tm*/
	NULL, /*iomuxc_rtc*/
	NULL, /*pmu*/
	NULL, /*rc_rtc*/
	&g_rtc_res,
	&g_ftbu_res,
	NULL, /*cssys*/
	&g_scr4k_ssid_res,
	&g_scr4k_sid_res,
	&g_smmu_tcu_res,
	NULL, /*ddr_cfg*/
	&g_pcie_reg_res,
	&g_pcie_io_res,
	&g_iomuxc_res,
	&g_rstgen_res,
	&g_scr_res,
	&g_ckgen_res,
};

#endif /* _DOMAIN_RES_H */
