
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef _OS_RES_CNT_H
#define _OS_RES_CNT_H

/* Generated os resource count declarations.
 * DO NOT MODIFY!
 */

#define RES_VERSION 0
#define GPU_RES_NUM 1
#define DDR_RES_NUM 7
#define GIC_RES_NUM 1
#define CE_MEM_RES_NUM 1
#define DMA_RES_NUM 2
#define MSHC_RES_NUM 1
#define SPI_RES_NUM 1
#define CE_REG_RES_NUM 1
#define I2S_SC_RES_NUM 1
#define PLL_RES_NUM 1
#define DMA_MUX_RES_NUM 2
#define PWM_RES_NUM 2
#define TIMER_RES_NUM 1
#define UART_RES_NUM 4
#define I2C_RES_NUM 2
#define GPIO_RES_NUM 1
#define SPDIF_RES_NUM 1
#define DC_RES_NUM 2
#define DP_RES_NUM 1
#define LVDS_RES_NUM 1
#define SYS_CNT_RES_NUM 1
#define SCR4K_SSID_RES_NUM 1
#define RSTGEN_RES_NUM 9
#define CKGEN_RES_NUM 44


#endif /* _OS_RES_CNT_H */
