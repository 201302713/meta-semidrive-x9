
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef __MEM_MAP_H__
#define __MEM_MAP_H__

// mem map define from Resource Allocation sheet
// MB
#define MB_MB_MEM_BASE 0x34040000
#define MB_MB_MEM_SIZE 0x9000

// DDR
#define DDR_DDR_MEM_ECO_DEV_BASE 0x5E200000
#define DDR_DDR_MEM_ECO_DEV_SIZE 0x2000000

// GIC
#define GIC_GIC4_BASE 0x35430000
#define GIC_GIC4_SIZE 0x10000

// CE_MEM
#define CE_MEM_CE2_VCE5_BASE 0x528000
#define CE_MEM_CE2_VCE5_SIZE 0x2000

// MSHC
#define MSHC_SD1_BASE 0x34180000
#define MSHC_SD1_SIZE 0x10000

// CE_REG
#define CE_REG_CE2_VCE5_BASE 0x34004000
#define CE_REG_CE2_VCE5_SIZE 0x1000

// PVT_SENS
#define PVT_SENS_PVT_SENS_SEC_BASE 0x30880000
#define PVT_SENS_PVT_SENS_SEC_SIZE 0x10000

// PLL
#define PLL_PLL_CPU1B_BASE 0x314D0000
#define PLL_PLL_CPU1B_SIZE 0x10000
#define PLL_PLL_CPU1A_BASE 0x314A0000
#define PLL_PLL_CPU1A_SIZE 0x10000
#define PLL_PLL_GPU2_BASE 0x31490000
#define PLL_PLL_GPU2_SIZE 0x10000
#define PLL_PLL_GPU1_BASE 0x31480000
#define PLL_PLL_GPU1_SIZE 0x10000

// WATCHDOG
#define WATCHDOG_WDT5_BASE 0x30A20000
#define WATCHDOG_WDT5_SIZE 0x10000

// UART
#define UART_UART9_BASE 0x304C0000
#define UART_UART9_SIZE 0x10000

// SYS_CNT
#define SYS_CNT_SYS_CNT_RO_BASE 0x31400000
#define SYS_CNT_SYS_CNT_RO_SIZE 0x10000

// SCR4K_SSID
#define SCR4K_SSID_SCR4K_SSID_BASE 0x35700000
#define SCR4K_SSID_SCR4K_SSID_SIZE 0x80000


// mam map define from Rpc sheet

#endif /* __MEM_MAP_H__*/
