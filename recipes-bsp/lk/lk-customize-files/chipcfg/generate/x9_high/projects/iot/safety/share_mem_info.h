
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef __SHARE_MEM_INFO_H__
#define __SHARE_MEM_INFO_H__

typedef struct share_mem_info {
    uint32_t res_id;
    addr_t paddr;
    uint32_t size;
    uint8_t mid_a;
    uint8_t mid_b;
} share_mem_info_t;

typedef struct share_mem_list {
    uint32_t share_mem_num;
    share_mem_info_t share_mem_info[];
} share_mem_list_t;

const share_mem_list_t share_mem_list = {
	.share_mem_info[0].res_id = RES_DDR_DDR_MEM_SAF_SEC,
	.share_mem_info[0].paddr = 0x3AC00000,
	.share_mem_info[0].size = 0x100000,
	.share_mem_info[0].mid_a = 0,
	.share_mem_info[0].mid_b = 1,
	.share_mem_info[1].res_id = RES_DDR_DDR_MEM_SAF_ECO,
	.share_mem_info[1].paddr = 0x3AD00000,
	.share_mem_info[1].size = 0x200000,
	.share_mem_info[1].mid_a = 0,
	.share_mem_info[1].mid_b = 3,
	.share_mem_num = 2,
};

#endif /* __SHARE_MEM_INFO_H__*/
