
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef __MEM_MAP_H__
#define __MEM_MAP_H__

// mem map define from Resource Allocation sheet
// DDR
#define DDR_DDR_MEM_ECO_ATF_BASE 0x5B000000
#define DDR_DDR_MEM_ECO_ATF_SIZE 0x200000

// GIC
#define GIC_GIC4_BASE 0x35430000
#define GIC_GIC4_SIZE 0x10000

// UART
#define UART_UART9_BASE 0x304C0000
#define UART_UART9_SIZE 0x10000

// SYS_CNT
#define SYS_CNT_SYS_CNT_RO_BASE 0x31400000
#define SYS_CNT_SYS_CNT_RO_SIZE 0x10000


// mam map define from Rpc sheet

#endif /* __MEM_MAP_H__*/
