
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef _PADDR_DEFINE_H
#define _PADDR_DEFINE_H

#define APBMUX1_IP_BASE     (0xf0000000u)
#define APBMUX2_IP_BASE     (0xf0400000u)
#define APBMUX3_IP_BASE     (0xf0800000u)
#define APBMUX4_IP_BASE     (0xf0c00000u)
#define APBMUX5_IP_BASE     (0xf1000000u)
#define APBMUX6_IP_BASE     (0xf1400000u)
#define APBMUX7_IP_BASE     (0xf1800000u)
#define APBMUX8_IP_BASE     (0xf1c00000u)
#define APB_DDR_CFG_BASE    (0xf2000000u)
#define APB_SMMU_BASE       (0xf5800000u)
#define APB_CE2_REG_BASE    (0xf4000000u)
#define APB_SCR4K_SID_BASE  (0xf5600000u)
#define APB_SCR4K_SSID_BASE (0xf5700000u)
#define APB_CSSYS_BASE      (0xf1000000u)
#define APB_RPC_SOC_BASE    (0xf6000000u)
#define APB_RPC_SEC_BASE    (0xf8000000u)
#define APB_RPC_SAF_BASE    (0xfC000000u)

#define _SIZE_32M_  0x2000000
#define _SIZE_20M_  0x1400000
#define _SIZE_16M_  0x1000000
#define _SIZE_8M_   0x800000
#define _SIZE_2M_   0x200000
#define _SIZE_1M_   0x100000
#define _SIZE_512K_ 0x80000
#define _SIZE_128K_ 0x20000
#define _SIZE_64K_  0x10000
#define _SIZE_4K_   0x1000
#define _SIZE_2K_   0x800
#define _SIZE_1K_   0x400
#define _SIZE_4B_   0x4
#define _RPC_MAC_OFFSET 0xFFE000

#define APBMUX1_IP_SIZE     (_SIZE_64K_)
#define APBMUX2_IP_SIZE     (_SIZE_64K_)
#define APBMUX3_IP_SIZE     (_SIZE_64K_)
#define APBMUX4_IP_SIZE     (_SIZE_64K_)
#define APBMUX5_IP_SIZE     (_SIZE_128K_)
#define APBMUX6_IP_SIZE     (_SIZE_64K_)
#define APBMUX7_IP_SIZE     (_SIZE_64K_)
#define APBMUX8_IP_SIZE     (_SIZE_4K_)
#define APB_DDR_CFG_SIZE    (_SIZE_20M_)
#define APB_SMMU_SIZE       (_SIZE_8M_)
#define APB_CE2_REG_SIZE    (_SIZE_4K_)
#define APB_SCR4K_SID_SIZE  (_SIZE_512K_)
#define APB_SCR4K_SSID_SIZE (_SIZE_512K_)
#define APB_CSSYS_SIZE      (_SIZE_16M_)
#define APB_GIC_SIZE        (_SIZE_64K_)

/* for address transformation from r core to a core */
#define APB_MUX_PHY_BASE_AP 0x30000000
#define DDR_PHY_BASE_AP 0x40000000
#define DDR_PHY_BASE 0x30000000
#define DDR_SPACE_SIZE 0xc0000000
#define APBMUX_SPACE_SIZE 0x10000000
#define SAF_TCM_PHY_BASE_AP 0x00330000
#define SAF_TCM_PHY_BASE 0x004b0000
#define SAF_TCM_SPACE_SIZE 0x50000

#define addrmap_def \
    {/*TCM/CACHE */\
        SAF_TCM_PHY_BASE,\
        SAF_TCM_SPACE_SIZE,\
        SAF_TCM_PHY_BASE_AP,\
    },	\
    {/*DDR */	\
        DDR_PHY_BASE,\
        DDR_SPACE_SIZE,\
        DDR_PHY_BASE_AP,\
    },\
    {/*APB_MUX */\
        APBMUX1_IP_BASE,\
        APBMUX_SPACE_SIZE,\
        APB_MUX_PHY_BASE_AP,\
    },

/* for memory type resources and undefined resources */
#define MPC_CATEGORY_MAX 14

typedef struct mem_info {
    uint32_t res_id;
    addr_t paddr;
} mem_info_t;

typedef struct domain_mem_info {
    int res_num;
    mem_info_t mem_info[];
} domain_mem_info_t;
const domain_mem_info_t mem_info_iram =
{
	.mem_info[0].res_id = RES_IRAM_IRAM5,
	.mem_info[0].paddr = 0xF0E80000,
	.mem_info[1].res_id = RES_IRAM_IRAM1,
	.mem_info[1].paddr = 0x00100000,
	.res_num = 2,
 };

const domain_mem_info_t mem_info_mb =
{
	.mem_info[0].res_id = RES_MB_MB_MEM,
	.mem_info[0].paddr = 0xF4040000,
	.res_num = 1,
 };

const domain_mem_info_t mem_info_ddr =
{
	.mem_info[0].res_id = RES_DDR_DDR_MEM_SYS_CFG,
	.mem_info[0].paddr = 0x30600000,
	.mem_info[1].res_id = RES_DDR_DDR_MEM_VBMETA,
	.mem_info[1].paddr = 0x30800000,
	.mem_info[2].res_id = RES_DDR_DDR_MEM_DIL_IMAGES,
	.mem_info[2].paddr = 0x30810000,
	.mem_info[3].res_id = RES_DDR_DDR_MEM_VDSP,
	.mem_info[3].paddr = 0x30A00000,
	.mem_info[4].res_id = RES_DDR_DDR_MEM_VDSP_SHARE,
	.mem_info[4].paddr = 0x33A00000,
	.mem_info[5].res_id = RES_DDR_DDR_MEM_SERVICES,
	.mem_info[5].paddr = 0x35A00000,
	.mem_info[6].res_id = RES_DDR_DDR_MEM_SAF_SEC,
	.mem_info[6].paddr = 0x3AC00000,
	.mem_info[7].res_id = RES_DDR_DDR_MEM_SAF_ECO,
	.mem_info[7].paddr = 0x3AD00000,
	.mem_info[8].res_id = RES_DDR_DDR_MEM_SAF,
	.mem_info[8].paddr = 0x3B000000,
	.res_num = 9,
 };

const domain_mem_info_t mem_info_ospi =
{
	.mem_info[0].res_id = RES_OSPI_OSPI1,
	.mem_info[0].paddr = 0x04000000,
	.res_num = 1,
 };

const domain_mem_info_t mem_info_ce_mem =
{
	.mem_info[0].res_id = RES_CE_MEM_CE1,
	.mem_info[0].paddr = 0x00510000,
	.res_num = 1,
 };

const domain_mem_info_t mem_info_vdsp =
{
	.mem_info[0].res_id = RES_VDSP_VDSP,
	.mem_info[0].paddr = 0x00A00000,
	.mem_info[1].res_id = RES_VDSP_EIC_VSN,
	.mem_info[1].paddr = 0xF5020000,
	.mem_info[2].res_id = RES_VDSP_BIPC_VSN,
	.mem_info[2].paddr = 0xF5010000,
	.mem_info[3].res_id = RES_VDSP_PLL_VSN,
	.mem_info[3].paddr = 0xF5000000,
	.mem_info[4].res_id = RES_VDSP_GPV_VSN,
	.mem_info[4].paddr = 0xF4400000,
	.res_num = 5,
 };

const domain_mem_info_t mem_info_dma =
{
	.mem_info[0].res_id = RES_DMA_DMA1,
	.mem_info[0].paddr = 0xF5500000,
	.res_num = 1,
 };

const domain_mem_info_t mem_info_unprotected =
{
	.mem_info[0].res_id = RES_GIC_GIC1,
	.mem_info[0].paddr = 0xF5400000,
	.mem_info[1].res_id = RES_ROMC_ROMC1,
	.mem_info[1].paddr = 0x00000000,
	.mem_info[2].res_id = RES_PLATFORM_R5_SAF_TCM,
	.mem_info[2].paddr = 0x004B0000,
	.mem_info[3].res_id = RES_PLATFORM_R5_SAF_CACHE,
	.mem_info[3].paddr = 0x004F0000,
	.res_num = 4,
 };

const domain_mem_info_t * mem_info_init[] = {
	&mem_info_iram,
	&mem_info_mb,
	NULL, /*gpu*/
	&mem_info_ddr,
	NULL, /*pcie*/
	NULL, /*gic*/
	&mem_info_ospi,
	NULL, /*romc*/
	NULL, /*platform*/
	&mem_info_ce_mem,
	&mem_info_vdsp,
	&mem_info_dma,
	NULL, /*mshc*/
	NULL, /*mac*/
};
#endif /* _PADDR_DEFINE_H*/
