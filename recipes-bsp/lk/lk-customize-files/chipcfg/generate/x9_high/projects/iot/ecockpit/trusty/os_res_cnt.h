
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef _OS_RES_CNT_H
#define _OS_RES_CNT_H

/* Generated os resource count declarations.
 * DO NOT MODIFY!
 */

#define RES_VERSION 0
#define DDR_RES_NUM 1
#define GIC_RES_NUM 1
#define CE_MEM_RES_NUM 1
#define CE_REG_RES_NUM 1
#define TIMER_RES_NUM 1
#define UART_RES_NUM 1
#define SYS_CNT_RES_NUM 1
#define CKGEN_RES_NUM 1


#endif /* _OS_RES_CNT_H */
