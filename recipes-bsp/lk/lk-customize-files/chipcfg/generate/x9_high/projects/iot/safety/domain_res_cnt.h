
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef _DOMAIN_RES_CNT_H
#define _DOMAIN_RES_CNT_H

/* Generated domain resource count declarations.
 * DO NOT MODIFY!
 */

#define RES_VERSION 0
#define IRAM_RES_NUM 2
#define MB_RES_NUM 1
#define DDR_RES_NUM 9
#define GIC_RES_NUM 1
#define OSPI_RES_NUM 1
#define ROMC_RES_NUM 1
#define PLATFORM_RES_NUM 2
#define CE_MEM_RES_NUM 1
#define VDSP_RES_NUM 5
#define DMA_RES_NUM 1
#define GPV_RES_NUM 2
#define SPI_RES_NUM 4
#define CE_REG_RES_NUM 1
#define SEM_RES_NUM 2
#define I2S_SC_RES_NUM 2
#define BIPC_RES_NUM 2
#define EIC_RES_NUM 3
#define PVT_SENS_RES_NUM 1
#define PWRCTRL_RES_NUM 1
#define PLL_RES_NUM 19
#define RC24M_RES_NUM 1
#define XTAL_RES_NUM 2
#define ROMC_REG_RES_NUM 1
#define IRAM_REG_RES_NUM 1
#define DMA_MUX_RES_NUM 1
#define WATCHDOG_RES_NUM 2
#define PWM_RES_NUM 5
#define TIMER_RES_NUM 3
#define UART_RES_NUM 6
#define I2C_RES_NUM 12
#define CAN_RES_NUM 2
#define OSPI_REG_RES_NUM 1
#define EFUSE_RES_NUM 1
#define GPIO_RES_NUM 1
#define ADC_RES_NUM 1
#define DC_RES_NUM 4
#define MIPI_DSI_RES_NUM 2
#define MIPI_CSI_RES_NUM 2
#define CSI_RES_NUM 3
#define G2D_RES_NUM 1
#define LVDS_RES_NUM 4
#define MJPEG_RES_NUM 1
#define SYS_CNT_RES_NUM 1
#define SEC_STORAGE_RES_NUM 1
#define RSTGEN_RTC_RES_NUM 1
#define TM_RES_NUM 1
#define IOMUXC_RTC_RES_NUM 1
#define PMU_RES_NUM 1
#define RC_RTC_RES_NUM 1
#define RTC_RES_NUM 1
#define FTBU_RES_NUM 3
#define IOMUXC_RES_NUM 474
#define RSTGEN_RES_NUM 104
#define SCR_RES_NUM 169
#define CKGEN_RES_NUM 358


#endif /* _DOMAIN_RES_CNT_H */
