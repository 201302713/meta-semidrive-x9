
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef _DOMAIN_RES_CNT_H
#define _DOMAIN_RES_CNT_H

/* Generated domain resource count declarations.
 * DO NOT MODIFY!
 */

#define RES_VERSION 0
#define IRAM_RES_NUM 6
#define MB_RES_NUM 1
#define GPU_RES_NUM 2
#define DDR_RES_NUM 14
#define PCIE_RES_NUM 18
#define GIC_RES_NUM 1
#define OSPI_RES_NUM 1
#define CE_MEM_RES_NUM 4
#define VDSP_RES_NUM 1
#define DMA_RES_NUM 6
#define MSHC_RES_NUM 4
#define SPI_RES_NUM 3
#define CE_REG_RES_NUM 4
#define I2S_SC_RES_NUM 6
#define PVT_SENS_RES_NUM 1
#define PLL_RES_NUM 7
#define IRAM_REG_RES_NUM 3
#define DMA_MUX_RES_NUM 6
#define WATCHDOG_RES_NUM 2
#define PWM_RES_NUM 3
#define TIMER_RES_NUM 4
#define ENET_QOS_RES_NUM 1
#define UART_RES_NUM 8
#define I2C_RES_NUM 12
#define OSPI_REG_RES_NUM 1
#define GPIO_RES_NUM 1
#define I2S_MC_RES_NUM 2
#define SPDIF_RES_NUM 4
#define DC_RES_NUM 5
#define DP_RES_NUM 3
#define MIPI_CSI_RES_NUM 2
#define CSI_RES_NUM 3
#define G2D_RES_NUM 1
#define LVDS_RES_NUM 1
#define DMA_CH_MUX_PCIE_RES_NUM 2
#define USBPHY_RES_NUM 2
#define USB_RES_NUM 2
#define PCIE_PHY_RES_NUM 1
#define VPU_RES_NUM 2
#define MJPEG_RES_NUM 1
#define SYS_CNT_RES_NUM 1
#define RTC_RES_NUM 1
#define FTBU_RES_NUM 7
#define SCR4K_SSID_RES_NUM 1
#define SCR4K_SID_RES_NUM 1
#define SMMU_TCU_RES_NUM 1
#define PCIE_REG_RES_NUM 2
#define PCIE_IO_RES_NUM 2
#define IOMUXC_RES_NUM 224
#define RSTGEN_RES_NUM 59
#define SCR_RES_NUM 43
#define CKGEN_RES_NUM 183


#endif /* _DOMAIN_RES_CNT_H */
