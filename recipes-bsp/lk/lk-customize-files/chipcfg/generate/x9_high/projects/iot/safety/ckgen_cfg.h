
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef __CKGEN_CFG_H__
#define __CKGEN_CFG_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define CFG_CKGEN_DISP_BASE (0xF6200000)
#define CFG_CKGEN_SOC_BASE (0xF6000000)
#define CFG_CKGEN_SAF_BASE (0xFC000000)
#define CFG_CKGEN_SEC_BASE (0xF8000000)

/*clkgen saf bus slice index*/
typedef enum _clkgen_saf_bus_slice_idx
{
	saf_bus_slice_saf_plat = 0U,
	saf_bus_slice_max,
}clkgen_saf_bus_slice_idx;

/*clkgen saf ip slice index*/
typedef enum _clkgen_saf_ip_slice_idx
{
	saf_ip_slice_ce1 = 0U,
	saf_ip_slice_i2c_saf = 1U,
	saf_ip_slice_spi_saf = 2U,
	saf_ip_slice_uart_saf = 3U,
	saf_ip_slice_i2s_mclk1 = 4U,
	saf_ip_slice_i2s_sc1 = 5U,
	saf_ip_slice_i2s_sc2 = 6U,
	saf_ip_slice_enet1_tx = 7U,
	saf_ip_slice_enet1_rmii = 8U,
	saf_ip_slice_enet1_phy_ref = 9U,
	saf_ip_slice_enet1_timer_sec = 10U,
	saf_ip_slice_ospi1 = 11U,
	saf_ip_slice_timer1 = 12U,
	saf_ip_slice_timer2 = 13U,
	saf_ip_slice_pwm1 = 14U,
	saf_ip_slice_pwm2 = 15U,
	saf_ip_slice_can_1_to_4 = 16U,
	saf_ip_slice_max,
}clkgen_saf_ip_slice_idx;

#define saf_bus_slice {\
	{\
		.slice_index = saf_bus_slice_saf_plat,\
		.clk_src_select_a[0] = 24000000,\
		.clk_src_select_a[1] = 2000000,\
		.clk_src_select_a[2] = 24000000,\
		.clk_src_select_a[3] = 32000,\
		.clk_src_select_a[4] = 1000000000,\
		.clk_src_select_a[5] = 800000000,\
		.clk_src_select_a[6] = 500000000,\
		.clk_src_select_a[7] = 400000000,\
		.clk_src_select_a_num = 5,\
		.clk_src_select_b[0] = 24000000,\
		.clk_src_select_b[1] = 2000000,\
		.clk_src_select_b[2] = 24000000,\
		.clk_src_select_b[3] = 32000,\
		.clk_src_select_b[4] = 1000000000,\
		.clk_src_select_b[5] = 800000000,\
		.clk_src_select_b[6] = 500000000,\
		.clk_src_select_b[7] = 400000000,\
		.clk_src_select_b_num = 5,\
		.clk_a_b_select = 0,\
		.pre_div_a = 0,\
		.pre_div_b = 0,\
		.post_div = 0,\
		.m_div = 1,\
		.n_div = 3,\
		.p_div = 0,\
		.q_div = 0,\
		.a_sel_clk = 800000000,\
		.b_sel_clk = 800000000,\
	},\
}

#define saf_ip_slice {\
	{\
		.slice_index = saf_ip_slice_ce1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 800000000,\
		.clk_src_select[5] = 1000000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 1,\
		.sel_clk = 400000000,\
	},\
	{\
		.slice_index = saf_ip_slice_i2c_saf,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 160000000,\
		.clk_src_select[5] = 266666999,\
		.clk_src_select[6] = 333333000,\
		.clk_src_select[7] = 200000000,\
		.clk_src_select_num = 2,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 24000000,\
	},\
	{\
		.slice_index = saf_ip_slice_spi_saf,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 333333000,\
		.clk_src_select[5] = 266666999,\
		.clk_src_select[6] = 160000000,\
		.clk_src_select[7] = 200000000,\
		.clk_src_select_num = 5,\
		.pre_div = 0,\
		.post_div = 1,\
		.sel_clk = 133330000,\
	},\
	{\
		.slice_index = saf_ip_slice_uart_saf,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 160000000,\
		.clk_src_select[5] = 266666999,\
		.clk_src_select[6] = 333333000,\
		.clk_src_select[7] = 200000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 1,\
		.sel_clk = 80000000,\
	},\
	{\
		.slice_index = saf_ip_slice_i2s_mclk1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 500000000,\
		.clk_src_select[5] = 400000000,\
		.clk_src_select[6] = 333333000,\
		.clk_src_select[7] = 266666999,\
		.clk_src_select_num = 2,\
		.pre_div = 0,\
		.post_div = 1,\
		.sel_clk = 12000000,\
	},\
	{\
		.slice_index = saf_ip_slice_i2s_sc1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 199500000,\
		.clk_src_select[4] = 500000000,\
		.clk_src_select[5] = 400000000,\
		.clk_src_select[6] = 333333000,\
		.clk_src_select[7] = 266666999,\
		.clk_src_select_num = 5,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 400000000,\
	},\
	{\
		.slice_index = saf_ip_slice_i2s_sc2,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 199500000,\
		.clk_src_select[4] = 500000000,\
		.clk_src_select[5] = 400000000,\
		.clk_src_select[6] = 333333000,\
		.clk_src_select[7] = 266666999,\
		.clk_src_select_num = 5,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 400000000,\
	},\
	{\
		.slice_index = saf_ip_slice_enet1_tx,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 250000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 250000000,\
	},\
	{\
		.slice_index = saf_ip_slice_enet1_rmii,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 250000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 4,\
		.sel_clk = 50000000,\
	},\
	{\
		.slice_index = saf_ip_slice_enet1_phy_ref,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 250000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 1,\
		.sel_clk = 125000000,\
	},\
	{\
		.slice_index = saf_ip_slice_enet1_timer_sec,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 500000000,\
		.clk_src_select[5] = 800000000,\
		.clk_src_select[6] = 400000000,\
		.clk_src_select[7] = 200000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 500000000,\
	},\
	{\
		.slice_index = saf_ip_slice_ospi1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 333333000,\
		.clk_src_select[5] = 266666999,\
		.clk_src_select[6] = 200000000,\
		.clk_src_select[7] = 400000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 333333000,\
	},\
	{\
		.slice_index = saf_ip_slice_timer1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 400000000,\
		.clk_src_select[5] = 200000000,\
		.clk_src_select[6] = 500000000,\
		.clk_src_select[7] = 200000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 400000000,\
	},\
	{\
		.slice_index = saf_ip_slice_timer2,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 400000000,\
		.clk_src_select[5] = 200000000,\
		.clk_src_select[6] = 500000000,\
		.clk_src_select[7] = 200000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 400000000,\
	},\
	{\
		.slice_index = saf_ip_slice_pwm1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 400000000,\
		.clk_src_select[5] = 200000000,\
		.clk_src_select[6] = 500000000,\
		.clk_src_select[7] = 200000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 400000000,\
	},\
	{\
		.slice_index = saf_ip_slice_pwm2,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 400000000,\
		.clk_src_select[5] = 200000000,\
		.clk_src_select[6] = 500000000,\
		.clk_src_select[7] = 200000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 400000000,\
	},\
	{\
		.slice_index = saf_ip_slice_can_1_to_4,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 160000000,\
		.clk_src_select[5] = 200000000,\
		.clk_src_select[6] = 500000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 1,\
		.sel_clk = 80000000,\
	},\
}

/*clkgen disp ip slice index*/
typedef enum _clkgen_disp_ip_slice_idx
{
	disp_ip_slice_mipi_csi1_pix = 0U,
	disp_ip_slice_mipi_csi2_pix = 1U,
	disp_ip_slice_csi3 = 2U,
	disp_ip_slice_dp1 = 3U,
	disp_ip_slice_dp2 = 4U,
	disp_ip_slice_dp3 = 5U,
	disp_ip_slice_dc5 = 6U,
	disp_ip_slice_dc1 = 7U,
	disp_ip_slice_dc2 = 8U,
	disp_ip_slice_dc3 = 9U,
	disp_ip_slice_dc4 = 10U,
	disp_ip_slice_spare1 = 11U,
	disp_ip_slice_dc5_alt_dsp_clk = 12U,
	disp_ip_slice_ext_aud1 = 13U,
	disp_ip_slice_ext_aud2 = 14U,
	disp_ip_slice_ext_aud3 = 15U,
	disp_ip_slice_ext_aud4 = 16U,
	disp_ip_slice_max,
}clkgen_disp_ip_slice_idx;

/*clkgen disp bus slice index*/
typedef enum _clkgen_disp_bus_slice_idx
{
	disp_bus_slice_disp_bus = 0U,
	disp_bus_slice_max,
}clkgen_disp_bus_slice_idx;

/*clkgen sec bus slice index*/
typedef enum _clkgen_sec_bus_slice_idx
{
	sec_bus_slice_sec_plat = 0U,
	sec_bus_slice_spare_0 = 1U,
	sec_bus_slice_spare_1 = 2U,
	sec_bus_slice_max,
}clkgen_sec_bus_slice_idx;

/*clkgen sec core slice index*/
typedef enum _clkgen_sec_core_slice_idx
{
	sec_core_slice_mp_plat = 0U,
	sec_core_slice_max,
}clkgen_sec_core_slice_idx;

/*clkgen sec ip slice index*/
typedef enum _clkgen_sec_ip_slice_idx
{
	sec_ip_slice_ce2 = 0U,
	sec_ip_slice_adc = 1U,
	sec_ip_slice_spare_2 = 2U,
	sec_ip_slice_i2c_sec0 = 3U,
	sec_ip_slice_i2c_sec1 = 4U,
	sec_ip_slice_spi_sec0 = 5U,
	sec_ip_slice_spi_sec1 = 6U,
	sec_ip_slice_uart_sec0 = 7U,
	sec_ip_slice_uart_sec1 = 8U,
	sec_ip_slice_emmc1 = 9U,
	sec_ip_slice_emmc2 = 10U,
	sec_ip_slice_emmc3 = 11U,
	sec_ip_slice_emmc4 = 12U,
	sec_ip_slice_enet2_tx = 13U,
	sec_ip_slice_enet2_rmii = 14U,
	sec_ip_slice_enet2_phy_ref = 15U,
	sec_ip_slice_enet2_timer_sec = 16U,
	sec_ip_slice_spdif1 = 17U,
	sec_ip_slice_spdif2 = 18U,
	sec_ip_slice_spdif3 = 19U,
	sec_ip_slice_spdif4 = 20U,
	sec_ip_slice_ospi2 = 21U,
	sec_ip_slice_timer3 = 22U,
	sec_ip_slice_timer4 = 23U,
	sec_ip_slice_timer5 = 24U,
	sec_ip_slice_timer6 = 25U,
	sec_ip_slice_timer7 = 26U,
	sec_ip_slice_timer8 = 27U,
	sec_ip_slice_pwm3 = 28U,
	sec_ip_slice_pwm4 = 29U,
	sec_ip_slice_pwm5 = 30U,
	sec_ip_slice_pwm6 = 31U,
	sec_ip_slice_pwm7 = 32U,
	sec_ip_slice_pwm8 = 33U,
	sec_ip_slice_i2s_mclk2 = 34U,
	sec_ip_slice_i2s_mclk3 = 35U,
	sec_ip_slice_i2s_mc1 = 36U,
	sec_ip_slice_i2s_mc2 = 37U,
	sec_ip_slice_i2s_sc3 = 38U,
	sec_ip_slice_i2s_sc4 = 39U,
	sec_ip_slice_i2s_sc5 = 40U,
	sec_ip_slice_i2s_sc6 = 41U,
	sec_ip_slice_i2s_sc7 = 42U,
	sec_ip_slice_i2s_sc8 = 43U,
	sec_ip_slice_csi_mclk1 = 44U,
	sec_ip_slice_csi_mclk2 = 45U,
	sec_ip_slice_gic_4_5 = 46U,
	sec_ip_slice_can_5_to_20 = 47U,
	sec_ip_slice_trace = 48U,
	sec_ip_slice_sys_cnt = 49U,
	sec_ip_slice_mshc_timer = 50U,
	sec_ip_slice_hpi_clk600 = 51U,
	sec_ip_slice_hpi_clk800 = 52U,
	sec_ip_slice_max,
}clkgen_sec_ip_slice_idx;

/*clkgen soc core slice index*/
typedef enum _clkgen_soc_core_slice_idx
{
	soc_core_slice_cpu1a = 0U,
	soc_core_slice_cpu1b = 1U,
	soc_core_slice_cpu2 = 2U,
	soc_core_slice_gpu1 = 3U,
	soc_core_slice_gpu2 = 4U,
	soc_core_slice_ddr = 5U,
	soc_core_slice_max,
}clkgen_soc_core_slice_idx;

/*clkgen soc ip slice index*/
typedef enum _clkgen_soc_ip_slice_idx
{
	soc_ip_slice_vpu1 = 0U,
	soc_ip_slice_mjpeg = 1U,
	soc_ip_slice_can_9_to_20 = 2U,
	soc_ip_slice_max,
}clkgen_soc_ip_slice_idx;

/*clkgen soc bus slice index*/
typedef enum _clkgen_soc_bus_slice_idx
{
	soc_bus_slice_vpu_bus = 0U,
	soc_bus_slice_vsn_bus = 1U,
	soc_bus_slice_noc = 2U,
	soc_bus_slice_his_bus = 3U,
	soc_bus_slice_max,
}clkgen_soc_bus_slice_idx;

/*clkgen uuu wrapper index*/
typedef enum _clkgen_uuu_wrapper_idx
{
	uuu_clock_wrapper_cpu1a = 0U,
	uuu_clock_wrapper_cpu1b = 1U,
	uuu_clock_wrapper_cpu2 = 2U,
	uuu_clock_wrapper_gpu1 = 3U,
	uuu_clock_wrapper_gpu2 = 4U,
	uuu_clock_wrapper_vpu1 = 5U,
	uuu_clock_wrapper_mjpeg = 6U,
	uuu_clock_wrapper_vpu_bus = 7U,
	uuu_clock_wrapper_vsn_bus = 8U,
	uuu_clock_wrapper_ddr = 9U,
	uuu_clock_wrapper_his_bus = 10U,
	uuu_clock_wrapper_idx_max,
} clkgen_uuu_wrapper_idx;

#define uuu_wrapper {\
	{\
		.slice_index = uuu_clock_wrapper_cpu1a,\
		.uuu_input_clk_sel = uuu_input_pll_clk,\
		.m_div = 0,\
		.n_div = 1,\
		.p_div = 3,\
		.q_div = 7,\
	},\
	{\
		.slice_index = uuu_clock_wrapper_cpu1b,\
		.uuu_input_clk_sel = uuu_input_pll_clk,\
		.m_div = 0,\
		.n_div = 1,\
		.p_div = 3,\
		.q_div = 7,\
	},\
	{\
		.slice_index = uuu_clock_wrapper_gpu1,\
		.uuu_input_clk_sel = uuu_input_pll_clk,\
		.m_div = 0,\
		.n_div = 0,\
		.p_div = 3,\
		.q_div = 0,\
	},\
	{\
		.slice_index = uuu_clock_wrapper_gpu2,\
		.uuu_input_clk_sel = uuu_input_pll_clk,\
		.m_div = 0,\
		.n_div = 0,\
		.p_div = 3,\
		.q_div = 0,\
	},\
	{\
		.slice_index = uuu_clock_wrapper_vpu1,\
		.uuu_input_clk_sel = uuu_input_pll_clk,\
		.m_div = 0,\
		.n_div = 0,\
		.p_div = 0,\
		.q_div = 0,\
	},\
	{\
		.slice_index = uuu_clock_wrapper_mjpeg,\
		.uuu_input_clk_sel = uuu_input_pll_clk,\
		.m_div = 0,\
		.n_div = 0,\
		.p_div = 0,\
		.q_div = 0,\
	},\
	{\
		.slice_index = uuu_clock_wrapper_vpu_bus,\
		.uuu_input_clk_sel = uuu_input_pll_clk,\
		.m_div = 0,\
		.n_div = 3,\
		.p_div = 0,\
		.q_div = 0,\
	},\
	{\
		.slice_index = uuu_clock_wrapper_vsn_bus,\
		.uuu_input_clk_sel = uuu_input_pll_clk,\
		.m_div = 0,\
		.n_div = 3,\
		.p_div = 0,\
		.q_div = 0,\
	},\
	{\
		.slice_index = uuu_clock_wrapper_ddr,\
		.uuu_input_clk_sel = uuu_input_pll_clk,\
		.m_div = 0,\
		.n_div = 1,\
		.p_div = 3,\
		.q_div = 0,\
	},\
	{\
		.slice_index = uuu_clock_wrapper_his_bus,\
		.uuu_input_clk_sel = uuu_input_pll_clk,\
		.m_div = 0,\
		.n_div = 9,\
		.p_div = 1,\
		.q_div = 3,\
	},\
}
#define disp_ip_slice {\
	{\
		.slice_index = disp_ip_slice_mipi_csi1_pix,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 1039500000,\
		.clk_src_select[5] = 2079000000,\
		.clk_src_select_num = 5,\
		.pre_div = 0,\
		.post_div = 2,\
		.sel_clk = 693000000,\
	},\
	{\
		.slice_index = disp_ip_slice_mipi_csi2_pix,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 1039500000,\
		.clk_src_select[5] = 2079000000,\
		.clk_src_select_num = 5,\
		.pre_div = 0,\
		.post_div = 2,\
		.sel_clk = 693000000,\
	},\
	{\
		.slice_index = disp_ip_slice_csi3,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 1039500000,\
		.clk_src_select[5] = 2079000000,\
		.clk_src_select_num = 5,\
		.pre_div = 0,\
		.post_div = 2,\
		.sel_clk = 693000000,\
	},\
	{\
		.slice_index = disp_ip_slice_dp1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 415800000,\
		.clk_src_select[5] = 2079000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 415800000,\
	},\
	{\
		.slice_index = disp_ip_slice_dp2,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 415800000,\
		.clk_src_select[5] = 2079000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 415800000,\
	},\
	{\
		.slice_index = disp_ip_slice_dp3,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 415800000,\
		.clk_src_select[5] = 2079000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 415800000,\
	},\
	{\
		.slice_index = disp_ip_slice_dc5,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 415800000,\
		.clk_src_select[5] = 2079000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 415800000,\
	},\
	{\
		.slice_index = disp_ip_slice_dc1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 415800000,\
		.clk_src_select[5] = 2079000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 415800000,\
	},\
	{\
		.slice_index = disp_ip_slice_dc2,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 415800000,\
		.clk_src_select[5] = 2079000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 415800000,\
	},\
	{\
		.slice_index = disp_ip_slice_dc3,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 415800000,\
		.clk_src_select[5] = 2079000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 415800000,\
	},\
	{\
		.slice_index = disp_ip_slice_dc4,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 415800000,\
		.clk_src_select[5] = 2079000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 415800000,\
	},\
	{\
		.slice_index = disp_ip_slice_spare1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 1039500000,\
		.clk_src_select[5] = 2079000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 9,\
		.sel_clk = 103950000,\
	},\
	{\
		.slice_index = disp_ip_slice_dc5_alt_dsp_clk,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 1039500000,\
		.clk_src_select[5] = 2079000000,\
		.clk_src_select_num = 5,\
		.pre_div = 0,\
		.post_div = 13,\
		.sel_clk = 148500000,\
	},\
	{\
		.slice_index = disp_ip_slice_ext_aud1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 1995000000,\
		.clk_src_select[5] = 1659000000,\
		.clk_src_select[6] = 1995000000,\
		.clk_src_select[7] = 1995000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 9,\
		.sel_clk = 199500000,\
	},\
	{\
		.slice_index = disp_ip_slice_ext_aud2,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 1995000000,\
		.clk_src_select[5] = 1659000000,\
		.clk_src_select[6] = 1995000000,\
		.clk_src_select[7] = 1995000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 9,\
		.sel_clk = 199500000,\
	},\
	{\
		.slice_index = disp_ip_slice_ext_aud3,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 1995000000,\
		.clk_src_select[5] = 1659000000,\
		.clk_src_select[6] = 1995000000,\
		.clk_src_select[7] = 1995000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 9,\
		.sel_clk = 199500000,\
	},\
	{\
		.slice_index = disp_ip_slice_ext_aud4,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 1995000000,\
		.clk_src_select[5] = 1659000000,\
		.clk_src_select[6] = 1995000000,\
		.clk_src_select[7] = 1995000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 9,\
		.sel_clk = 199500000,\
	},\
}

#define disp_bus_slice {\
	{\
		.slice_index = disp_bus_slice_disp_bus,\
		.clk_src_select_a[0] = 24000000,\
		.clk_src_select_a[1] = 2000000,\
		.clk_src_select_a[2] = 24000000,\
		.clk_src_select_a[3] = 32000,\
		.clk_src_select_a[4] = 1039500000,\
		.clk_src_select_a[5] = 2079000000,\
		.clk_src_select_a_num = 4,\
		.clk_src_select_b[0] = 24000000,\
		.clk_src_select_b[1] = 2000000,\
		.clk_src_select_b[2] = 24000000,\
		.clk_src_select_b[3] = 32000,\
		.clk_src_select_b[4] = 1039500000,\
		.clk_src_select_b[5] = 2079000000,\
		.clk_src_select_b_num = 4,\
		.clk_a_b_select = 0,\
		.pre_div_a = 0,\
		.pre_div_b = 0,\
		.post_div = 1,\
		.m_div = 0,\
		.n_div = 1,\
		.p_div = 0,\
		.q_div = 0,\
		.a_sel_clk = 519750000,\
		.b_sel_clk = 519750000,\
	},\
}

#define sec_bus_slice {\
	{\
		.slice_index = sec_bus_slice_sec_plat,\
		.clk_src_select_a[0] = 24000000,\
		.clk_src_select_a[1] = 2000000,\
		.clk_src_select_a[2] = 24000000,\
		.clk_src_select_a[3] = 32000,\
		.clk_src_select_a[4] = 1000000000,\
		.clk_src_select_a[5] = 796000000,\
		.clk_src_select_a[6] = 900000000,\
		.clk_src_select_a[7] = 600000000,\
		.clk_src_select_a_num = 5,\
		.clk_src_select_b[0] = 24000000,\
		.clk_src_select_b[1] = 2000000,\
		.clk_src_select_b[2] = 24000000,\
		.clk_src_select_b[3] = 32000,\
		.clk_src_select_b[4] = 1000000000,\
		.clk_src_select_b[5] = 796000000,\
		.clk_src_select_b[6] = 900000000,\
		.clk_src_select_b[7] = 600000000,\
		.clk_src_select_b_num = 5,\
		.clk_a_b_select = 0,\
		.pre_div_a = 0,\
		.pre_div_b = 0,\
		.post_div = 0,\
		.m_div = 1,\
		.n_div = 7,\
		.p_div = 0,\
		.q_div = 0,\
		.a_sel_clk = 796000000,\
		.b_sel_clk = 796000000,\
	},\
	{\
		.slice_index = sec_bus_slice_spare_0,\
		.clk_src_select_a[0] = 24000000,\
		.clk_src_select_a[1] = 2000000,\
		.clk_src_select_a[2] = 24000000,\
		.clk_src_select_a[3] = 32000,\
		.clk_src_select_a[4] = 1000000000,\
		.clk_src_select_a[5] = 796000000,\
		.clk_src_select_a[6] = 900000000,\
		.clk_src_select_a[7] = 600000000,\
		.clk_src_select_a_num = 4,\
		.clk_src_select_b[0] = 24000000,\
		.clk_src_select_b[1] = 2000000,\
		.clk_src_select_b[2] = 24000000,\
		.clk_src_select_b[3] = 32000,\
		.clk_src_select_b[4] = 1000000000,\
		.clk_src_select_b[5] = 796000000,\
		.clk_src_select_b[6] = 900000000,\
		.clk_src_select_b[7] = 600000000,\
		.clk_src_select_b_num = 4,\
		.clk_a_b_select = 0,\
		.pre_div_a = 0,\
		.pre_div_b = 0,\
		.post_div = 1,\
		.m_div = 0,\
		.n_div = 0,\
		.p_div = 0,\
		.q_div = 0,\
		.a_sel_clk = 500000000,\
		.b_sel_clk = 500000000,\
	},\
	{\
		.slice_index = sec_bus_slice_spare_1,\
		.clk_src_select_a[0] = 24000000,\
		.clk_src_select_a[1] = 2000000,\
		.clk_src_select_a[2] = 24000000,\
		.clk_src_select_a[3] = 32000,\
		.clk_src_select_a[4] = 1000000000,\
		.clk_src_select_a[5] = 796000000,\
		.clk_src_select_a[6] = 900000000,\
		.clk_src_select_a[7] = 600000000,\
		.clk_src_select_a_num = 4,\
		.clk_src_select_b[0] = 24000000,\
		.clk_src_select_b[1] = 2000000,\
		.clk_src_select_b[2] = 24000000,\
		.clk_src_select_b[3] = 32000,\
		.clk_src_select_b[4] = 1000000000,\
		.clk_src_select_b[5] = 796000000,\
		.clk_src_select_b[6] = 900000000,\
		.clk_src_select_b[7] = 600000000,\
		.clk_src_select_b_num = 4,\
		.clk_a_b_select = 0,\
		.pre_div_a = 0,\
		.pre_div_b = 0,\
		.post_div = 1,\
		.m_div = 0,\
		.n_div = 0,\
		.p_div = 0,\
		.q_div = 0,\
		.a_sel_clk = 500000000,\
		.b_sel_clk = 500000000,\
	},\
}

#define sec_ip_slice {\
	{\
		.slice_index = sec_ip_slice_ce2,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 796000000,\
		.clk_src_select[5] = 1000000000,\
		.clk_src_select[6] = 900000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 1,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_adc,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 796000000,\
		.clk_src_select[5] = 1000000000,\
		.clk_src_select[6] = 600000000,\
		.clk_src_select[7] = 294912000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 1,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_spare_2,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 796000000,\
		.clk_src_select[5] = 1000000000,\
		.clk_src_select[6] = 600000000,\
		.clk_src_select[7] = 294912000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 796000000,\
	},\
	{\
		.slice_index = sec_ip_slice_i2c_sec0,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 120000000,\
		.clk_src_select[5] = 360000000,\
		.clk_src_select[6] = 159200000,\
		.clk_src_select[7] = 150000000,\
		.clk_src_select_num = 2,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 24000000,\
	},\
	{\
		.slice_index = sec_ip_slice_i2c_sec1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 120000000,\
		.clk_src_select[5] = 360000000,\
		.clk_src_select[6] = 159200000,\
		.clk_src_select[7] = 150000000,\
		.clk_src_select_num = 2,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 24000000,\
	},\
	{\
		.slice_index = sec_ip_slice_spi_sec0,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 120000000,\
		.clk_src_select[5] = 360000000,\
		.clk_src_select[6] = 159200000,\
		.clk_src_select[7] = 150000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 120000000,\
	},\
	{\
		.slice_index = sec_ip_slice_spi_sec1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 120000000,\
		.clk_src_select[5] = 360000000,\
		.clk_src_select[6] = 159200000,\
		.clk_src_select[7] = 150000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 120000000,\
	},\
	{\
		.slice_index = sec_ip_slice_uart_sec0,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 120000000,\
		.clk_src_select[5] = 360000000,\
		.clk_src_select[6] = 159200000,\
		.clk_src_select[7] = 150000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 1,\
		.sel_clk = 60000000,\
	},\
	{\
		.slice_index = sec_ip_slice_uart_sec1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 120000000,\
		.clk_src_select[5] = 360000000,\
		.clk_src_select[6] = 159200000,\
		.clk_src_select[7] = 150000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 1,\
		.sel_clk = 60000000,\
	},\
	{\
		.slice_index = sec_ip_slice_emmc1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 398000000,\
		.clk_src_select[5] = 333333000,\
		.clk_src_select[6] = 199000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_emmc2,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 398000000,\
		.clk_src_select[5] = 333333000,\
		.clk_src_select[6] = 199000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_emmc3,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 398000000,\
		.clk_src_select[5] = 333333000,\
		.clk_src_select[6] = 199000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_emmc4,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 398000000,\
		.clk_src_select[5] = 333333000,\
		.clk_src_select[6] = 199000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 1,\
		.sel_clk = 199000000,\
	},\
	{\
		.slice_index = sec_ip_slice_enet2_tx,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 250000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 250000000,\
	},\
	{\
		.slice_index = sec_ip_slice_enet2_rmii,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 250000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 4,\
		.sel_clk = 50000000,\
	},\
	{\
		.slice_index = sec_ip_slice_enet2_phy_ref,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 250000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 1,\
		.sel_clk = 125000000,\
	},\
	{\
		.slice_index = sec_ip_slice_enet2_timer_sec,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 500000000,\
		.clk_src_select[5] = 796000000,\
		.clk_src_select[6] = 398000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 500000000,\
	},\
	{\
		.slice_index = sec_ip_slice_spdif1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 294912000,\
		.clk_src_select[5] = 270950000,\
		.clk_src_select[6] = 265333000,\
		.clk_src_select[7] = 360000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 4,\
		.sel_clk = 58980000,\
	},\
	{\
		.slice_index = sec_ip_slice_spdif2,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 294912000,\
		.clk_src_select[5] = 270950000,\
		.clk_src_select[6] = 265333000,\
		.clk_src_select[7] = 360000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 4,\
		.sel_clk = 58980000,\
	},\
	{\
		.slice_index = sec_ip_slice_spdif3,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 294912000,\
		.clk_src_select[5] = 270950000,\
		.clk_src_select[6] = 265333000,\
		.clk_src_select[7] = 360000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 4,\
		.sel_clk = 58980000,\
	},\
	{\
		.slice_index = sec_ip_slice_spdif4,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 294912000,\
		.clk_src_select[5] = 270950000,\
		.clk_src_select[6] = 265333000,\
		.clk_src_select[7] = 360000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 4,\
		.sel_clk = 58980000,\
	},\
	{\
		.slice_index = sec_ip_slice_ospi2,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 333333000,\
		.clk_src_select[5] = 265333000,\
		.clk_src_select[6] = 199000000,\
		.clk_src_select[7] = 398000000,\
		.clk_src_select_num = 7,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_timer3,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 398000000,\
		.clk_src_select[5] = 216760000,\
		.clk_src_select[6] = 600000000,\
		.clk_src_select[7] = 245760000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_timer4,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 398000000,\
		.clk_src_select[5] = 216760000,\
		.clk_src_select[6] = 600000000,\
		.clk_src_select[7] = 245760000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_timer5,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 398000000,\
		.clk_src_select[5] = 216760000,\
		.clk_src_select[6] = 600000000,\
		.clk_src_select[7] = 245760000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_timer6,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 398000000,\
		.clk_src_select[5] = 216760000,\
		.clk_src_select[6] = 600000000,\
		.clk_src_select[7] = 245760000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_timer7,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 398000000,\
		.clk_src_select[5] = 216760000,\
		.clk_src_select[6] = 600000000,\
		.clk_src_select[7] = 245760000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_timer8,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 398000000,\
		.clk_src_select[5] = 216760000,\
		.clk_src_select[6] = 600000000,\
		.clk_src_select[7] = 245760000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_pwm3,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 398000000,\
		.clk_src_select[5] = 216760000,\
		.clk_src_select[6] = 600000000,\
		.clk_src_select[7] = 245760000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_pwm4,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 398000000,\
		.clk_src_select[5] = 216760000,\
		.clk_src_select[6] = 600000000,\
		.clk_src_select[7] = 245760000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_pwm5,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 398000000,\
		.clk_src_select[5] = 216760000,\
		.clk_src_select[6] = 600000000,\
		.clk_src_select[7] = 245760000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_pwm6,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 398000000,\
		.clk_src_select[5] = 216760000,\
		.clk_src_select[6] = 600000000,\
		.clk_src_select[7] = 245760000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_pwm7,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 398000000,\
		.clk_src_select[5] = 216760000,\
		.clk_src_select[6] = 600000000,\
		.clk_src_select[7] = 245760000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_pwm8,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 398000000,\
		.clk_src_select[5] = 216760000,\
		.clk_src_select[6] = 600000000,\
		.clk_src_select[7] = 245760000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_i2s_mclk2,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 199500000,\
		.clk_src_select[4] = 294912000,\
		.clk_src_select[5] = 270950000,\
		.clk_src_select[6] = 398000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 23,\
		.sel_clk = 12290000,\
	},\
	{\
		.slice_index = sec_ip_slice_i2s_mclk3,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 199500000,\
		.clk_src_select[4] = 294912000,\
		.clk_src_select[5] = 270950000,\
		.clk_src_select[6] = 398000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 23,\
		.sel_clk = 12290000,\
	},\
	{\
		.slice_index = sec_ip_slice_i2s_mc1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 199500000,\
		.clk_src_select[4] = 294912000,\
		.clk_src_select[5] = 270950000,\
		.clk_src_select[6] = 398000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 6,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_i2s_mc2,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 199500000,\
		.clk_src_select[4] = 294912000,\
		.clk_src_select[5] = 270950000,\
		.clk_src_select[6] = 398000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 6,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 398000000,\
	},\
	{\
		.slice_index = sec_ip_slice_i2s_sc3,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 199500000,\
		.clk_src_select[4] = 294912000,\
		.clk_src_select[5] = 270950000,\
		.clk_src_select[6] = 398000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 2,\
		.sel_clk = 98300000,\
	},\
	{\
		.slice_index = sec_ip_slice_i2s_sc4,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 199500000,\
		.clk_src_select[4] = 294912000,\
		.clk_src_select[5] = 270950000,\
		.clk_src_select[6] = 398000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 2,\
		.sel_clk = 98300000,\
	},\
	{\
		.slice_index = sec_ip_slice_i2s_sc5,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 199500000,\
		.clk_src_select[4] = 294912000,\
		.clk_src_select[5] = 270950000,\
		.clk_src_select[6] = 398000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 2,\
		.sel_clk = 98300000,\
	},\
	{\
		.slice_index = sec_ip_slice_i2s_sc6,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 199500000,\
		.clk_src_select[4] = 294912000,\
		.clk_src_select[5] = 270950000,\
		.clk_src_select[6] = 398000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 2,\
		.sel_clk = 98300000,\
	},\
	{\
		.slice_index = sec_ip_slice_i2s_sc7,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 199500000,\
		.clk_src_select[4] = 294912000,\
		.clk_src_select[5] = 270950000,\
		.clk_src_select[6] = 398000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 2,\
		.sel_clk = 98300000,\
	},\
	{\
		.slice_index = sec_ip_slice_i2s_sc8,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 199500000,\
		.clk_src_select[4] = 294912000,\
		.clk_src_select[5] = 270950000,\
		.clk_src_select[6] = 398000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 2,\
		.sel_clk = 98300000,\
	},\
	{\
		.slice_index = sec_ip_slice_csi_mclk1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 150000000,\
		.clk_src_select[5] = 368640000,\
		.clk_src_select[6] = 796000000,\
		.clk_src_select[7] = 500000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 150000000,\
	},\
	{\
		.slice_index = sec_ip_slice_csi_mclk2,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 150000000,\
		.clk_src_select[5] = 368640000,\
		.clk_src_select[6] = 796000000,\
		.clk_src_select[7] = 500000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 150000000,\
	},\
	{\
		.slice_index = sec_ip_slice_gic_4_5,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 500000000,\
		.clk_src_select[5] = 398000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 500000000,\
	},\
	{\
		.slice_index = sec_ip_slice_trace,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 333333000,\
		.clk_src_select[5] = 398000000,\
		.clk_src_select[6] = 600000000,\
		.clk_src_select[7] = 1000000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 1,\
		.sel_clk = 166670000,\
	},\
	{\
		.slice_index = sec_ip_slice_sys_cnt,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 1000000000,\
		.clk_src_select[5] = 796000000,\
		.clk_src_select[6] = 900000000,\
		.clk_src_select[7] = 294912000,\
		.clk_src_select_num = 2,\
		.pre_div = 0,\
		.post_div = 7,\
		.sel_clk = 3000000,\
	},\
	{\
		.slice_index = sec_ip_slice_mshc_timer,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select_num = 2,\
		.pre_div = 0,\
		.post_div = 23,\
		.sel_clk = 1000000,\
	},\
	{\
		.slice_index = sec_ip_slice_hpi_clk600,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 600000000,\
		.clk_src_select[5] = 900000000,\
		.clk_src_select[6] = 1000000000,\
		.clk_src_select[7] = 796000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 600000000,\
	},\
	{\
		.slice_index = sec_ip_slice_hpi_clk800,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 2000000,\
		.clk_src_select[2] = 24000000,\
		.clk_src_select[3] = 32000,\
		.clk_src_select[4] = 796000000,\
		.clk_src_select[5] = 900000000,\
		.clk_src_select[6] = 1000000000,\
		.clk_src_select[7] = 600000000,\
		.clk_src_select_num = 4,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 796000000,\
	},\
}

#define soc_core_slice {\
	{\
		.slice_index = soc_core_slice_cpu1a,\
		.clk_src_select_a[0] = 24000000,\
		.clk_src_select_a[1] = 24000000,\
		.clk_src_select_a[2] = 500000000,\
		.clk_src_select_a[3] = 400000000,\
		.clk_src_select_a_num = 3,\
		.clk_src_select_b[0] = 24000000,\
		.clk_src_select_b[1] = 24000000,\
		.clk_src_select_b[2] = 500000000,\
		.clk_src_select_b[3] = 400000000,\
		.clk_src_select_b_num = 3,\
		.clk_a_b_select = 0,\
		.post_div = 0,\
		.a_sel_clk = 400000000,\
		.b_sel_clk = 400000000,\
	},\
	{\
		.slice_index = soc_core_slice_cpu1b,\
		.clk_src_select_a[0] = 24000000,\
		.clk_src_select_a[1] = 24000000,\
		.clk_src_select_a[2] = 500000000,\
		.clk_src_select_a[3] = 400000000,\
		.clk_src_select_a_num = 3,\
		.clk_src_select_b[0] = 24000000,\
		.clk_src_select_b[1] = 24000000,\
		.clk_src_select_b[2] = 500000000,\
		.clk_src_select_b[3] = 400000000,\
		.clk_src_select_b_num = 3,\
		.clk_a_b_select = 0,\
		.post_div = 0,\
		.a_sel_clk = 400000000,\
		.b_sel_clk = 400000000,\
	},\
	{\
		.slice_index = soc_core_slice_gpu1,\
		.clk_src_select_a[0] = 24000000,\
		.clk_src_select_a[1] = 24000000,\
		.clk_src_select_a[2] = 500000000,\
		.clk_src_select_a[3] = 400000000,\
		.clk_src_select_a_num = 3,\
		.clk_src_select_b[0] = 24000000,\
		.clk_src_select_b[1] = 24000000,\
		.clk_src_select_b[2] = 500000000,\
		.clk_src_select_b[3] = 400000000,\
		.clk_src_select_b_num = 3,\
		.clk_a_b_select = 0,\
		.post_div = 0,\
		.a_sel_clk = 400000000,\
		.b_sel_clk = 400000000,\
	},\
	{\
		.slice_index = soc_core_slice_gpu2,\
		.clk_src_select_a[0] = 24000000,\
		.clk_src_select_a[1] = 24000000,\
		.clk_src_select_a[2] = 500000000,\
		.clk_src_select_a[3] = 400000000,\
		.clk_src_select_a_num = 3,\
		.clk_src_select_b[0] = 24000000,\
		.clk_src_select_b[1] = 24000000,\
		.clk_src_select_b[2] = 500000000,\
		.clk_src_select_b[3] = 400000000,\
		.clk_src_select_b_num = 3,\
		.clk_a_b_select = 0,\
		.post_div = 0,\
		.a_sel_clk = 400000000,\
		.b_sel_clk = 400000000,\
	},\
	{\
		.slice_index = soc_core_slice_ddr,\
		.clk_src_select_a[0] = 24000000,\
		.clk_src_select_a[1] = 24000000,\
		.clk_src_select_a[2] = 500000000,\
		.clk_src_select_a[3] = 400000000,\
		.clk_src_select_a_num = 3,\
		.clk_src_select_b[0] = 24000000,\
		.clk_src_select_b[1] = 24000000,\
		.clk_src_select_b[2] = 500000000,\
		.clk_src_select_b[3] = 400000000,\
		.clk_src_select_b_num = 3,\
		.clk_a_b_select = 0,\
		.post_div = 0,\
		.a_sel_clk = 400000000,\
		.b_sel_clk = 400000000,\
	},\
}

#define soc_ip_slice {\
	{\
		.slice_index = soc_ip_slice_vpu1,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 24000000,\
		.clk_src_select[2] = 500000000,\
		.clk_src_select[3] = 400000000,\
		.clk_src_select_num = 3,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 400000000,\
	},\
	{\
		.slice_index = soc_ip_slice_mjpeg,\
		.clk_src_select[0] = 24000000,\
		.clk_src_select[1] = 24000000,\
		.clk_src_select[2] = 500000000,\
		.clk_src_select[3] = 400000000,\
		.clk_src_select_num = 3,\
		.pre_div = 0,\
		.post_div = 0,\
		.sel_clk = 400000000,\
	},\
}

#define soc_bus_slice {\
	{\
		.slice_index = soc_bus_slice_vpu_bus,\
		.clk_src_select_a[0] = 24000000,\
		.clk_src_select_a[1] = 24000000,\
		.clk_src_select_a[2] = 500000000,\
		.clk_src_select_a[3] = 400000000,\
		.clk_src_select_a_num = 3,\
		.clk_src_select_b[0] = 24000000,\
		.clk_src_select_b[1] = 24000000,\
		.clk_src_select_b[2] = 500000000,\
		.clk_src_select_b[3] = 400000000,\
		.clk_src_select_b_num = 3,\
		.clk_a_b_select = 0,\
		.pre_div_a = 0,\
		.pre_div_b = 0,\
		.post_div = 0,\
		.m_div = 0,\
		.n_div = 0,\
		.p_div = 0,\
		.q_div = 0,\
		.a_sel_clk = 400000000,\
		.b_sel_clk = 400000000,\
	},\
	{\
		.slice_index = soc_bus_slice_vsn_bus,\
		.clk_src_select_a[0] = 24000000,\
		.clk_src_select_a[1] = 24000000,\
		.clk_src_select_a[2] = 500000000,\
		.clk_src_select_a[3] = 400000000,\
		.clk_src_select_a_num = 3,\
		.clk_src_select_b[0] = 24000000,\
		.clk_src_select_b[1] = 24000000,\
		.clk_src_select_b[2] = 500000000,\
		.clk_src_select_b[3] = 400000000,\
		.clk_src_select_b_num = 3,\
		.clk_a_b_select = 0,\
		.pre_div_a = 0,\
		.pre_div_b = 0,\
		.post_div = 0,\
		.m_div = 0,\
		.n_div = 0,\
		.p_div = 0,\
		.q_div = 0,\
		.a_sel_clk = 400000000,\
		.b_sel_clk = 400000000,\
	},\
	{\
		.slice_index = soc_bus_slice_noc,\
		.clk_src_select_a[0] = 24000000,\
		.clk_src_select_a[1] = 24000000,\
		.clk_src_select_a[2] = 500000000,\
		.clk_src_select_a[3] = 400000000,\
		.clk_src_select_a[4] = 1896000000,\
		.clk_src_select_a_num = 4,\
		.clk_src_select_b[0] = 24000000,\
		.clk_src_select_b[1] = 24000000,\
		.clk_src_select_b[2] = 500000000,\
		.clk_src_select_b[3] = 400000000,\
		.clk_src_select_b[4] = 1896000000,\
		.clk_src_select_b_num = 4,\
		.clk_a_b_select = 0,\
		.pre_div_a = 0,\
		.pre_div_b = 0,\
		.post_div = 1,\
		.m_div = 1,\
		.n_div = 5,\
		.p_div = 5,\
		.q_div = 7,\
		.a_sel_clk = 948000000,\
		.b_sel_clk = 948000000,\
	},\
	{\
		.slice_index = soc_bus_slice_his_bus,\
		.clk_src_select_a[0] = 24000000,\
		.clk_src_select_a[1] = 24000000,\
		.clk_src_select_a[2] = 500000000,\
		.clk_src_select_a[3] = 400000000,\
		.clk_src_select_a_num = 3,\
		.clk_src_select_b[0] = 24000000,\
		.clk_src_select_b[1] = 24000000,\
		.clk_src_select_b[2] = 500000000,\
		.clk_src_select_b[3] = 400000000,\
		.clk_src_select_b_num = 3,\
		.clk_a_b_select = 0,\
		.pre_div_a = 0,\
		.pre_div_b = 0,\
		.post_div = 0,\
		.m_div = 0,\
		.n_div = 0,\
		.p_div = 0,\
		.q_div = 0,\
		.a_sel_clk = 400000000,\
		.b_sel_clk = 400000000,\
	},\
}



#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif
