
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef PORT_CFG_H
#define PORT_CFG_H

#include "Port.h"

/*
 * Container : PortPinConfiguration
 */

/*
Configuration Options: Physical pin level
-LOW  (Low Volatage Level)
-HIGH (High Voltage Level)
*/

#define PORT_PIN_LEVEL_LOW    (0x00U)
#define PORT_PIN_LEVEL_HIGH   (0x01U)

/*
Configuration Options: Pin driver strength
*/

#define RFAST_PORT_PIN_STRONG_DRIVER_SHARP_EDGE  (0x0U)
#define RFAST_PORT_PIN_STRONG_DRIVER_MEDIUM_EDGE (0x1U)
#define RFAST_PORT_PIN_MEDIUM_DRIVER             (0x2U)
#define RFAST_PORT_PIN_RGMII_DRIVER              (0x3U)
#define RFAST_PORT_PIN_DEFAULT_DRIVER            (0x0U)

#define FAST_PORT_PIN_STRONG_DRIVER_SHARP_EDGE  (0x0U)
#define FAST_PORT_PIN_STRONG_DRIVER_MEDIUM_EDGE (0x1U)
#define FAST_PORT_PIN_MEDIUM_DRIVER             (0x2U)
#define FAST_PORT_PIN_DEFAULT_DRIVER            (0x0U)

#define SLOW_PORT_PIN_MEDIUM_DRIVER_SHARP_EDGE  (0x0U)
#define SLOW_PORT_PIN_MEDIUM_DRIVER             (0x1U)
#define SLOW_PORT_PIN_DEFAULT_DRIVER            (0x0U)

/* Pin driver strength value for the non available pins*/

#define  PORT_PIN_PAD_STRENGTH_DEFAULT      (0x0U)
#define  PORT_PIN_PAD_LEVEL_DEFAULT         (0x0U)
#define  PORT_PIN_PAD_DEFAULT               (0x0U)

/*
Configuration Options: Pin Pad Level
*/

#define PORT_INPUT_LEVEL_CMOS_AUTOMOTIVE  (0x0U)
#define PORT_INPUT_LEVEL_TTL_3_3V       (0xCU)
#define PORT_INPUT_LEVEL_TTL_5_0V       (0x8U)

#define PORT_GPIO_DUMMY			(0xEFFFFFFFU)
#define PORT_PIN_LEVEL_DUMMY    PORT_PIN_LEVEL_LOW
#define PORT_PIN_MODE_DUMMY    PORT_PIN_MODE_NOT_CHANGEABLE

#define PORT_PAD_MMC_SP_40_OHM (0xC << 20)
#define PORT_PAD_MMC_SN_40_OHM (0xC << 16)

#define PORT_GPIO_IS_IN (0x0 << 16)
#define PORT_GPIO_IS_OUT (0x1 << 16)

static const Port_n_ConfigType Port_kConfiguration[] = {
    /*                              Port0                       */
    {
        {
            /* IO_PAD_CONFIG: Port Parametric output, input select, slew rate, driver select, pull enable configuration */

            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_PULL_UP ),/*AD21, Pin 0, OSPI1_SCLK, Mux:OSPI1.SCLK*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_PULL_UP ),/*AB21, Pin 1, OSPI1_SS0, Mux:OSPI1.SS0*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_NO_PULL ),/*AE22, Pin 2, OSPI1_DATA0, Mux:OSPI1.DATA0*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_NO_PULL ),/*AE24, Pin 3, OSPI1_DATA1, Mux:OSPI1.DATA1*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_NO_PULL ),/*AD23, Pin 4, OSPI1_DATA2, Mux:OSPI1.DATA2*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_NO_PULL ),/*AE23, Pin 5, OSPI1_DATA3, Mux:OSPI1.DATA3*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_NO_PULL ),/*AD25, Pin 6, OSPI1_DATA4, Mux:OSPI1.DATA4*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_NO_PULL ),/*AD24, Pin 7, OSPI1_DATA5, Mux:OSPI1.DATA5*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_NO_PULL ),/*AD22, Pin 8, OSPI1_DATA6, Mux:OSPI1.DATA6*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_NO_PULL ),/*AB20, Pin 9, OSPI1_DATA7, Mux:OSPI1.DATA7*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_PULL_DOWN ),/*AC21, Pin 10, OSPI1_DQS, Mux:OSPI1.DQS*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_PULL_UP ),/*AC20, Pin 11, OSPI1_SS1, Mux:OSPI1.SS1*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*AD14, Pin 12, GPIO_E0, Mux:I2S_SC1.SCK*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*AD15, Pin 13, GPIO_E1, Mux:I2S_SC1.SDO/SDI*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*AE15, Pin 14, GPIO_E2, Mux:I2S_SC1.SDI/SDO*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*AE16, Pin 15, GPIO_E3, Mux:I2S_SC1.WS*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*AD17, Pin 16, GPIO_E4, Mux:SPI2.SCLK*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*AD16, Pin 17, GPIO_E5, Mux:SPI2.MISO*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*AD18, Pin 18, GPIO_E6, Mux:SPI2.MOSI*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*AE18, Pin 19, GPIO_E7, Mux:SPI2.SS*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*AE19, Pin 20, GPIO_E8, Mux:GPIO.IO44*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*AD19, Pin 21, GPIO_E9, Mux:GPIO.IO45*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*AD20, Pin 22, GPIO_E10, Mux:I2C4.SCL*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*AE20, Pin 23, GPIO_E11, Mux:I2C4.SDA*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*AA20, Pin 24, GPIO_A0, Mux:GPIO.IO0*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*Y19, Pin 25, GPIO_A1, Mux:GPIO.IO1*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*AB19, Pin 26, GPIO_A2, Mux:GPIO.IO2*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*AC18, Pin 27, GPIO_A3, Mux:GPIO.IO3*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*AC19, Pin 28, GPIO_A4, Mux:UART1.TX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*AB18, Pin 29, GPIO_A5, Mux:UART1.RX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*AA18, Pin 30, GPIO_A6, Mux:UART2.TX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*AA19, Pin 31, GPIO_A7, Mux:UART2.RX*/

        },

        {
            /* PIN_MUX_CONFIG: Port pins mode, open drain configuration */

            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__1 | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 0*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 1*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 2*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 3*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 4*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 5*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 6*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 7*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 8*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 9*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__1 | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 10*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 11*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 12*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 13*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 14*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 15*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 16*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 17*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 18*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 19*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 20*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 21*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 22*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 23*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 24*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 25*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 26*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 27*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 28*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 29*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 30*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 31*/
        },

        {
            /* GPIO Controller id for each pin */
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 0, OSPI1_SCLK*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 1, OSPI1_SS0*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 2, OSPI1_DATA0*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 3, OSPI1_DATA1*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 4, OSPI1_DATA2*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 5, OSPI1_DATA3*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 6, OSPI1_DATA4*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 7, OSPI1_DATA5*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 8, OSPI1_DATA6*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 9, OSPI1_DATA7*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 10, OSPI1_DQS*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 11, OSPI1_SS1*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 12, GPIO_E0*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 13, GPIO_E1*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 14, GPIO_E2*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 15, GPIO_E3*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 16, GPIO_E4*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 17, GPIO_E5*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 18, GPIO_E6*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 19, GPIO_E7*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 20, GPIO_E8*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 21, GPIO_E9*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 22, GPIO_E10*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 23, GPIO_E11*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 24, GPIO_A0*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 25, GPIO_A1*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 26, GPIO_A2*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 27, GPIO_A3*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_1 ),/*Pin 28, GPIO_A4*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 29, GPIO_A5*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 30, GPIO_A6*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 31, GPIO_A7*/

        },

        {
            /* Port pins initial level configuration */

            PORT_PIN_LEVEL_LOW,/* Pin 0 */
            PORT_PIN_LEVEL_LOW,/* Pin 1 */
            PORT_PIN_LEVEL_LOW,/* Pin 2 */
            PORT_PIN_LEVEL_LOW,/* Pin 3 */
            PORT_PIN_LEVEL_LOW,/* Pin 4 */
            PORT_PIN_LEVEL_LOW,/* Pin 5 */
            PORT_PIN_LEVEL_LOW,/* Pin 6 */
            PORT_PIN_LEVEL_LOW,/* Pin 7 */
            PORT_PIN_LEVEL_LOW,/* Pin 8 */
            PORT_PIN_LEVEL_LOW,/* Pin 9 */
            PORT_PIN_LEVEL_LOW,/* Pin 10 */
            PORT_PIN_LEVEL_LOW,/* Pin 11 */
            PORT_PIN_LEVEL_LOW,/* Pin 12 */
            PORT_PIN_LEVEL_LOW,/* Pin 13 */
            PORT_PIN_LEVEL_LOW,/* Pin 14 */
            PORT_PIN_LEVEL_LOW,/* Pin 15 */
            PORT_PIN_LEVEL_LOW,/* Pin 16 */
            PORT_PIN_LEVEL_LOW,/* Pin 17 */
            PORT_PIN_LEVEL_LOW,/* Pin 18 */
            PORT_PIN_LEVEL_LOW,/* Pin 19 */
            PORT_PIN_LEVEL_LOW,/* Pin 20 */
            PORT_PIN_LEVEL_LOW,/* Pin 21 */
            PORT_PIN_LEVEL_LOW,/* Pin 22 */
            PORT_PIN_LEVEL_LOW,/* Pin 23 */
            PORT_PIN_LEVEL_LOW,/* Pin 24 */
            PORT_PIN_LEVEL_LOW,/* Pin 25 */
            PORT_PIN_LEVEL_LOW,/* Pin 26 */
            PORT_PIN_LEVEL_LOW,/* Pin 27 */
            PORT_PIN_LEVEL_LOW,/* Pin 28 */
            PORT_PIN_LEVEL_LOW,/* Pin 29 */
            PORT_PIN_LEVEL_LOW,/* Pin 30 */
            PORT_PIN_LEVEL_LOW,/* Pin 31 */

        },

        {/* Port pin run time mode changeable or not configuration */

            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 0 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 1 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 2 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 3 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 4 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 5 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 6 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 7 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 8 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 9 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 10 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 11 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 12 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 13 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 14 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 15 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 16 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 17 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 18 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 19 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 20 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 21 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 22 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 23 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 24 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 25 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 26 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 27 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 28 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 29 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 30 */
            PORT_PIN_MODE_NOT_CHANGEABLE /* Pin 31 */

        },

    },  // end of PORT0

    /*                              Port1                       */
    {
        {
            /* IO_PAD_CONFIG: Port Parametric output, input select, slew rate, driver select, pull enable configuration */

            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*AC16, Pin 32, GPIO_A8, Mux:UART3.TX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*AC17, Pin 33, GPIO_A9, Mux:UART3.RX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Y18, Pin 34, GPIO_A10, Mux:PWM1.CH0*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*AB16, Pin 35, GPIO_A11, Mux:GPIO.IO11*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__SLOW | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*AA17, Pin 36, GPIO_B0, Mux:CANFD1.TX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Y17, Pin 37, GPIO_B1, Mux:CANFD1.RX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__SLOW | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*Y16, Pin 38, GPIO_B2, Mux:CANFD2.TX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*AA16, Pin 39, GPIO_B3, Mux:CANFD2.RX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*Y15, Pin 40, GPIO_B4, Mux:GPIO.IO16*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*AA15, Pin 41, GPIO_B5, Mux:OSPI1.RESET*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*AB15, Pin 42, GPIO_B6, Mux:UART6.TX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*AC15, Pin 43, GPIO_B7, Mux:UART6.RX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Y14, Pin 44, GPIO_B8, Mux:GPIO.IO20*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*AA14, Pin 45, GPIO_B9, Mux:GPIO.IO21*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*AB14, Pin 46, GPIO_B10, Mux:UART8.TX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*AC14, Pin 47, GPIO_B11, Mux:UART8.RX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*G7, Pin 48, GPIO_C0, Mux:GPIO.IO48*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*F7, Pin 49, GPIO_C1, Mux:GPIO.IO49*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*F9, Pin 50, GPIO_C2, Mux:I2C6.SCL*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*F8, Pin 51, GPIO_C3, Mux:I2C6.SDA*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*E8, Pin 52, GPIO_C4, Mux:UART9.TX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*E11, Pin 53, GPIO_C5, Mux:UART9.RX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*E10, Pin 54, GPIO_C6, Mux:UART10.TX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*F10, Pin 55, GPIO_C7, Mux:UART10.RX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*F12, Pin 56, GPIO_C8, Mux:PWM3.CH0*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*G10, Pin 57, GPIO_C9, Mux:PWM3.CH1*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*E12, Pin 58, GPIO_C10, Mux:GPIO.IO58*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*F13, Pin 59, GPIO_C11, Mux:GPIO.IO59*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*F11, Pin 60, GPIO_C12, Mux:GPIO.IO60*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*G8, Pin 61, GPIO_C13, Mux:GPIO.IO61*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*G9, Pin 62, GPIO_C14, Mux:PCIE1.PERST_N*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*G11, Pin 63, GPIO_C15, Mux:PCIE1.WAKE_N*/

        },

        {
            /* PIN_MUX_CONFIG: Port pins mode, open drain configuration */

            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 32*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 33*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 34*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_OPENDRAIN | PORT_PIN_MODE_GPIO),/*Pin 35*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 36*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 37*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 38*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 39*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_OPENDRAIN | PORT_PIN_MODE_GPIO),/*Pin 40*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 41*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 42*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 43*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 44*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 45*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 46*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 47*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 48*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 49*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 50*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 51*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 52*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 53*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 54*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 55*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT6),/*Pin 56*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT6),/*Pin 57*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 58*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 59*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 60*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 61*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT5),/*Pin 62*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT5),/*Pin 63*/
        },

        {
            /* GPIO Controller id for each pin */
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_1 ),/*Pin 32, GPIO_A8*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 33, GPIO_A9*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 34, GPIO_A10*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_1 ),/*Pin 35, GPIO_A11*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 36, GPIO_B0*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 37, GPIO_B1*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 38, GPIO_B2*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 39, GPIO_B3*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_1 ),/*Pin 40, GPIO_B4*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 41, GPIO_B5*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 42, GPIO_B6*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 43, GPIO_B7*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 44, GPIO_B8*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 45, GPIO_B9*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_1 ),/*Pin 46, GPIO_B10*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_1 ),/*Pin 47, GPIO_B11*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_3 ),/*Pin 48, GPIO_C0*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_3 ),/*Pin 49, GPIO_C1*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 50, GPIO_C2*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 51, GPIO_C3*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 52, GPIO_C4*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 53, GPIO_C5*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 54, GPIO_C6*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 55, GPIO_C7*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 56, GPIO_C8*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 57, GPIO_C9*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 58, GPIO_C10*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 59, GPIO_C11*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 60, GPIO_C12*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_5 ),/*Pin 61, GPIO_C13*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 62, GPIO_C14*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 63, GPIO_C15*/

        },

        {
            /* Port pins initial level configuration */

            PORT_PIN_LEVEL_LOW,/* Pin 32 */
            PORT_PIN_LEVEL_LOW,/* Pin 33 */
            PORT_PIN_LEVEL_LOW,/* Pin 34 */
            PORT_PIN_LEVEL_LOW,/* Pin 35 */
            PORT_PIN_LEVEL_LOW,/* Pin 36 */
            PORT_PIN_LEVEL_LOW,/* Pin 37 */
            PORT_PIN_LEVEL_LOW,/* Pin 38 */
            PORT_PIN_LEVEL_LOW,/* Pin 39 */
            PORT_PIN_LEVEL_LOW,/* Pin 40 */
            PORT_PIN_LEVEL_LOW,/* Pin 41 */
            PORT_PIN_LEVEL_LOW,/* Pin 42 */
            PORT_PIN_LEVEL_LOW,/* Pin 43 */
            PORT_PIN_LEVEL_LOW,/* Pin 44 */
            PORT_PIN_LEVEL_LOW,/* Pin 45 */
            PORT_PIN_LEVEL_LOW,/* Pin 46 */
            PORT_PIN_LEVEL_LOW,/* Pin 47 */
            PORT_PIN_LEVEL_LOW,/* Pin 48 */
            PORT_PIN_LEVEL_LOW,/* Pin 49 */
            PORT_PIN_LEVEL_LOW,/* Pin 50 */
            PORT_PIN_LEVEL_LOW,/* Pin 51 */
            PORT_PIN_LEVEL_LOW,/* Pin 52 */
            PORT_PIN_LEVEL_LOW,/* Pin 53 */
            PORT_PIN_LEVEL_LOW,/* Pin 54 */
            PORT_PIN_LEVEL_LOW,/* Pin 55 */
            PORT_PIN_LEVEL_LOW,/* Pin 56 */
            PORT_PIN_LEVEL_LOW,/* Pin 57 */
            PORT_PIN_LEVEL_LOW,/* Pin 58 */
            PORT_PIN_LEVEL_LOW,/* Pin 59 */
            PORT_PIN_LEVEL_HIGH,/* Pin 60 */
            PORT_PIN_LEVEL_LOW,/* Pin 61 */
            PORT_PIN_LEVEL_LOW,/* Pin 62 */
            PORT_PIN_LEVEL_LOW,/* Pin 63 */

        },

        {/* Port pin run time mode changeable or not configuration */

            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 32 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 33 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 34 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 35 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 36 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 37 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 38 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 39 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 40 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 41 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 42 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 43 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 44 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 45 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 46 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 47 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 48 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 49 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 50 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 51 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 52 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 53 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 54 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 55 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 56 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 57 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 58 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 59 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 60 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 61 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 62 */
            PORT_PIN_MODE_NOT_CHANGEABLE /* Pin 63 */

        },

    },  // end of PORT1

    /*                              Port2                       */
    {
        {
            /* IO_PAD_CONFIG: Port Parametric output, input select, slew rate, driver select, pull enable configuration */

            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*G1, Pin 64, GPIO_D0, Mux:I2C9.SCL*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*F1, Pin 65, GPIO_D1, Mux:I2C9.SDA*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*G2, Pin 66, GPIO_D2, Mux:I2C10.SCL*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*F2, Pin 67, GPIO_D3, Mux:I2C10.SDA*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*G3, Pin 68, GPIO_D4, Mux:GPIO.IO68*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*F3, Pin 69, GPIO_D5, Mux:GPIO.IO69*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*G4, Pin 70, GPIO_D6, Mux:I2C12.SCL*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*F4, Pin 71, GPIO_D7, Mux:I2C12.SDA*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*G5, Pin 72, GPIO_D8, Mux:PWM5.CH0*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*F5, Pin 73, GPIO_D9, Mux:GPIO.IO73*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*E2, Pin 74, GPIO_D10, Mux:ETHERNET.MDIO*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*E3, Pin 75, GPIO_D11, Mux:ETHERNET.MDC*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*E6, Pin 76, GPIO_D12, Mux:PCIE1.CLKREQ_N*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_PULL_UP ),/*E5, Pin 77, GPIO_D13, Mux:GPIO.IO77*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__SLOW | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*F6, Pin 78, GPIO_D14, Mux:I2C16.SCL*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__SLOW | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*E7, Pin 79, GPIO_D15, Mux:I2C16.SDA*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*P1, Pin 80, OSPI2_SCLK, Mux:CSI.CH0_CLK*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*J2, Pin 81, OSPI2_SS0, Mux:CSI.CH0_HSYNC*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*H1, Pin 82, OSPI2_DATA0, Mux:CSI.CH0_VSYNC*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*H2, Pin 83, OSPI2_DATA1, Mux:GPIO.IO83*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*N2, Pin 84, OSPI2_DATA2, Mux:CSI.CH0_DATA0*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_PULL_DOWN ),/*N1, Pin 85, OSPI2_DATA3, Mux:GPIO.IO85*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*M2, Pin 86, OSPI2_DATA4, Mux:UART11.TX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*L1, Pin 87, OSPI2_DATA5, Mux:UART11.RX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*L2, Pin 88, OSPI2_DATA6, Mux:CSI.CH0_DATA4*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*K2, Pin 89, OSPI2_DATA7, Mux:CSI.CH0_DATA5*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*K1, Pin 90, OSPI2_DQS, Mux:CSI.CH0_DATA6*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*P2, Pin 91, OSPI2_SS1, Mux:CSI.CH0_DATA7*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*K3, Pin 92, RGMII_TXC, Mux:ETHERNET.TXC*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*L4, Pin 93, RGMII_TXD0, Mux:ETHERNET.TXD0*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*K4, Pin 94, RGMII_TXD1, Mux:ETHERNET.TXD1*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*H4, Pin 95, RGMII_TXD2, Mux:ETHERNET.TXD2*/

        },

        {
            /* PIN_MUX_CONFIG: Port pins mode, open drain configuration */

            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 64*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 65*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 66*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 67*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 68*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 69*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 70*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 71*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT6),/*Pin 72*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 73*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 74*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 75*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__1 | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT5),/*Pin 76*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__1 | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 77*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 78*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 79*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT5),/*Pin 80*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT5),/*Pin 81*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT5),/*Pin 82*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 83*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT5),/*Pin 84*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 85*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 86*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 87*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT5),/*Pin 88*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT5),/*Pin 89*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT5),/*Pin 90*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT5),/*Pin 91*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 92*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 93*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 94*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 95*/
        },

        {
            /* GPIO Controller id for each pin */
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 64, GPIO_D0*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 65, GPIO_D1*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 66, GPIO_D2*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 67, GPIO_D3*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 68, GPIO_D4*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 69, GPIO_D5*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 70, GPIO_D6*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 71, GPIO_D7*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_2 ),/*Pin 72, GPIO_D8*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 73, GPIO_D9*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 74, GPIO_D10*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 75, GPIO_D11*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 76, GPIO_D12*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 77, GPIO_D13*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 78, GPIO_D14*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 79, GPIO_D15*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 80, OSPI2_SCLK*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 81, OSPI2_SS0*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 82, OSPI2_DATA0*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 83, OSPI2_DATA1*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 84, OSPI2_DATA2*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 85, OSPI2_DATA3*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 86, OSPI2_DATA4*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 87, OSPI2_DATA5*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 88, OSPI2_DATA6*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 89, OSPI2_DATA7*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 90, OSPI2_DQS*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 91, OSPI2_SS1*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 92, RGMII_TXC*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 93, RGMII_TXD0*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 94, RGMII_TXD1*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 95, RGMII_TXD2*/

        },

        {
            /* Port pins initial level configuration */

            PORT_PIN_LEVEL_LOW,/* Pin 64 */
            PORT_PIN_LEVEL_LOW,/* Pin 65 */
            PORT_PIN_LEVEL_LOW,/* Pin 66 */
            PORT_PIN_LEVEL_LOW,/* Pin 67 */
            PORT_PIN_LEVEL_LOW,/* Pin 68 */
            PORT_PIN_LEVEL_HIGH,/* Pin 69 */
            PORT_PIN_LEVEL_LOW,/* Pin 70 */
            PORT_PIN_LEVEL_LOW,/* Pin 71 */
            PORT_PIN_LEVEL_HIGH,/* Pin 72 */
            PORT_PIN_LEVEL_HIGH,/* Pin 73 */
            PORT_PIN_LEVEL_LOW,/* Pin 74 */
            PORT_PIN_LEVEL_LOW,/* Pin 75 */
            PORT_PIN_LEVEL_HIGH,/* Pin 76 */
            PORT_PIN_LEVEL_HIGH,/* Pin 77 */
            PORT_PIN_LEVEL_LOW,/* Pin 78 */
            PORT_PIN_LEVEL_HIGH,/* Pin 79 */
            PORT_PIN_LEVEL_LOW,/* Pin 80 */
            PORT_PIN_LEVEL_LOW,/* Pin 81 */
            PORT_PIN_LEVEL_LOW,/* Pin 82 */
            PORT_PIN_LEVEL_LOW,/* Pin 83 */
            PORT_PIN_LEVEL_LOW,/* Pin 84 */
            PORT_PIN_LEVEL_LOW,/* Pin 85 */
            PORT_PIN_LEVEL_LOW,/* Pin 86 */
            PORT_PIN_LEVEL_LOW,/* Pin 87 */
            PORT_PIN_LEVEL_LOW,/* Pin 88 */
            PORT_PIN_LEVEL_LOW,/* Pin 89 */
            PORT_PIN_LEVEL_LOW,/* Pin 90 */
            PORT_PIN_LEVEL_LOW,/* Pin 91 */
            PORT_PIN_LEVEL_LOW,/* Pin 92 */
            PORT_PIN_LEVEL_LOW,/* Pin 93 */
            PORT_PIN_LEVEL_LOW,/* Pin 94 */
            PORT_PIN_LEVEL_LOW,/* Pin 95 */

        },

        {/* Port pin run time mode changeable or not configuration */

            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 64 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 65 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 66 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 67 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 68 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 69 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 70 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 71 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 72 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 73 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 74 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 75 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 76 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 77 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 78 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 79 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 80 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 81 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 82 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 83 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 84 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 85 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 86 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 87 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 88 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 89 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 90 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 91 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 92 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 93 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 94 */
            PORT_PIN_MODE_NOT_CHANGEABLE /* Pin 95 */

        },

    },  // end of PORT2

    /*                              Port3                       */
    {
        {
            /* IO_PAD_CONFIG: Port Parametric output, input select, slew rate, driver select, pull enable configuration */

            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*J3, Pin 96, RGMII_TXD3, Mux:ETHERNET.TXD3*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*H3, Pin 97, RGMII_TX_CTL, Mux:ETHERNET.TXEN*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*P3, Pin 98, RGMII_RXC, Mux:ETHERNET.RXC*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*N3, Pin 99, RGMII_RXD0, Mux:ETHERNET.RXD0*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*M4, Pin 100, RGMII_RXD1, Mux:ETHERNET.RXD1*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*L3, Pin 101, RGMII_RXD2, Mux:ETHERNET.RXD2*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*M3, Pin 102, RGMII_RXD3, Mux:ETHERNET.RXD3*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*P4, Pin 103, RGMII_RX_CTL, Mux:ETHERNET.RXDV*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*V6, Pin 104, I2S_SC3_SCK, Mux:I2S_SC3.SCK*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*V5, Pin 105, I2S_SC3_WS, Mux:I2S_SC3.WS*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*U5, Pin 106, I2S_SC3_SD, Mux:I2S_SC3.SDO/SDI*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*T5, Pin 107, I2S_SC4_SCK, Mux:GPIO.IO107*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*T6, Pin 108, I2S_SC4_WS, Mux:GPIO.IO108*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*U6, Pin 109, I2S_SC4_SD, Mux:I2S_SC3.SDI/SDO*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*M6, Pin 110, I2S_SC5_SCK, Mux:I2C14.SCL*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*N5, Pin 111, I2S_SC5_WS, Mux:I2C14.SDA*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*M5, Pin 112, I2S_SC5_SD, Mux:I2S_SC6.SDI/SDO*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*L5, Pin 113, I2S_SC6_SCK, Mux:I2S_SC6.SCK*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*K6, Pin 114, I2S_SC6_WS, Mux:GPIO.IO114*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*L6, Pin 115, I2S_SC6_SD, Mux:I2S_SC6.SDO/SDI*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*J6, Pin 116, I2S_SC7_SCK, Mux:CKGEN_AP.I2S_MCLK2*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*K5, Pin 117, I2S_SC7_WS, Mux:CKGEN_AP.I2S_MCLK3*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*J5, Pin 118, I2S_SC7_SD, Mux:UART15.TX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*H5, Pin 119, I2S_SC8_SCK, Mux:I2C15.SCL*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*G6, Pin 120, I2S_SC8_WS, Mux:I2C15.SDA*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*H6, Pin 121, I2S_SC8_SD, Mux:UART15.RX*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*V7, Pin 122, I2S_MC_SCK, Mux:I2S_SC8.SCK*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*W7, Pin 123, I2S_MC_WS, Mux:I2S_SC8.WS*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Y7, Pin 124, I2S_MC_SD0, Mux:I2S_SC8.SDI/SDO*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*W8, Pin 125, I2S_MC_SD1, Mux:I2S_SC8.SDO/SDI*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*W5, Pin 126, I2S_MC_SD2, Mux:CKGEN_AP.CSI_MCLK1*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*W6, Pin 127, I2S_MC_SD3, Mux:TMR7.CH3*/

        },

        {
            /* PIN_MUX_CONFIG: Port pins mode, open drain configuration */

            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 96*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 97*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 98*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 99*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 100*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 101*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 102*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 103*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 104*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 105*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 106*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 107*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 108*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 109*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 110*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 111*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 112*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 113*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 114*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 115*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 116*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 117*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 118*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 119*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 120*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 121*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 122*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 123*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 124*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 125*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT6),/*Pin 126*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 127*/
        },

        {
            /* GPIO Controller id for each pin */
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 96, RGMII_TXD3*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 97, RGMII_TX_CTL*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 98, RGMII_RXC*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 99, RGMII_RXD0*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 100, RGMII_RXD1*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 101, RGMII_RXD2*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 102, RGMII_RXD3*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 103, RGMII_RX_CTL*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 104, I2S_SC3_SCK*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 105, I2S_SC3_WS*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 106, I2S_SC3_SD*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 107, I2S_SC4_SCK*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 108, I2S_SC4_WS*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 109, I2S_SC4_SD*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_5 ),/*Pin 110, I2S_SC5_SCK*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_5 ),/*Pin 111, I2S_SC5_WS*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 112, I2S_SC5_SD*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 113, I2S_SC6_SCK*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 114, I2S_SC6_WS*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 115, I2S_SC6_SD*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 116, I2S_SC7_SCK*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 117, I2S_SC7_WS*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 118, I2S_SC7_SD*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_5 ),/*Pin 119, I2S_SC8_SCK*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_5 ),/*Pin 120, I2S_SC8_WS*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 121, I2S_SC8_SD*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 122, I2S_MC_SCK*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 123, I2S_MC_WS*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 124, I2S_MC_SD0*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 125, I2S_MC_SD1*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 126, I2S_MC_SD2*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_5 ),/*Pin 127, I2S_MC_SD3*/

        },

        {
            /* Port pins initial level configuration */

            PORT_PIN_LEVEL_LOW,/* Pin 96 */
            PORT_PIN_LEVEL_LOW,/* Pin 97 */
            PORT_PIN_LEVEL_LOW,/* Pin 98 */
            PORT_PIN_LEVEL_LOW,/* Pin 99 */
            PORT_PIN_LEVEL_LOW,/* Pin 100 */
            PORT_PIN_LEVEL_LOW,/* Pin 101 */
            PORT_PIN_LEVEL_LOW,/* Pin 102 */
            PORT_PIN_LEVEL_LOW,/* Pin 103 */
            PORT_PIN_LEVEL_LOW,/* Pin 104 */
            PORT_PIN_LEVEL_LOW,/* Pin 105 */
            PORT_PIN_LEVEL_LOW,/* Pin 106 */
            PORT_PIN_LEVEL_LOW,/* Pin 107 */
            PORT_PIN_LEVEL_LOW,/* Pin 108 */
            PORT_PIN_LEVEL_LOW,/* Pin 109 */
            PORT_PIN_LEVEL_LOW,/* Pin 110 */
            PORT_PIN_LEVEL_LOW,/* Pin 111 */
            PORT_PIN_LEVEL_LOW,/* Pin 112 */
            PORT_PIN_LEVEL_LOW,/* Pin 113 */
            PORT_PIN_LEVEL_LOW,/* Pin 114 */
            PORT_PIN_LEVEL_LOW,/* Pin 115 */
            PORT_PIN_LEVEL_LOW,/* Pin 116 */
            PORT_PIN_LEVEL_LOW,/* Pin 117 */
            PORT_PIN_LEVEL_LOW,/* Pin 118 */
            PORT_PIN_LEVEL_LOW,/* Pin 119 */
            PORT_PIN_LEVEL_LOW,/* Pin 120 */
            PORT_PIN_LEVEL_LOW,/* Pin 121 */
            PORT_PIN_LEVEL_LOW,/* Pin 122 */
            PORT_PIN_LEVEL_LOW,/* Pin 123 */
            PORT_PIN_LEVEL_LOW,/* Pin 124 */
            PORT_PIN_LEVEL_LOW,/* Pin 125 */
            PORT_PIN_LEVEL_LOW,/* Pin 126 */
            PORT_PIN_LEVEL_LOW,/* Pin 127 */

        },

        {/* Port pin run time mode changeable or not configuration */

            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 96 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 97 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 98 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 99 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 100 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 101 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 102 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 103 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 104 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 105 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 106 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 107 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 108 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 109 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 110 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 111 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 112 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 113 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 114 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 115 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 116 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 117 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 118 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 119 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 120 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 121 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 122 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 123 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 124 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 125 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 126 */
            PORT_PIN_MODE_NOT_CHANGEABLE /* Pin 127 */

        },

    },  // end of PORT3

    /*                              Port4                       */
    {
        {
            /* IO_PAD_CONFIG: Port Parametric output, input select, slew rate, driver select, pull enable configuration */

            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*Y5, Pin 128, I2S_MC_SD4, Mux:GPIO.IO128*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*AA5, Pin 129, I2S_MC_SD5, Mux:GPIO.IO129*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*AA6, Pin 130, I2S_MC_SD6, Mux:GPIO.IO130*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*AA4, Pin 131, I2S_MC_SD7, Mux:GPIO.IO131*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__MIN | PORT_PAD_MMC_TXPREP__MIN | PORT_PAD_MMC_TXPREN__MIN | PORT_PAD_MMC_PULL__DOWN ),/*R1, Pin 132, EMMC1_CLK, Mux:MSHC1.CLK*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*Y1, Pin 133, EMMC1_CMD, Mux:MSHC1.CMD*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*W2, Pin 134, EMMC1_DATA0, Mux:MSHC1.DATA_0*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*W1, Pin 135, EMMC1_DATA1, Mux:MSHC1.DATA_1*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*V2, Pin 136, EMMC1_DATA2, Mux:MSHC1.DATA_2*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*V1, Pin 137, EMMC1_DATA3, Mux:MSHC1.DATA_3*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*U2, Pin 138, EMMC1_DATA4, Mux:MSHC1.DATA_4*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*T1, Pin 139, EMMC1_DATA5, Mux:MSHC1.DATA_5*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*T2, Pin 140, EMMC1_DATA6, Mux:MSHC1.DATA_6*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*R2, Pin 141, EMMC1_DATA7, Mux:MSHC1.DATA_7*/
            ((uint32_t)PORT_PAD_MMC_SP__MIN | PORT_PAD_MMC_SN__MIN | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__DOWN ),/*Y2, Pin 142, EMMC1_STROBE, Mux:MSHC1.STB*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*AA1, Pin 143, EMMC1_RESET_N, Mux:MSHC1.RST_N*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__DOWN ),/*V4, Pin 144, EMMC2_CLK, Mux:MSHC2.CLK*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*Y4, Pin 145, EMMC2_CMD, Mux:MSHC2.CMD*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*T3, Pin 146, EMMC2_DATA0, Mux:MSHC2.DATA_0*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*V3, Pin 147, EMMC2_DATA1, Mux:MSHC2.DATA_1*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*R4, Pin 148, EMMC2_DATA2, Mux:MSHC2.DATA_2*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*U3, Pin 149, EMMC2_DATA3, Mux:MSHC2.DATA_3*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*R3, Pin 150, EMMC2_DATA4, Mux:MSHC3.DATA_3*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*W3, Pin 151, EMMC2_DATA5, Mux:MSHC3.DATA_2*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*T4, Pin 152, EMMC2_DATA6, Mux:MSHC3.DATA_1*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*W4, Pin 153, EMMC2_DATA7, Mux:MSHC3.DATA_0*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__DOWN ),/*Y3, Pin 154, EMMC2_STROBE, Mux:MSHC3.CLK*/
            ((uint32_t)PORT_PAD_MMC_SP__MAX | PORT_PAD_MMC_SN__MAX | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MAX | PORT_PAD_MMC_TXPREN__MAX | PORT_PAD_MMC_PULL__UP ),/*AA3, Pin 155, EMMC2_RESET_N, Mux:MSHC3.CMD*/

        },

        {
            /* PIN_MUX_CONFIG: Port pins mode, open drain configuration */

            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_OPENDRAIN | PORT_PIN_MODE_GPIO),/*Pin 128*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_OPENDRAIN | PORT_PIN_MODE_GPIO),/*Pin 129*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_OPENDRAIN | PORT_PIN_MODE_GPIO),/*Pin 130*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_OPENDRAIN | PORT_PIN_MODE_GPIO),/*Pin 131*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__1 | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 132*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 133*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 134*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 135*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 136*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 137*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 138*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 139*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 140*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 141*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 142*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 143*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 144*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 145*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 146*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 147*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 148*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 149*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 150*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 151*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 152*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 153*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__1 | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 154*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 155*/
        },

        {
            /* GPIO Controller id for each pin */
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 128, I2S_MC_SD4*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 129, I2S_MC_SD5*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 130, I2S_MC_SD6*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 131, I2S_MC_SD7*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 132, EMMC1_CLK*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 133, EMMC1_CMD*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 134, EMMC1_DATA0*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 135, EMMC1_DATA1*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 136, EMMC1_DATA2*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 137, EMMC1_DATA3*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 138, EMMC1_DATA4*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 139, EMMC1_DATA5*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 140, EMMC1_DATA6*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 141, EMMC1_DATA7*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 142, EMMC1_STROBE*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 143, EMMC1_RESET_N*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 144, EMMC2_CLK*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 145, EMMC2_CMD*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 146, EMMC2_DATA0*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 147, EMMC2_DATA1*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 148, EMMC2_DATA2*/
            ((uint32_t)PORT_GPIO_IS_OUT | PORT_GPIO_4 ),/*Pin 149, EMMC2_DATA3*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 150, EMMC2_DATA4*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 151, EMMC2_DATA5*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 152, EMMC2_DATA6*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 153, EMMC2_DATA7*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 154, EMMC2_STROBE*/
            ((uint32_t)PORT_GPIO_IS_IN | PORT_GPIO_4 ),/*Pin 155, EMMC2_RESET_N*/

        },

        {
            /* Port pins initial level configuration */

            PORT_PIN_LEVEL_LOW,/* Pin 128 */
            PORT_PIN_LEVEL_LOW,/* Pin 129 */
            PORT_PIN_LEVEL_LOW,/* Pin 130 */
            PORT_PIN_LEVEL_LOW,/* Pin 131 */
            PORT_PIN_LEVEL_LOW,/* Pin 132 */
            PORT_PIN_LEVEL_LOW,/* Pin 133 */
            PORT_PIN_LEVEL_LOW,/* Pin 134 */
            PORT_PIN_LEVEL_LOW,/* Pin 135 */
            PORT_PIN_LEVEL_LOW,/* Pin 136 */
            PORT_PIN_LEVEL_LOW,/* Pin 137 */
            PORT_PIN_LEVEL_LOW,/* Pin 138 */
            PORT_PIN_LEVEL_LOW,/* Pin 139 */
            PORT_PIN_LEVEL_LOW,/* Pin 140 */
            PORT_PIN_LEVEL_LOW,/* Pin 141 */
            PORT_PIN_LEVEL_LOW,/* Pin 142 */
            PORT_PIN_LEVEL_LOW,/* Pin 143 */
            PORT_PIN_LEVEL_LOW,/* Pin 144 */
            PORT_PIN_LEVEL_LOW,/* Pin 145 */
            PORT_PIN_LEVEL_LOW,/* Pin 146 */
            PORT_PIN_LEVEL_LOW,/* Pin 147 */
            PORT_PIN_LEVEL_LOW,/* Pin 148 */
            PORT_PIN_LEVEL_LOW,/* Pin 149 */
            PORT_PIN_LEVEL_LOW,/* Pin 150 */
            PORT_PIN_LEVEL_LOW,/* Pin 151 */
            PORT_PIN_LEVEL_LOW,/* Pin 152 */
            PORT_PIN_LEVEL_LOW,/* Pin 153 */
            PORT_PIN_LEVEL_LOW,/* Pin 154 */
            PORT_PIN_LEVEL_LOW,/* Pin 155 */

        },

        {/* Port pin run time mode changeable or not configuration */

            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 128 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 129 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 130 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 131 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 132 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 133 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 134 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 135 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 136 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 137 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 138 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 139 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 140 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 141 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 142 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 143 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 144 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 145 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 146 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 147 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 148 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 149 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 150 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 151 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 152 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 153 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 154 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 155 */

        },

    },  // end of PORT4

};


#endif  /* PORT_CFG_H */
