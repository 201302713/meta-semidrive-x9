
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef __SSYSTEM_CONFIGS_H__
#define __SSYSTEM_CONFIGS_H__

#include "image_cfg.h"

#define CPU_ID_CURRENT CPU_ID_SEC
#define CPU_ID_BACKDOOR  CPU_ID_AP1

#define CONFIGS \
            PT_LOAD_CONFIG_ITEM(PT_USER, 0, AP1_PRELOADER_MEMBASE, AP1_PRELOADER_MEMSIZE, CPU_ID_AP1, PT_KICK_F|PT_AP2SEC_F, preloader)

#endif
