
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef __DISP_RES__
#define __DISP_RES__

struct display_resource g_display_res[] = {
	{
		.name = "Cluster",
		.type = CLUSTER,
		.dp_res = DP_RES_DP1,
		.dc_res = DC_RES_DC1,
		.if_res = IF_RES_LVDS1_LVDS2,
		.lvds_mode = LVDS_MODE_DUAL,
		.pixel_bpp = PIXEL_BPP_24,
		.gui_enable = 1,
	},

};

#endif /*__DISP_RES__*/
