
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef __MEM_MAP_H__
#define __MEM_MAP_H__

// mem map define from Resource Allocation sheet
// DDR
#define DDR_DDR_MEM_ECO_TEE_BASE 0x5B200000
#define DDR_DDR_MEM_ECO_TEE_SIZE 0x1000000

// GIC
#define GIC_GIC4_BASE 0x35430000
#define GIC_GIC4_SIZE 0x10000

// CE_MEM
#define CE_MEM_CE2_VCE3_BASE 0x524000
#define CE_MEM_CE2_VCE3_SIZE 0x2000

// CE_REG
#define CE_REG_CE2_VCE3_BASE 0x34002000
#define CE_REG_CE2_VCE3_SIZE 0x1000

// TIMER
#define TIMER_TIMER6_BASE 0x308E0000
#define TIMER_TIMER6_SIZE 0x10000

// UART
#define UART_UART9_BASE 0x304C0000
#define UART_UART9_SIZE 0x10000

// SYS_CNT
#define SYS_CNT_SYS_CNT_RO_BASE 0x31400000
#define SYS_CNT_SYS_CNT_RO_SIZE 0x10000


// mam map define from Rpc sheet
// CKGEN
#define GATING_EN_SEC_TIMER6_BASE 0x3813D000
#define GATING_EN_SEC_TIMER6_SIZE 0x4


#endif /* __MEM_MAP_H__*/
