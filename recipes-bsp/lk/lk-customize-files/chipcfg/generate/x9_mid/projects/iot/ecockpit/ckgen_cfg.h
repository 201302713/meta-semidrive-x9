
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef __CKGEN_CFG_H__
#define __CKGEN_CFG_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define CFG_CKGEN_DISP_BASE (0x36200000)
#define CFG_CKGEN_SOC_BASE (0x36000000)
#define CFG_CKGEN_SAF_BASE (0x3C000000)
#define CFG_CKGEN_SEC_BASE (0x38000000)

/*clkgen disp ip slice index*/
typedef enum _clkgen_disp_ip_slice_idx
{
	disp_ip_slice_mipi_csi1_pix = 0U,
	disp_ip_slice_mipi_csi2_pix = 1U,
	disp_ip_slice_csi3 = 2U,
	disp_ip_slice_dp1 = 3U,
	disp_ip_slice_dp2 = 4U,
	disp_ip_slice_dp3 = 5U,
	disp_ip_slice_dc5 = 6U,
	disp_ip_slice_dc1 = 7U,
	disp_ip_slice_dc2 = 8U,
	disp_ip_slice_dc3 = 9U,
	disp_ip_slice_dc4 = 10U,
	disp_ip_slice_spare1 = 11U,
	disp_ip_slice_dc5_alt_dsp_clk = 12U,
	disp_ip_slice_ext_aud1 = 13U,
	disp_ip_slice_ext_aud2 = 14U,
	disp_ip_slice_ext_aud3 = 15U,
	disp_ip_slice_ext_aud4 = 16U,
	disp_ip_slice_max,
}clkgen_disp_ip_slice_idx;

/*clkgen disp bus slice index*/
typedef enum _clkgen_disp_bus_slice_idx
{
	disp_bus_slice_disp_bus = 0U,
	disp_bus_slice_max,
}clkgen_disp_bus_slice_idx;

/*clkgen sec bus slice index*/
typedef enum _clkgen_sec_bus_slice_idx
{
	sec_bus_slice_sec_plat = 0U,
	sec_bus_slice_spare_0 = 1U,
	sec_bus_slice_spare_1 = 2U,
	sec_bus_slice_max,
}clkgen_sec_bus_slice_idx;

/*clkgen sec core slice index*/
typedef enum _clkgen_sec_core_slice_idx
{
	sec_core_slice_mp_plat = 0U,
	sec_core_slice_max,
}clkgen_sec_core_slice_idx;

/*clkgen sec ip slice index*/
typedef enum _clkgen_sec_ip_slice_idx
{
	sec_ip_slice_ce2 = 0U,
	sec_ip_slice_adc = 1U,
	sec_ip_slice_spare_2 = 2U,
	sec_ip_slice_i2c_sec0 = 3U,
	sec_ip_slice_i2c_sec1 = 4U,
	sec_ip_slice_spi_sec0 = 5U,
	sec_ip_slice_spi_sec1 = 6U,
	sec_ip_slice_uart_sec0 = 7U,
	sec_ip_slice_uart_sec1 = 8U,
	sec_ip_slice_emmc1 = 9U,
	sec_ip_slice_emmc2 = 10U,
	sec_ip_slice_emmc3 = 11U,
	sec_ip_slice_emmc4 = 12U,
	sec_ip_slice_enet2_tx = 13U,
	sec_ip_slice_enet2_rmii = 14U,
	sec_ip_slice_enet2_phy_ref = 15U,
	sec_ip_slice_enet2_timer_sec = 16U,
	sec_ip_slice_spdif1 = 17U,
	sec_ip_slice_spdif2 = 18U,
	sec_ip_slice_spdif3 = 19U,
	sec_ip_slice_spdif4 = 20U,
	sec_ip_slice_ospi2 = 21U,
	sec_ip_slice_timer3 = 22U,
	sec_ip_slice_timer4 = 23U,
	sec_ip_slice_timer5 = 24U,
	sec_ip_slice_timer6 = 25U,
	sec_ip_slice_timer7 = 26U,
	sec_ip_slice_timer8 = 27U,
	sec_ip_slice_pwm3 = 28U,
	sec_ip_slice_pwm4 = 29U,
	sec_ip_slice_pwm5 = 30U,
	sec_ip_slice_pwm6 = 31U,
	sec_ip_slice_pwm7 = 32U,
	sec_ip_slice_pwm8 = 33U,
	sec_ip_slice_i2s_mclk2 = 34U,
	sec_ip_slice_i2s_mclk3 = 35U,
	sec_ip_slice_i2s_mc1 = 36U,
	sec_ip_slice_i2s_mc2 = 37U,
	sec_ip_slice_i2s_sc3 = 38U,
	sec_ip_slice_i2s_sc4 = 39U,
	sec_ip_slice_i2s_sc5 = 40U,
	sec_ip_slice_i2s_sc6 = 41U,
	sec_ip_slice_i2s_sc7 = 42U,
	sec_ip_slice_i2s_sc8 = 43U,
	sec_ip_slice_csi_mclk1 = 44U,
	sec_ip_slice_csi_mclk2 = 45U,
	sec_ip_slice_gic_4_5 = 46U,
	sec_ip_slice_can_5_to_20 = 47U,
	sec_ip_slice_trace = 48U,
	sec_ip_slice_sys_cnt = 49U,
	sec_ip_slice_mshc_timer = 50U,
	sec_ip_slice_hpi_clk600 = 51U,
	sec_ip_slice_hpi_clk800 = 52U,
	sec_ip_slice_max,
}clkgen_sec_ip_slice_idx;

/*clkgen soc core slice index*/
typedef enum _clkgen_soc_core_slice_idx
{
	soc_core_slice_cpu1a = 0U,
	soc_core_slice_cpu1b = 1U,
	soc_core_slice_cpu2 = 2U,
	soc_core_slice_gpu1 = 3U,
	soc_core_slice_gpu2 = 4U,
	soc_core_slice_ddr = 5U,
	soc_core_slice_max,
}clkgen_soc_core_slice_idx;

/*clkgen soc ip slice index*/
typedef enum _clkgen_soc_ip_slice_idx
{
	soc_ip_slice_vpu1 = 0U,
	soc_ip_slice_mjpeg = 1U,
	soc_ip_slice_can_9_to_20 = 2U,
	soc_ip_slice_max,
}clkgen_soc_ip_slice_idx;

/*clkgen soc bus slice index*/
typedef enum _clkgen_soc_bus_slice_idx
{
	soc_bus_slice_vpu_bus = 0U,
	soc_bus_slice_vsn_bus = 1U,
	soc_bus_slice_noc = 2U,
	soc_bus_slice_his_bus = 3U,
	soc_bus_slice_max,
}clkgen_soc_bus_slice_idx;

/*clkgen uuu wrapper index*/
typedef enum _clkgen_uuu_wrapper_idx
{
	uuu_clock_wrapper_cpu1a = 0U,
	uuu_clock_wrapper_cpu1b = 1U,
	uuu_clock_wrapper_cpu2 = 2U,
	uuu_clock_wrapper_gpu1 = 3U,
	uuu_clock_wrapper_gpu2 = 4U,
	uuu_clock_wrapper_vpu1 = 5U,
	uuu_clock_wrapper_mjpeg = 6U,
	uuu_clock_wrapper_vpu_bus = 7U,
	uuu_clock_wrapper_vsn_bus = 8U,
	uuu_clock_wrapper_ddr = 9U,
	uuu_clock_wrapper_his_bus = 10U,
	uuu_clock_wrapper_idx_max,
} clkgen_uuu_wrapper_idx;



#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif
