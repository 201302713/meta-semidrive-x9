
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef __MEM_MAP_H__
#define __MEM_MAP_H__

// mem map define from Resource Allocation sheet
// DDR
#define DDR_DDR_MEM_ECO_HYP_BASE 0x5C200000
#define DDR_DDR_MEM_ECO_HYP_SIZE 0x2000000

// GIC
#define GIC_GIC4_BASE 0x35430000
#define GIC_GIC4_SIZE 0x10000

// UART
#define UART_UART13_BASE 0x30500000
#define UART_UART13_SIZE 0x10000
#define UART_UART9_BASE 0x304C0000
#define UART_UART9_SIZE 0x10000

// SYS_CNT
#define SYS_CNT_SYS_CNT_RO_BASE 0x31400000
#define SYS_CNT_SYS_CNT_RO_SIZE 0x10000

// SCR4K_SID
#define SCR4K_SID_SCR4K_SID_BASE 0x35600000
#define SCR4K_SID_SCR4K_SID_SIZE 0x80000

// SMMU_TCU
#define SMMU_TCU_SMMU_TCU_BASE 0x35800000
#define SMMU_TCU_SMMU_TCU_SIZE 0x800000


// mam map define from Rpc sheet

#endif /* __MEM_MAP_H__*/
