
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef _DOMAIN_RES_CNT_H
#define _DOMAIN_RES_CNT_H

/* Generated domain resource count declarations.
 * DO NOT MODIFY!
 */

#define RES_VERSION 0
#define MB_RES_NUM 1
#define DDR_RES_NUM 6
#define GIC_RES_NUM 1
#define OSPI_RES_NUM 2
#define ROMC_RES_NUM 1
#define PLATFORM_RES_NUM 4
#define CE_MEM_RES_NUM 4
#define VDSP_RES_NUM 2
#define DMA_RES_NUM 1
#define MSHC_RES_NUM 3
#define MAC_RES_NUM 1
#define GPV_RES_NUM 5
#define SPI_RES_NUM 1
#define CE_REG_RES_NUM 5
#define PLL_RES_NUM 1
#define XTAL_RES_NUM 1
#define ROMC_REG_RES_NUM 1
#define DMA_MUX_RES_NUM 1
#define WATCHDOG_RES_NUM 2
#define TIMER_RES_NUM 1
#define UART_RES_NUM 2
#define I2C_RES_NUM 2
#define OSPI_REG_RES_NUM 2
#define EFUSE_RES_NUM 1
#define GPIO_RES_NUM 1
#define GPV_SEC_M_REG_RES_NUM 1
#define DISP_MUX_RES_NUM 1
#define LVDS_RES_NUM 1
#define USBPHY_RES_NUM 1
#define USB_RES_NUM 1
#define SCR_HPI_RES_NUM 1
#define SYS_CNT_RES_NUM 2
#define SEC_STORAGE_RES_NUM 1
#define FTBU_RES_NUM 1
#define CSSYS_RES_NUM 1
#define DDR_CFG_RES_NUM 1
#define IOMUXC_RES_NUM 224
#define RSTGEN_RES_NUM 26
#define SCR_RES_NUM 83
#define CKGEN_RES_NUM 125


#endif /* _DOMAIN_RES_CNT_H */
