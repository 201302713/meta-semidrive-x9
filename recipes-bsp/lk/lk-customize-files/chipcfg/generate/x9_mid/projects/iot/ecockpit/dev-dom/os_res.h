
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef _OS_RES_H
#define _OS_RES_H
#include "os_res_cnt.h"

/* Generated os resource declarations.
 * DO NOT MODIFY!
 */

// os res define from Resource Allocation sheet
const domain_res_t g_mb_res =
{
	.res_id[0] = RES_MB_MB_MEM,
	.res_num = MB_RES_NUM,
 };

const domain_res_t g_gpu_res =
{
	.res_id[0] = RES_GPU_GPU1_VGPU1,
	.res_num = GPU_RES_NUM,
 };

const domain_res_t g_ddr_res =
{
	.res_id[0] = RES_DDR_DDR_MEM_ECO_DEV,
	.res_num = DDR_RES_NUM,
 };

const domain_res_t g_gic_res =
{
	.res_id[0] = RES_GIC_GIC4,
	.res_num = GIC_RES_NUM,
 };

const domain_res_t g_ce_mem_res =
{
	.res_id[0] = RES_CE_MEM_CE2_VCE5,
	.res_num = CE_MEM_RES_NUM,
 };

const domain_res_t g_mshc_res =
{
	.res_id[0] = RES_MSHC_SD1,
	.res_num = MSHC_RES_NUM,
 };

const domain_res_t g_ce_reg_res =
{
	.res_id[0] = RES_CE_REG_CE2_VCE5,
	.res_num = CE_REG_RES_NUM,
 };

const domain_res_t g_pvt_sens_res =
{
	.res_id[0] = RES_PVT_SENS_PVT_SENS_SEC,
	.res_num = PVT_SENS_RES_NUM,
 };

const domain_res_t g_pll_res =
{
	.res_id[0] = RES_PLL_PLL_CPU1B,
	.res_id[1] = RES_PLL_PLL_CPU1A,
	.res_id[2] = RES_PLL_PLL_GPU2,
	.res_id[3] = RES_PLL_PLL_GPU1,
	.res_num = PLL_RES_NUM,
 };

const domain_res_t g_watchdog_res =
{
	.res_id[0] = RES_WATCHDOG_WDT5,
	.res_num = WATCHDOG_RES_NUM,
 };

const domain_res_t g_uart_res =
{
	.res_id[0] = RES_UART_UART9,
	.res_num = UART_RES_NUM,
 };

const domain_res_t g_sys_cnt_res =
{
	.res_id[0] = RES_SYS_CNT_SYS_CNT_RO,
	.res_num = SYS_CNT_RES_NUM,
 };

const domain_res_t g_scr4k_ssid_res =
{
	.res_id[0] = RES_SCR4K_SSID_SCR4K_SSID,
	.res_num = SCR4K_SSID_RES_NUM,
 };


// os res define from Rpc sheet
static const domain_res_t * g_res_cat[] = {
	NULL, /*iram*/
	&g_mb_res,
	&g_gpu_res,
	&g_ddr_res,
	NULL, /*pcie*/
	&g_gic_res,
	NULL, /*ospi*/
	NULL, /*romc*/
	NULL, /*platform*/
	&g_ce_mem_res,
	NULL, /*vdsp*/
	NULL, /*dma*/
	&g_mshc_res,
	NULL, /*mac*/
	NULL, /*gpv*/
	NULL, /*spi*/
	&g_ce_reg_res,
	NULL, /*sem*/
	NULL, /*i2s_sc*/
	NULL, /*bipc*/
	NULL, /*eic*/
	&g_pvt_sens_res,
	NULL, /*pwrctrl*/
	&g_pll_res,
	NULL, /*rc24m*/
	NULL, /*xtal*/
	NULL, /*romc_reg*/
	NULL, /*iram_reg*/
	NULL, /*dma_mux*/
	&g_watchdog_res,
	NULL, /*pwm*/
	NULL, /*timer*/
	NULL, /*enet_qos*/
	&g_uart_res,
	NULL, /*i2c*/
	NULL, /*can*/
	NULL, /*ospi_reg*/
	NULL, /*efuse*/
	NULL, /*gpio*/
	NULL, /*gpv_sec_m_reg*/
	NULL, /*i2s_mc*/
	NULL, /*spdif*/
	NULL, /*adc*/
	NULL, /*dc*/
	NULL, /*dp*/
	NULL, /*mipi_dsi*/
	NULL, /*disp_mux*/
	NULL, /*mipi_csi*/
	NULL, /*csi*/
	NULL, /*g2d*/
	NULL, /*lvds*/
	NULL, /*dma_ch_mux_pcie*/
	NULL, /*usbphy*/
	NULL, /*usb*/
	NULL, /*pcie_phy*/
	NULL, /*scr_hpi*/
	NULL, /*vpu*/
	NULL, /*mjpeg*/
	&g_sys_cnt_res,
	NULL, /*sec_storage*/
	NULL, /*rstgen_rtc*/
	NULL, /*tm*/
	NULL, /*iomuxc_rtc*/
	NULL, /*pmu*/
	NULL, /*rc_rtc*/
	NULL, /*rtc*/
	NULL, /*ftbu*/
	NULL, /*cssys*/
	&g_scr4k_ssid_res,
	NULL, /*scr4k_sid*/
	NULL, /*smmu_tcu*/
	NULL, /*ddr_cfg*/
	NULL, /*pcie_reg*/
	NULL, /*pcie_io*/
	NULL, /*iomuxc*/
	NULL, /*rstgen*/
	NULL, /*scr*/
	NULL, /*ckgen*/
};

#endif /* _OS_RES_H */
