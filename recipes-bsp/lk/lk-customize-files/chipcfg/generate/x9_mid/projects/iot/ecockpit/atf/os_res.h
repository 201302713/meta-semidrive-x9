
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef _OS_RES_H
#define _OS_RES_H
#include "os_res_cnt.h"

/* Generated os resource declarations.
 * DO NOT MODIFY!
 */

// os res define from Resource Allocation sheet
const domain_res_t g_ddr_res =
{
	.res_id[0] = RES_DDR_DDR_MEM_ECO_ATF,
	.res_num = DDR_RES_NUM,
 };

const domain_res_t g_gic_res =
{
	.res_id[0] = RES_GIC_GIC4,
	.res_num = GIC_RES_NUM,
 };

const domain_res_t g_uart_res =
{
	.res_id[0] = RES_UART_UART9,
	.res_num = UART_RES_NUM,
 };

const domain_res_t g_sys_cnt_res =
{
	.res_id[0] = RES_SYS_CNT_SYS_CNT_RO,
	.res_num = SYS_CNT_RES_NUM,
 };


// os res define from Rpc sheet
static const domain_res_t * g_res_cat[] = {
	NULL, /*iram*/
	NULL, /*mb*/
	NULL, /*gpu*/
	&g_ddr_res,
	NULL, /*pcie*/
	&g_gic_res,
	NULL, /*ospi*/
	NULL, /*romc*/
	NULL, /*platform*/
	NULL, /*ce_mem*/
	NULL, /*vdsp*/
	NULL, /*dma*/
	NULL, /*mshc*/
	NULL, /*mac*/
	NULL, /*gpv*/
	NULL, /*spi*/
	NULL, /*ce_reg*/
	NULL, /*sem*/
	NULL, /*i2s_sc*/
	NULL, /*bipc*/
	NULL, /*eic*/
	NULL, /*pvt_sens*/
	NULL, /*pwrctrl*/
	NULL, /*pll*/
	NULL, /*rc24m*/
	NULL, /*xtal*/
	NULL, /*romc_reg*/
	NULL, /*iram_reg*/
	NULL, /*dma_mux*/
	NULL, /*watchdog*/
	NULL, /*pwm*/
	NULL, /*timer*/
	NULL, /*enet_qos*/
	&g_uart_res,
	NULL, /*i2c*/
	NULL, /*can*/
	NULL, /*ospi_reg*/
	NULL, /*efuse*/
	NULL, /*gpio*/
	NULL, /*gpv_sec_m_reg*/
	NULL, /*i2s_mc*/
	NULL, /*spdif*/
	NULL, /*adc*/
	NULL, /*dc*/
	NULL, /*dp*/
	NULL, /*mipi_dsi*/
	NULL, /*disp_mux*/
	NULL, /*mipi_csi*/
	NULL, /*csi*/
	NULL, /*g2d*/
	NULL, /*lvds*/
	NULL, /*dma_ch_mux_pcie*/
	NULL, /*usbphy*/
	NULL, /*usb*/
	NULL, /*pcie_phy*/
	NULL, /*scr_hpi*/
	NULL, /*vpu*/
	NULL, /*mjpeg*/
	&g_sys_cnt_res,
	NULL, /*sec_storage*/
	NULL, /*rstgen_rtc*/
	NULL, /*tm*/
	NULL, /*iomuxc_rtc*/
	NULL, /*pmu*/
	NULL, /*rc_rtc*/
	NULL, /*rtc*/
	NULL, /*ftbu*/
	NULL, /*cssys*/
	NULL, /*scr4k_ssid*/
	NULL, /*scr4k_sid*/
	NULL, /*smmu_tcu*/
	NULL, /*ddr_cfg*/
	NULL, /*pcie_reg*/
	NULL, /*pcie_io*/
	NULL, /*iomuxc*/
	NULL, /*rstgen*/
	NULL, /*scr*/
	NULL, /*ckgen*/
};

#endif /* _OS_RES_H */
