/*
 * bootloader_configs.h
 *
 * Copyright (c) 2019 Semidrive Semiconductor.
 * All rights reserved.
 *
 * Description:
 *
 * Revision History:
 *-----------------
 */
#ifndef _BOOTLOADER_CONFIGS_H
#define _BOOTLOADER_CONFIGS_H

#include "image_cfg.h"

#define ALIAS_DTB_PARTITION  "dtb"
#define ALIAS_KERNEL_PARTITION "kernel"
#define ALIAS_ROOTFS_PARTITION "rootfs"

#define PART_SEPARAT ""

#define HYP_MEMBASE  ECO_HYP_MEMBASE
#define HYP_MEMSIZE  ECO_HYP_MEMSIZE
#define DOM0_MEMBASE  ECO_DEV_MEMBASE
#define DOM0_MEMSIZE  ECO_DEV_MEMSIZE
#define ROOTFS_MEMSIZE  AP1_BOARD_RAMDISK_MEMSIZE
#define REE_MEMBASE  ECO_REE_MEMBASE
#define REE_MEMSIZE  ECO_REE_MEMSIZE

#endif
