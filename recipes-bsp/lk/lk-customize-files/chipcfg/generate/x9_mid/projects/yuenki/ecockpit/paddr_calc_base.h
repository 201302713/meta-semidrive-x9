
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef _PADDR_DEFINE_H
#define _PADDR_DEFINE_H

#define APBMUX1_IP_BASE     (0x30000000u)
#define APBMUX2_IP_BASE     (0x30400000u)
#define APBMUX3_IP_BASE     (0x30800000u)
#define APBMUX4_IP_BASE     (0x30c00000u)
#define APBMUX5_IP_BASE     (0x31000000u)
#define APBMUX6_IP_BASE     (0x31400000u)
#define APBMUX7_IP_BASE     (0x31800000u)
#define APBMUX8_IP_BASE     (0x31c00000u)
#define APB_DDR_CFG_BASE    (0x32000000u)
#define APB_SMMU_BASE       (0x35800000u)
#define APB_CE2_REG_BASE    (0x34000000u)
#define APB_SCR4K_SID_BASE  (0x35600000u)
#define APB_SCR4K_SSID_BASE (0x35700000u)
#define APB_CSSYS_BASE      (0x01000000u)
#define APB_RPC_SOC_BASE    (0x36000000u)
#define APB_RPC_SEC_BASE    (0x38000000u)
#define APB_RPC_SAF_BASE    (0x3C000000u)

#define _SIZE_32M_  0x2000000
#define _SIZE_20M_  0x1400000
#define _SIZE_16M_  0x1000000
#define _SIZE_8M_   0x800000
#define _SIZE_2M_   0x200000
#define _SIZE_1M_   0x100000
#define _SIZE_512K_ 0x80000
#define _SIZE_128K_ 0x20000
#define _SIZE_64K_  0x10000
#define _SIZE_4K_   0x1000
#define _SIZE_2K_   0x800
#define _SIZE_1K_   0x400
#define _SIZE_4B_   0x4
#define _RPC_MAC_OFFSET 0xFFE000

#define APBMUX1_IP_SIZE     (_SIZE_64K_)
#define APBMUX2_IP_SIZE     (_SIZE_64K_)
#define APBMUX3_IP_SIZE     (_SIZE_64K_)
#define APBMUX4_IP_SIZE     (_SIZE_64K_)
#define APBMUX5_IP_SIZE     (_SIZE_128K_)
#define APBMUX6_IP_SIZE     (_SIZE_64K_)
#define APBMUX7_IP_SIZE     (_SIZE_64K_)
#define APBMUX8_IP_SIZE     (_SIZE_4K_)
#define APB_DDR_CFG_SIZE    (_SIZE_20M_)
#define APB_SMMU_SIZE       (_SIZE_8M_)
#define APB_CE2_REG_SIZE    (_SIZE_4K_)
#define APB_SCR4K_SID_SIZE  (_SIZE_512K_)
#define APB_SCR4K_SSID_SIZE (_SIZE_512K_)
#define APB_CSSYS_SIZE      (_SIZE_16M_)
#define APB_GIC_SIZE        (_SIZE_64K_)

/* for address transformation from r core to a core */
#define addrmap_def \
    {0},

/* for memory type resources and undefined resources */
#define MPC_CATEGORY_MAX 14

typedef struct mem_info {
    uint32_t res_id;
    addr_t paddr;
} mem_info_t;

typedef struct domain_mem_info {
    int res_num;
    mem_info_t mem_info[];
} domain_mem_info_t;
const domain_mem_info_t mem_info_iram =
{
	.mem_info[0].res_id = RES_IRAM_IRAM4,
	.mem_info[0].paddr = 0x001C0000,
	.mem_info[1].res_id = RES_IRAM_IRAM4_ECC,
	.mem_info[1].paddr = 0x00200000,
	.mem_info[2].res_id = RES_IRAM_IRAM3,
	.mem_info[2].paddr = 0x00180000,
	.mem_info[3].res_id = RES_IRAM_IRAM3_ECC,
	.mem_info[3].paddr = 0x00208000,
	.mem_info[4].res_id = RES_IRAM_IRAM2,
	.mem_info[4].paddr = 0x00140000,
	.mem_info[5].res_id = RES_IRAM_IRAM2_ECC,
	.mem_info[5].paddr = 0x00210000,
	.res_num = 6,
 };

const domain_mem_info_t mem_info_mb =
{
	.mem_info[0].res_id = RES_MB_MB_MEM,
	.mem_info[0].paddr = 0x34040000,
	.res_num = 1,
 };

const domain_mem_info_t mem_info_gpu =
{
	.mem_info[0].res_id = RES_GPU_GPU1_VGPU1,
	.mem_info[0].paddr = 0x34C00000,
	.mem_info[1].res_id = RES_GPU_GPU1_VGPU2,
	.mem_info[1].paddr = 0x34C10000,
	.res_num = 2,
 };

const domain_mem_info_t mem_info_ddr =
{
	.mem_info[0].res_id = RES_DDR_DDR_MEM_SYS_CFG,
	.mem_info[0].paddr = 0x40600000,
	.mem_info[1].res_id = RES_DDR_DDR_MEM_VBMETA,
	.mem_info[1].paddr = 0x40800000,
	.mem_info[2].res_id = RES_DDR_DDR_MEM_DIL_IMAGES,
	.mem_info[2].paddr = 0x40810000,
	.mem_info[3].res_id = RES_DDR_DDR_MEM_VDSP,
	.mem_info[3].paddr = 0x40A00000,
	.mem_info[4].res_id = RES_DDR_DDR_MEM_VDSP_SHARE,
	.mem_info[4].paddr = 0x43A00000,
	.mem_info[5].res_id = RES_DDR_DDR_MEM_SERVICES,
	.mem_info[5].paddr = 0x45A00000,
	.mem_info[6].res_id = RES_DDR_DDR_MEM_SAF_ECO,
	.mem_info[6].paddr = 0x4AD00000,
	.mem_info[7].res_id = RES_DDR_DDR_MEM_SEC_ECO,
	.mem_info[7].paddr = 0x4AF00000,
	.mem_info[8].res_id = RES_DDR_DDR_MEM_ECO_ATF,
	.mem_info[8].paddr = 0x5B000000,
	.mem_info[9].res_id = RES_DDR_DDR_MEM_ECO_TEE,
	.mem_info[9].paddr = 0x5B200000,
	.mem_info[10].res_id = RES_DDR_DDR_MEM_ECO_HYP,
	.mem_info[10].paddr = 0x5C200000,
	.mem_info[11].res_id = RES_DDR_DDR_MEM_ECO_DEV,
	.mem_info[11].paddr = 0x5E200000,
	.mem_info[12].res_id = RES_DDR_DDR_MEM_ECO_REE,
	.mem_info[12].paddr = 0x5C200000,
	.mem_info[13].res_id = RES_DDR_DDR_MEM_CLU,
	.mem_info[13].paddr = 0x120000000,
	.res_num = 14,
 };

const domain_mem_info_t mem_info_pcie =
{
	.mem_info[0].res_id = RES_PCIE_PCIE1,
	.mem_info[0].paddr = 0x10000000,
	.res_num = 1,
 };

const domain_mem_info_t mem_info_gic =
{
	.mem_info[0].res_id = RES_GIC_GIC4,
	.mem_info[0].paddr = 0x35430000,
	.res_num = 1,
 };

const domain_mem_info_t mem_info_ospi =
{
	.mem_info[0].res_id = RES_OSPI_OSPI2,
	.mem_info[0].paddr = 0x08000000,
	.res_num = 1,
 };

const domain_mem_info_t mem_info_ce_mem =
{
	.mem_info[0].res_id = RES_CE_MEM_CE2_VCE2,
	.mem_info[0].paddr = 0x00522000,
	.mem_info[1].res_id = RES_CE_MEM_CE2_VCE3,
	.mem_info[1].paddr = 0x00524000,
	.mem_info[2].res_id = RES_CE_MEM_CE2_VCE4,
	.mem_info[2].paddr = 0x00526000,
	.mem_info[3].res_id = RES_CE_MEM_CE2_VCE5,
	.mem_info[3].paddr = 0x00528000,
	.res_num = 4,
 };

const domain_mem_info_t mem_info_vdsp =
{
	.mem_info[0].res_id = RES_VDSP_VDSP,
	.mem_info[0].paddr = 0x00A00000,
	.res_num = 1,
 };

const domain_mem_info_t mem_info_dma =
{
	.mem_info[0].res_id = RES_DMA_DMA8,
	.mem_info[0].paddr = 0x35570000,
	.mem_info[1].res_id = RES_DMA_DMA7,
	.mem_info[1].paddr = 0x35560000,
	.mem_info[2].res_id = RES_DMA_DMA6,
	.mem_info[2].paddr = 0x35550000,
	.mem_info[3].res_id = RES_DMA_DMA5,
	.mem_info[3].paddr = 0x35540000,
	.mem_info[4].res_id = RES_DMA_DMA4,
	.mem_info[4].paddr = 0x35530000,
	.mem_info[5].res_id = RES_DMA_DMA3,
	.mem_info[5].paddr = 0x35520000,
	.res_num = 6,
 };

const domain_mem_info_t mem_info_mshc =
{
	.mem_info[0].res_id = RES_MSHC_SD4,
	.mem_info[0].paddr = 0x341B0000,
	.mem_info[1].res_id = RES_MSHC_SD3,
	.mem_info[1].paddr = 0x341A0000,
	.mem_info[2].res_id = RES_MSHC_SD2,
	.mem_info[2].paddr = 0x34190000,
	.mem_info[3].res_id = RES_MSHC_SD1,
	.mem_info[3].paddr = 0x34180000,
	.res_num = 4,
 };

const domain_mem_info_t mem_info_unprotected =
{
	.mem_info[0].res_id = RES_PCIE_REG_PCIE2_REG,
	.mem_info[0].paddr = 0x34080000,
	.mem_info[1].res_id = RES_PCIE_REG_PCIE1_REG,
	.mem_info[1].paddr = 0x34060000,
	.mem_info[2].res_id = RES_PCIE_IO_PCIE_IO2,
	.mem_info[2].paddr = 0x600000000,
	.mem_info[3].res_id = RES_PCIE_IO_PCIE_IO1,
	.mem_info[3].paddr = 0x500000000,
	.res_num = 4,
 };

const domain_mem_info_t * mem_info_init[] = {
	&mem_info_iram,
	&mem_info_mb,
	&mem_info_gpu,
	&mem_info_ddr,
	&mem_info_pcie,
	&mem_info_gic,
	&mem_info_ospi,
	NULL, /*romc*/
	NULL, /*platform*/
	&mem_info_ce_mem,
	&mem_info_vdsp,
	&mem_info_dma,
	&mem_info_mshc,
	NULL, /*mac*/
};
#endif /* _PADDR_DEFINE_H*/
