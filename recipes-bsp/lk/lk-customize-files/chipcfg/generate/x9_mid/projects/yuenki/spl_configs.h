
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef __SPL_CONFIGS_H__
#define __SPL_CONFIGS_H__

#include "image_cfg.h"

#define CONFIGS \
            PT_LOAD_CONFIG_ITEM(PT_USER, 0, SYS_CFG_MEMBASE, SYS_CFG_MEMSIZE, CPU_ID_SEC, 0, system_config) \
            PT_LOAD_CONFIG_ITEM(PT_USER, 1, SEC_MEMBASE, SEC_MEMSIZE, CPU_ID_SEC, PT_KICK_F, ssystem)

#endif
