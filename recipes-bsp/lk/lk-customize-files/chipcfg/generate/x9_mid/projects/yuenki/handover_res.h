
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef __HANDOVER_RES_H__
#define __HANDOVER_RES_H__

typedef struct handover_info {
    uint32_t res_id;
    addr_t op_addr;
    uint8_t op_bit;
}handover_info_t;

typedef struct handover_list {
    uint32_t handover_num;
    handover_info_t handover_info[];
} handover_list_t;

const handover_list_t handover_list = {
	.handover_info[0].res_id = RES_SEC_STORAGE_SEC_STORAGE2,
	.handover_info[0].op_addr = 0xFC283000,
	.handover_info[0].op_bit = 9,
	.handover_info[1].res_id = RES_RTC_RTC2,
	.handover_info[1].op_addr = 0xFC283000,
	.handover_info[1].op_bit = 5,
	.handover_num = 2,
};

#endif /* __HANDOVER_RES_H__*/
