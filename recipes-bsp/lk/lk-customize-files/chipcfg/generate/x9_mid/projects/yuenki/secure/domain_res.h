
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef _DOMAIN_RES_H
#define _DOMAIN_RES_H
#include "domain_res_cnt.h"

/* Generated domain resource declarations.
 * DO NOT MODIFY!
 */

// domain res define from Resource Allocation sheet
const domain_res_t g_mb_res =
{
	.res_id[0] = RES_MB_MB_MEM,
	.res_num = MB_RES_NUM,
 };

const domain_res_t g_ddr_res =
{
	.res_id[0] = RES_DDR_DDR_MEM_SEC,
	.res_id[1] = RES_DDR_DDR_MEM_SYS_CFG,
	.res_id[2] = RES_DDR_DDR_MEM_VBMETA,
	.res_id[3] = RES_DDR_DDR_MEM_DIL_IMAGES,
	.res_id[4] = RES_DDR_DDR_MEM_SAF_SEC,
	.res_id[5] = RES_DDR_DDR_MEM_SEC_ECO,
	.res_num = DDR_RES_NUM,
 };

const domain_res_t g_gic_res =
{
	.res_id[0] = RES_GIC_GIC2,
	.res_num = GIC_RES_NUM,
 };

const domain_res_t g_ospi_res =
{
	.res_id[0] = RES_OSPI_OSPI2,
	.res_id[1] = RES_OSPI_OSPI1,
	.res_num = OSPI_RES_NUM,
 };

const domain_res_t g_romc_res =
{
	.res_id[0] = RES_ROMC_ROMC2,
	.res_num = ROMC_RES_NUM,
 };

const domain_res_t g_platform_res =
{
	.res_id[0] = RES_PLATFORM_R5_SEC_TCMB,
	.res_id[1] = RES_PLATFORM_R5_SEC_TCMA,
	.res_id[2] = RES_PLATFORM_R5_SEC_ICACHE,
	.res_id[3] = RES_PLATFORM_R5_SEC_DCACHE,
	.res_num = PLATFORM_RES_NUM,
 };

const domain_res_t g_ce_mem_res =
{
	.res_id[0] = RES_CE_MEM_CE2_VCE1,
	.res_id[1] = RES_CE_MEM_CE2_VCE6,
	.res_id[2] = RES_CE_MEM_CE2_VCE7,
	.res_id[3] = RES_CE_MEM_CE2_VCE8,
	.res_num = CE_MEM_RES_NUM,
 };

const domain_res_t g_vdsp_res =
{
	.res_id[0] = RES_VDSP_GPV_VSN_1,
	.res_id[1] = RES_VDSP_GPV_VSN_2,
	.res_num = VDSP_RES_NUM,
 };

const domain_res_t g_dma_res =
{
	.res_id[0] = RES_DMA_DMA2,
	.res_num = DMA_RES_NUM,
 };

const domain_res_t g_mshc_res =
{
	.res_id[0] = RES_MSHC_SD4,
	.res_id[1] = RES_MSHC_SD3,
	.res_id[2] = RES_MSHC_SD1,
	.res_num = MSHC_RES_NUM,
 };

const domain_res_t g_mac_res =
{
	.res_id[0] = RES_MAC_MAC_GLOBAL,
	.res_num = MAC_RES_NUM,
 };

const domain_res_t g_gpv_res =
{
	.res_id[0] = RES_GPV_GPV_SEC,
	.res_id[1] = RES_GPV_GPV_DISP,
	.res_id[2] = RES_GPV_GPV_HIS,
	.res_id[3] = RES_GPV_GPV_HPIA,
	.res_id[4] = RES_GPV_GPV_VPU,
	.res_num = GPV_RES_NUM,
 };

const domain_res_t g_spi_res =
{
	.res_id[0] = RES_SPI_SPI5,
	.res_num = SPI_RES_NUM,
 };

const domain_res_t g_ce_reg_res =
{
	.res_id[0] = RES_CE_REG_CE2_VCE1,
	.res_id[1] = RES_CE_REG_CE2_VCE6,
	.res_id[2] = RES_CE_REG_CE2_VCE7,
	.res_id[3] = RES_CE_REG_CE2_VCE8,
	.res_id[4] = RES_CE_REG_CE2_GLOBAL_CTRL,
	.res_num = CE_REG_RES_NUM,
 };

const domain_res_t g_pll_res =
{
	.res_id[0] = RES_PLL_PLL_DISP,
	.res_num = PLL_RES_NUM,
 };

const domain_res_t g_xtal_res =
{
	.res_id[0] = RES_XTAL_XTAL_AP,
	.res_num = XTAL_RES_NUM,
 };

const domain_res_t g_romc_reg_res =
{
	.res_id[0] = RES_ROMC_REG_ROMC2,
	.res_num = ROMC_REG_RES_NUM,
 };

const domain_res_t g_dma_mux_res =
{
	.res_id[0] = RES_DMA_MUX_DMA_MUX2,
	.res_num = DMA_MUX_RES_NUM,
 };

const domain_res_t g_watchdog_res =
{
	.res_id[0] = RES_WATCHDOG_WDT5,
	.res_id[1] = RES_WATCHDOG_WDT3,
	.res_num = WATCHDOG_RES_NUM,
 };

const domain_res_t g_timer_res =
{
	.res_id[0] = RES_TIMER_TIMER3,
	.res_num = TIMER_RES_NUM,
 };

const domain_res_t g_uart_res =
{
	.res_id[0] = RES_UART_UART10,
	.res_id[1] = RES_UART_UART9,
	.res_num = UART_RES_NUM,
 };

const domain_res_t g_i2c_res =
{
	.res_id[0] = RES_I2C_I2C12,
	.res_id[1] = RES_I2C_I2C10,
	.res_num = I2C_RES_NUM,
 };

const domain_res_t g_ospi_reg_res =
{
	.res_id[0] = RES_OSPI_REG_OSPI1,
	.res_id[1] = RES_OSPI_REG_OSPI2,
	.res_num = OSPI_REG_RES_NUM,
 };

const domain_res_t g_efuse_res =
{
	.res_id[0] = RES_EFUSE_EFUSEC,
	.res_num = EFUSE_RES_NUM,
 };

const domain_res_t g_gpio_res =
{
	.res_id[0] = RES_GPIO_GPIO2,
	.res_num = GPIO_RES_NUM,
 };

const domain_res_t g_gpv_sec_m_reg_res =
{
	.res_id[0] = RES_GPV_SEC_M_REG_GPV_SEC_M,
	.res_num = GPV_SEC_M_REG_RES_NUM,
 };

const domain_res_t g_disp_mux_res =
{
	.res_id[0] = RES_DISP_MUX_DISP_MUX,
	.res_num = DISP_MUX_RES_NUM,
 };

const domain_res_t g_lvds_res =
{
	.res_id[0] = RES_LVDS_LVDS_COMMON,
	.res_num = LVDS_RES_NUM,
 };

const domain_res_t g_usbphy_res =
{
	.res_id[0] = RES_USBPHY_USBPHY1,
	.res_num = USBPHY_RES_NUM,
 };

const domain_res_t g_usb_res =
{
	.res_id[0] = RES_USB_USB1,
	.res_num = USB_RES_NUM,
 };

const domain_res_t g_scr_hpi_res =
{
	.res_id[0] = RES_SCR_HPI_SCR_HPI,
	.res_num = SCR_HPI_RES_NUM,
 };

const domain_res_t g_sys_cnt_res =
{
	.res_id[0] = RES_SYS_CNT_SYS_CNT_RW,
	.res_id[1] = RES_SYS_CNT_SYS_CNT_RO,
	.res_num = SYS_CNT_RES_NUM,
 };

const domain_res_t g_sec_storage_res =
{
	.res_id[0] = RES_SEC_STORAGE_SEC_STORAGE2,
	.res_num = SEC_STORAGE_RES_NUM,
 };

const domain_res_t g_ftbu_res =
{
	.res_id[0] = RES_FTBU_FTBU4_2,
	.res_num = FTBU_RES_NUM,
 };

const domain_res_t g_cssys_res =
{
	.res_id[0] = RES_CSSYS_CSSYS,
	.res_num = CSSYS_RES_NUM,
 };

const domain_res_t g_ddr_cfg_res =
{
	.res_id[0] = RES_DDR_CFG_DDR_CFG,
	.res_num = DDR_CFG_RES_NUM,
 };


// domain res define from Rpc sheet
const domain_res_t g_iomuxc_res =
{
	.res_id[0] = RES_INPUT_SELECT_SEC_I2S_MC1_SDI3_SDO3,
	.res_id[1] = RES_INPUT_SELECT_SEC_I2S_MC1_SDI2_SDO2,
	.res_id[2] = RES_INPUT_SELECT_SEC_I2S_MC1_SDI1_SDO1,
	.res_id[3] = RES_INPUT_SELECT_SEC_I2S_MC1_SDI0_SDO0,
	.res_id[4] = RES_INPUT_SELECT_SEC_I2S_MC1_WSI,
	.res_id[5] = RES_INPUT_SELECT_SEC_I2S_MC1_SCKI,
	.res_id[6] = RES_INPUT_SELECT_SEC_I2S_MC1_SDI7_SDO7,
	.res_id[7] = RES_INPUT_SELECT_SEC_I2S_SC7_SDI_SDO,
	.res_id[8] = RES_INPUT_SELECT_SEC_I2S_MC1_SDI6_SDO6,
	.res_id[9] = RES_INPUT_SELECT_SEC_I2S_SC8_WS,
	.res_id[10] = RES_INPUT_SELECT_SEC_I2S_MC1_SDI5_SDO5,
	.res_id[11] = RES_INPUT_SELECT_SEC_I2S_SC8_SCK,
	.res_id[12] = RES_INPUT_SELECT_SEC_I2S_MC1_SDI4_SDO4,
	.res_id[13] = RES_INPUT_SELECT_SEC_I2S_SC8_SDI_SDO,
	.res_id[14] = RES_INPUT_SELECT_SEC_I2S_MC1_WSO,
	.res_id[15] = RES_INPUT_SELECT_SEC_I2S_SC7_WS,
	.res_id[16] = RES_INPUT_SELECT_SEC_I2S_MC1_SCKO,
	.res_id[17] = RES_INPUT_SELECT_SEC_I2S_SC7_SCK,
	.res_id[18] = RES_INPUT_SELECT_SEC_I2S_SC5_SDI_SDO,
	.res_id[19] = RES_INPUT_SELECT_SEC_I2S_SC6_SDI_SDO,
	.res_id[20] = RES_INPUT_SELECT_SEC_I2S_SC4_SDI_SDO,
	.res_id[21] = RES_INPUT_SELECT_SEC_I2S_SC4_WS,
	.res_id[22] = RES_INPUT_SELECT_SEC_I2S_SC4_SCK,
	.res_id[23] = RES_INPUT_SELECT_SEC_I2S_SC3_SDI_SDO,
	.res_id[24] = RES_INPUT_SELECT_SEC_I2S_SC3_WS,
	.res_id[25] = RES_INPUT_SELECT_SEC_I2S_SC3_SCK,
	.res_id[26] = RES_INPUT_SELECT_SEC_I2S_MC2_SCKI,
	.res_id[27] = RES_INPUT_SELECT_SEC_I2S_MC2_WSI,
	.res_id[28] = RES_INPUT_SELECT_SEC_OSPI2_DQS,
	.res_id[29] = RES_INPUT_SELECT_SEC_I2S_MC2_SDI7_SDO7,
	.res_id[30] = RES_INPUT_SELECT_SEC_I2S_MC2_SDI6_SDO6,
	.res_id[31] = RES_INPUT_SELECT_SEC_I2S_MC2_SDI5_SDO5,
	.res_id[32] = RES_INPUT_SELECT_SEC_I2S_MC2_SDI4_SDO4,
	.res_id[33] = RES_INPUT_SELECT_SEC_I2S_MC2_SDI3_SDO3,
	.res_id[34] = RES_INPUT_SELECT_SEC_I2S_MC2_SDI2_SDO2,
	.res_id[35] = RES_INPUT_SELECT_SEC_I2S_MC2_SDI1_SDO1,
	.res_id[36] = RES_INPUT_SELECT_SEC_I2S_MC2_SDI0_SDO0,
	.res_id[37] = RES_INPUT_SELECT_SEC_I2S_MC2_WSO,
	.res_id[38] = RES_INPUT_SELECT_SEC_I2S_MC2_SCKO,
	.res_id[39] = RES_INPUT_SELECT_SEC_I2C16_SDA,
	.res_id[40] = RES_INPUT_SELECT_SEC_I2C16_SCL,
	.res_id[41] = RES_INPUT_SELECT_SEC_I2C15_SDA,
	.res_id[42] = RES_INPUT_SELECT_SEC_I2C15_SCL,
	.res_id[43] = RES_INPUT_SELECT_SEC_TIMER6_CH3,
	.res_id[44] = RES_INPUT_SELECT_SEC_UART16_RX,
	.res_id[45] = RES_INPUT_SELECT_SEC_SPI7_SS,
	.res_id[46] = RES_INPUT_SELECT_SEC_TIMER6_CH2,
	.res_id[47] = RES_INPUT_SELECT_SEC_SPI7_MOSI,
	.res_id[48] = RES_INPUT_SELECT_SEC_UART16_CTS,
	.res_id[49] = RES_INPUT_SELECT_SEC_UART15_RX,
	.res_id[50] = RES_INPUT_SELECT_SEC_SPI7_MISO,
	.res_id[51] = RES_INPUT_SELECT_SEC_SPI7_SCLK,
	.res_id[52] = RES_INPUT_SELECT_SEC_I2C12_SDA,
	.res_id[53] = RES_INPUT_SELECT_SEC_UART14_RX,
	.res_id[54] = RES_INPUT_SELECT_SEC_SPDIF4_IN,
	.res_id[55] = RES_INPUT_SELECT_SEC_I2C12_SCL,
	.res_id[56] = RES_INPUT_SELECT_SEC_UART14_CTS,
	.res_id[57] = RES_INPUT_SELECT_SEC_UART13_RX,
	.res_id[58] = RES_INPUT_SELECT_SEC_SPI8_SS,
	.res_id[59] = RES_INPUT_SELECT_SEC_SPDIF3_IN,
	.res_id[60] = RES_INPUT_SELECT_SEC_SPI8_MOSI,
	.res_id[61] = RES_INPUT_SELECT_SEC_SPI8_MISO,
	.res_id[62] = RES_INPUT_SELECT_SEC_SPI8_SCLK,
	.res_id[63] = RES_INPUT_SELECT_SEC_PCIE_SS_PCIEX2_WAKE_N,
	.res_id[64] = RES_INPUT_SELECT_SEC_CANFD8_RX,
	.res_id[65] = RES_INPUT_SELECT_SEC_I2C14_SDA,
	.res_id[66] = RES_INPUT_SELECT_SEC_PCIE_SS_PCIEX2_PERST_N,
	.res_id[67] = RES_INPUT_SELECT_SEC_I2C14_SCL,
	.res_id[68] = RES_INPUT_SELECT_SEC_PCIE_SS_PCIEX1_WAKE_N,
	.res_id[69] = RES_INPUT_SELECT_SEC_CANFD7_RX,
	.res_id[70] = RES_INPUT_SELECT_SEC_I2C13_SDA,
	.res_id[71] = RES_INPUT_SELECT_SEC_PCIE_SS_PCIEX1_PERST_N,
	.res_id[72] = RES_INPUT_SELECT_SEC_I2C13_SCL,
	.res_id[73] = RES_INPUT_SELECT_SEC_UART12_RX,
	.res_id[74] = RES_INPUT_SELECT_SEC_SPI5_SS,
	.res_id[75] = RES_INPUT_SELECT_SEC_SPI5_MOSI,
	.res_id[76] = RES_INPUT_SELECT_SEC_UART11_RX,
	.res_id[77] = RES_INPUT_SELECT_SEC_SPI5_MISO,
	.res_id[78] = RES_INPUT_SELECT_SEC_SPI5_SCLK,
	.res_id[79] = RES_INPUT_SELECT_SEC_I2C8_SDA,
	.res_id[80] = RES_INPUT_SELECT_SEC_MSHC3_VOLT_SW,
	.res_id[81] = RES_INPUT_SELECT_SEC_ENET2_MDIO,
	.res_id[82] = RES_INPUT_SELECT_SEC_I2C8_SCL,
	.res_id[83] = RES_INPUT_SELECT_SEC_PCIE_SS_PCIEX1_CLKREQ_N,
	.res_id[84] = RES_INPUT_SELECT_SEC_MSHC3_WP,
	.res_id[85] = RES_INPUT_SELECT_SEC_I2C7_SDA,
	.res_id[86] = RES_INPUT_SELECT_SEC_PCIE_SS_PCIEX2_CLKREQ_N,
	.res_id[87] = RES_INPUT_SELECT_SEC_MSHC3_CARD_DET_N,
	.res_id[88] = RES_INPUT_SELECT_SEC_I2C7_SCL,
	.res_id[89] = RES_INPUT_SELECT_SEC_USB_SS_USB2_OC,
	.res_id[90] = RES_INPUT_SELECT_SEC_CANFD6_RX,
	.res_id[91] = RES_INPUT_SELECT_SEC_SPI6_SS,
	.res_id[92] = RES_INPUT_SELECT_SEC_I2C6_SDA,
	.res_id[93] = RES_INPUT_SELECT_SEC_SPI6_MOSI,
	.res_id[94] = RES_INPUT_SELECT_SEC_I2C6_SCL,
	.res_id[95] = RES_INPUT_SELECT_SEC_USB_SS_USB1_OC,
	.res_id[96] = RES_INPUT_SELECT_SEC_MSHC4_WP,
	.res_id[97] = RES_INPUT_SELECT_SEC_CANFD5_RX,
	.res_id[98] = RES_INPUT_SELECT_SEC_SPI6_MISO,
	.res_id[99] = RES_INPUT_SELECT_SEC_I2C5_SDA,
	.res_id[100] = RES_INPUT_SELECT_SEC_MSHC4_CARD_DET_N,
	.res_id[101] = RES_INPUT_SELECT_SEC_SPI6_SCLK,
	.res_id[102] = RES_INPUT_SELECT_SEC_I2C5_SCL,
	.res_id[103] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO107,
	.res_id[104] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO106,
	.res_id[105] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO105,
	.res_id[106] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO104,
	.res_id[107] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO103,
	.res_id[108] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO102,
	.res_id[109] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO101,
	.res_id[110] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO100,
	.res_id[111] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO99,
	.res_id[112] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO98,
	.res_id[113] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO97,
	.res_id[114] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO96,
	.res_id[115] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO95,
	.res_id[116] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO94,
	.res_id[117] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO93,
	.res_id[118] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO92,
	.res_id[119] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO91,
	.res_id[120] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO90,
	.res_id[121] = RES_MUX_CONTROL_SEC_I2C5_SDA,
	.res_id[122] = RES_MUX_CONTROL_SEC_I2C5_SCL,
	.res_id[123] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO87,
	.res_id[124] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO86,
	.res_id[125] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO85,
	.res_id[126] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO84,
	.res_id[127] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO83,
	.res_id[128] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO82,
	.res_id[129] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO81,
	.res_id[130] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO80,
	.res_id[131] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO79,
	.res_id[132] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO78,
	.res_id[133] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO77,
	.res_id[134] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO76,
	.res_id[135] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO75,
	.res_id[136] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO74,
	.res_id[137] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO73,
	.res_id[138] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO72,
	.res_id[139] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO71,
	.res_id[140] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO70,
	.res_id[141] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO69,
	.res_id[142] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO68,
	.res_id[143] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO67,
	.res_id[144] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO66,
	.res_id[145] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO65,
	.res_id[146] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO64,
	.res_id[147] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO63,
	.res_id[148] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO62,
	.res_id[149] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO61,
	.res_id[150] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO60,
	.res_id[151] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO59,
	.res_id[152] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO58,
	.res_id[153] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO57,
	.res_id[154] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO56,
	.res_id[155] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO55,
	.res_id[156] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO54,
	.res_id[157] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO53,
	.res_id[158] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO52,
	.res_id[159] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO51,
	.res_id[160] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO50,
	.res_id[161] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO49,
	.res_id[162] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO48,
	.res_id[163] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO47,
	.res_id[164] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO46,
	.res_id[165] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO45,
	.res_id[166] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO44,
	.res_id[167] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO43,
	.res_id[168] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO42,
	.res_id[169] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO41,
	.res_id[170] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO40,
	.res_id[171] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO39,
	.res_id[172] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO38,
	.res_id[173] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO37,
	.res_id[174] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO36,
	.res_id[175] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO35,
	.res_id[176] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO34,
	.res_id[177] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO33,
	.res_id[178] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO32,
	.res_id[179] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO31,
	.res_id[180] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO30,
	.res_id[181] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO29,
	.res_id[182] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO28,
	.res_id[183] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO27,
	.res_id[184] = RES_MUX_CONTROL_SEC_WDT3_RESET_N,
	.res_id[185] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO25,
	.res_id[186] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO24,
	.res_id[187] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO23,
	.res_id[188] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO22,
	.res_id[189] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO21,
	.res_id[190] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO20,
	.res_id[191] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO19,
	.res_id[192] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO18,
	.res_id[193] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO17,
	.res_id[194] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO16,
	.res_id[195] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO15,
	.res_id[196] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO14,
	.res_id[197] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO13,
	.res_id[198] = RES_MUX_CONTROL_SEC_GPIO_MUX2_IO12,
	.res_id[199] = RES_MUX_CONTROL_SEC_SPI5_SCLK,
	.res_id[200] = RES_MUX_CONTROL_SEC_SPI5_MISO,
	.res_id[201] = RES_MUX_CONTROL_SEC_SPI5_MOSI,
	.res_id[202] = RES_MUX_CONTROL_SEC_SPI5_SS,
	.res_id[203] = RES_MUX_CONTROL_SEC_UART10_RX,
	.res_id[204] = RES_MUX_CONTROL_SEC_UART10_TX,
	.res_id[205] = RES_MUX_CONTROL_SEC_UART9_RX,
	.res_id[206] = RES_MUX_CONTROL_SEC_UART9_TX,
	.res_id[207] = RES_MUX_CONTROL_SEC_TMR3_CH3,
	.res_id[208] = RES_MUX_CONTROL_SEC_TMR3_CH2,
	.res_id[209] = RES_MUX_CONTROL_SEC_TMR3_CH1,
	.res_id[210] = RES_MUX_CONTROL_SEC_TMR3_CH0,
	.res_id[211] = RES_PAD_CONTROL_SEC_I2C5_SDA,
	.res_id[212] = RES_PAD_CONTROL_SEC_I2C5_SCL,
	.res_id[213] = RES_PAD_CONTROL_SEC_WDT3_RESET_N,
	.res_id[214] = RES_PAD_CONTROL_SEC_SPI5_SCLK,
	.res_id[215] = RES_PAD_CONTROL_SEC_SPI5_MISO,
	.res_id[216] = RES_PAD_CONTROL_SEC_SPI5_MOSI,
	.res_id[217] = RES_PAD_CONTROL_SEC_SPI5_SS,
	.res_id[218] = RES_PAD_CONTROL_SEC_UART9_RX,
	.res_id[219] = RES_PAD_CONTROL_SEC_UART9_TX,
	.res_id[220] = RES_PAD_CONTROL_SEC_TMR3_CH3,
	.res_id[221] = RES_PAD_CONTROL_SEC_TMR3_CH2,
	.res_id[222] = RES_PAD_CONTROL_SEC_TMR3_CH1,
	.res_id[223] = RES_PAD_CONTROL_SEC_TMR3_CH0,
	.res_num = IOMUXC_RES_NUM,
 };

const domain_res_t g_rstgen_res =
{
	.res_id[0] = RES_GLOBAL_RST_SEC_RST_EN,
	.res_id[1] = RES_MODULE_RST_SEC_ADC,
	.res_id[2] = RES_MODULE_RST_SEC_CE2,
	.res_id[3] = RES_MODULE_RST_SEC_DBG_REQ,
	.res_id[4] = RES_MODULE_RST_SEC_DISP_MUX,
	.res_id[5] = RES_MODULE_RST_SEC_LVDS_SS,
	.res_id[6] = RES_MODULE_RST_SEC_NNA,
	.res_id[7] = RES_MODULE_RST_SEC_CSSYS_TRESET_N,
	.res_id[8] = RES_MODULE_RST_SEC_DDR_SW_3,
	.res_id[9] = RES_MODULE_RST_SEC_DDR_SW_2,
	.res_id[10] = RES_MODULE_RST_SEC_DDR_SW_1,
	.res_id[11] = RES_MODULE_RST_SEC_DDR_SS,
	.res_id[12] = RES_MODULE_RST_SEC_GIC2,
	.res_id[13] = RES_MODULE_RST_SEC_MSHC4,
	.res_id[14] = RES_MODULE_RST_SEC_MSHC3,
	.res_id[15] = RES_MODULE_RST_SEC_MSHC2,
	.res_id[16] = RES_MODULE_RST_SEC_MSHC1,
	.res_id[17] = RES_MODULE_RST_SEC_ENET2,
	.res_id[18] = RES_MODULE_RST_SEC_OSPI2,
	.res_id[19] = RES_ISO_EN_SEC_DDR,
	.res_id[20] = RES_GENERAL_REG_SEC_GENERAL_REG2,
	.res_id[21] = RES_SW_SEC_RSTGEN_RST_STA,
	.res_id[22] = RES_SW_SEC_RSTGEN_SW_SELF_RST,
	.res_id[23] = RES_SW_SEC_RSTGEN_SW_OTH_RST,
	.res_id[24] = RES_CORE_RST_SEC_CR5_SEC_SW,
	.res_id[25] = RES_CORE_RST_SEC_CR5_SEC_EN,
	.res_num = RSTGEN_RES_NUM,
 };

const domain_res_t g_scr_res =
{
	.res_id[0] = RES_SCR_R16W16_SEC_CR5_SEC_CR5_MP,
	.res_id[1] = RES_SCR_R16W16_SEC_CR5_SEC,
	.res_id[2] = RES_SCR_R16W16_SEC_NOC_VSN_M_S,
	.res_id[3] = RES_SCR_R16W16_SEC_NOC_MAIN_M_S,
	.res_id[4] = RES_SCR_R16W16_SEC_NOC_SEC_M_S,
	.res_id[5] = RES_SCR_R16W16_SEC_TCU,
	.res_id[6] = RES_SCR_R16W16_SEC_TBU15,
	.res_id[7] = RES_SCR_R16W16_SEC_TBU14,
	.res_id[8] = RES_SCR_R16W16_SEC_TBU10,
	.res_id[9] = RES_SCR_R16W16_SEC_TBU9,
	.res_id[10] = RES_SCR_R16W16_SEC_TBU8,
	.res_id[11] = RES_SCR_R16W16_SEC_TBU6,
	.res_id[12] = RES_SCR_R16W16_SEC_TBU5,
	.res_id[13] = RES_SCR_R16W16_SEC_TBU4,
	.res_id[14] = RES_SCR_R16W16_SEC_TBU3,
	.res_id[15] = RES_SCR_R16W16_SEC_TBU2,
	.res_id[16] = RES_SCR_R16W16_SEC_TBU1,
	.res_id[17] = RES_SCR_R16W16_SEC_TBU0,
	.res_id[18] = RES_SCR_R16W16_SEC_DDR_SS_CSYSREQ,
	.res_id[19] = RES_SCR_L31_SEC_IOMUX_WRAP_AP_PIO_DISP_MUX,
	.res_id[20] = RES_SCR_L31_SEC_CSSYS_TP_MAXDATASIZE_3_0,
	.res_id[21] = RES_SCR_L31_SEC_CMDTAG_NOC2DDR_BYPASS,
	.res_id[22] = RES_SCR_L31_SEC_CSSYS_DBGEN,
	.res_id[23] = RES_SCR_L31_SEC_CR5_SEC_DBGEN,
	.res_id[24] = RES_SCR_L31_SEC_REMAP_CR5_SEC_AW_AR,
	.res_id[25] = RES_SCR_L31_SEC_PVT_SNS_SEC_FUSE_PVT_11_0,
	.res_id[26] = RES_SCR_L31_SEC_CPU1_DBGEN,
	.res_id[27] = RES_SCR_L16_SEC_CR5_SEC_DCCMINP,
	.res_id[28] = RES_SCR_L16_SEC_MPC_RPC_RESP_ERR_DIS,
	.res_id[29] = RES_SCR_L16_SEC_PPC_RESP_ERR_DIS,
	.res_id[30] = RES_SCR_L16_SEC_MAC_MPC_PPC_RESP_ERR_DIS,
	.res_id[31] = RES_SCR_L16_SEC_FSREFCLK_DDR_SS,
	.res_id[32] = RES_SCR_L16_SEC_FSREFCLK_DISP,
	.res_id[33] = RES_SCR_L16_SEC_FSREFCLK_VSN,
	.res_id[34] = RES_SCR_L16_SEC_FSREFCLK_VPU,
	.res_id[35] = RES_SCR_L16_SEC_FSREFCLK_HIS,
	.res_id[36] = RES_SCR_L16_SEC_FSREFCLK_GPU2,
	.res_id[37] = RES_SCR_L16_SEC_FSREFCLK_GPU1,
	.res_id[38] = RES_SCR_L16_SEC_FSREFCLK_CPU1,
	.res_id[39] = RES_SCR_L16_SEC_FSREFCLK_HPI,
	.res_id[40] = RES_SCR_L16_SEC_FSREFCLK_SEC,
	.res_id[41] = RES_SCR_L16_SEC_TBU_BYP_MUX_BYPASS,
	.res_id[42] = RES_SCR_L16_SEC_GPIO_MUX_SEC_GPIO_SEL_215_208,
	.res_id[43] = RES_SCR_L16_SEC_GPIO_MUX_SEC_GPIO_SEL_207_192,
	.res_id[44] = RES_SCR_L16_SEC_GPIO_MUX_SEC_GPIO_SEL_191_176,
	.res_id[45] = RES_SCR_L16_SEC_GPIO_MUX_SEC_GPIO_SEL_175_160,
	.res_id[46] = RES_SCR_L16_SEC_GPIO_MUX_SEC_GPIO_SEL_159_144,
	.res_id[47] = RES_SCR_L16_SEC_GPIO_MUX_SEC_GPIO_SEL_143_128,
	.res_id[48] = RES_SCR_L16_SEC_GPIO_MUX_SEC_GPIO_SEL_127_112,
	.res_id[49] = RES_SCR_L16_SEC_GPIO_MUX_SEC_GPIO_SEL_111_96,
	.res_id[50] = RES_SCR_L16_SEC_GPIO_MUX_SEC_GPIO_SEL_95_80,
	.res_id[51] = RES_SCR_L16_SEC_GPIO_MUX_SEC_GPIO_SEL_79_64,
	.res_id[52] = RES_SCR_L16_SEC_GPIO_MUX_SEC_GPIO_SEL_63_48,
	.res_id[53] = RES_SCR_L16_SEC_GPIO_MUX_SEC_GPIO_SEL_47_32,
	.res_id[54] = RES_SCR_L16_SEC_GPIO_MUX_SEC_GPIO_SEL_31_16,
	.res_id[55] = RES_SCR_L16_SEC_GPIO_MUX_SEC_GPIO_SEL_15_0,
	.res_id[56] = RES_SCR_L16_SEC_CE2_KEY_PERCTRL_SCR_KEY_ENABLE_127_112,
	.res_id[57] = RES_SCR_L16_SEC_CE2_KEY_PERCTRL_SCR_KEY_ENABLE_111_96,
	.res_id[58] = RES_SCR_L16_SEC_CE2_KEY_PERCTRL_SCR_KEY_ENABLE_95_80,
	.res_id[59] = RES_SCR_L16_SEC_CE2_KEY_PERCTRL_SCR_KEY_ENABLE_79_64,
	.res_id[60] = RES_SCR_L16_SEC_CE2_KEY_PERCTRL_SCR_KEY_ENABLE_63_48,
	.res_id[61] = RES_SCR_L16_SEC_CE2_KEY_PERCTRL_SCR_KEY_ENABLE_47_32,
	.res_id[62] = RES_SCR_L16_SEC_CE2_KEY_PERCTRL_SCR_KEY_ENABLE_31_16,
	.res_id[63] = RES_SCR_L16_SEC_CE2_KEY_PERCTRL_SCR_KEY_ENABLE_15_0,
	.res_id[64] = RES_SCR_RO_SEC_XTAL_SAF_XTAL_RDY,
	.res_id[65] = RES_SCR_RO_SEC_U_INFO_CMP_O_AULTINFO,
	.res_id[66] = RES_SCR_RO_SEC_NOC_MAIN_S,
	.res_id[67] = RES_SCR_RO_SEC_NOC_MAIN_M,
	.res_id[68] = RES_SCR_RO_SEC_NOC_SEC_M_M_S,
	.res_id[69] = RES_SCR_RO_SEC_NOC_SEC_S,
	.res_id[70] = RES_SCR_RO_SEC_NOC_SEC_M_0_9_I_MAINNOPENDINGTRANS,
	.res_id[71] = RES_SCR_RO_SEC_RSTGEN_SEC_MAIN_RST,
	.res_id[72] = RES_SCR_RO_SEC_EFUSE,
	.res_id[73] = RES_SCR_RO_SEC_RSTGEN_SAF_BOOT_MODE_SCR,
	.res_id[74] = RES_SCR_RW_SEC__SPARE4SPI,
	.res_id[75] = RES_SCR_RW_SEC_NOC_MAIN,
	.res_id[76] = RES_SCR_RW_SEC_DDR_SS_PWROKIN_AON,
	.res_id[77] = RES_SCR_RW_SEC_SEC_DEBUG_MODE,
	.res_id[78] = RES_SCR_RW_SEC_TIMER3_LP_MODE,
	.res_id[79] = RES_SCR_RW_SEC_SPI5_8_I_OPMODE,
	.res_id[80] = RES_SCR_RW_SEC_REMAP_CR5_SEC_AW_AR,
	.res_id[81] = RES_SCR_RW_SEC_DBG_MUX_PU_SEL,
	.res_id[82] = RES_SCR_RW_SEC_SECURITY_VIOLATION,
	.res_num = SCR_RES_NUM,
 };

const domain_res_t g_ckgen_res =
{
	.res_id[0] = RES_DBG_SEC_CKGEN_MON_MIN_DUTY,
	.res_id[1] = RES_DBG_SEC_CKGEN_MON_MAX_DUTY,
	.res_id[2] = RES_DBG_SEC_CKGEN_MON_MIN_FREQ,
	.res_id[3] = RES_DBG_SEC_CKGEN_MON_AVE_FREQ,
	.res_id[4] = RES_DBG_SEC_CKGEN_MON_MAX_FREQ,
	.res_id[5] = RES_DBG_SEC_CKGEN_MON_CTL,
	.res_id[6] = RES_DBG_SEC_CKGEN_CORE_SLICE_MON_CTL,
	.res_id[7] = RES_DBG_SEC_CKGEN_BUS_SLICE_MON_CTL,
	.res_id[8] = RES_DBG_SEC_CKGEN_IP_SLICE_MON_CTL,
	.res_id[9] = RES_DBG_SEC_CKGEN_DBG_CTL,
	.res_id[10] = RES_DBG_SEC_CKGEN_CORE_SLICE_DBG_CTL,
	.res_id[11] = RES_DBG_SEC_CKGEN_BUS_SLICE_DBG_CTL,
	.res_id[12] = RES_DBG_SEC_CKGEN_IP_SLICE_DBG_CTL,
	.res_id[13] = RES_GATING_EN_SEC_PLL7,
	.res_id[14] = RES_GATING_EN_SEC_PLL6,
	.res_id[15] = RES_GATING_EN_SEC_PLL5,
	.res_id[16] = RES_GATING_EN_SEC_PLL4,
	.res_id[17] = RES_GATING_EN_SEC_PLL3,
	.res_id[18] = RES_GATING_EN_SEC_WDT8,
	.res_id[19] = RES_GATING_EN_SEC_WDT3,
	.res_id[20] = RES_GATING_EN_SEC_GPIO2,
	.res_id[21] = RES_GATING_EN_SEC_TBU15,
	.res_id[22] = RES_GATING_EN_SEC_TBU14,
	.res_id[23] = RES_GATING_EN_SEC_SYS_CNT,
	.res_id[24] = RES_GATING_EN_SEC_TRACE,
	.res_id[25] = RES_GATING_EN_SEC_PWM3,
	.res_id[26] = RES_GATING_EN_SEC_TIMER7,
	.res_id[27] = RES_GATING_EN_SEC_TIMER3,
	.res_id[28] = RES_GATING_EN_SEC_SPDIF4,
	.res_id[29] = RES_GATING_EN_SEC_SPI5,
	.res_id[30] = RES_GATING_EN_SEC_I2C5,
	.res_id[31] = RES_GATING_EN_SEC_ROMC2,
	.res_id[32] = RES_GATING_EN_SEC_MB,
	.res_id[33] = RES_GATING_EN_SEC_OSPI2,
	.res_id[34] = RES_GATING_EN_SEC_EMMC4,
	.res_id[35] = RES_GATING_EN_SEC_EMMC3,
	.res_id[36] = RES_GATING_EN_SEC_EMMC2,
	.res_id[37] = RES_GATING_EN_SEC_MSHC_TIMER,
	.res_id[38] = RES_GATING_EN_SEC_EMMC1,
	.res_id[39] = RES_GATING_EN_SEC_GIC2,
	.res_id[40] = RES_GATING_EN_SEC_CE2,
	.res_id[41] = RES_GATING_EN_SEC_SEC_PLAT,
	.res_id[42] = RES_BUS_SLICE_SEC_SEC_PLAT_GASKET,
	.res_id[43] = RES_BUS_SLICE_SEC_SEC_PLAT_CTL,
	.res_id[44] = RES_IP_SLICE_SEC_HPI_CLK800,
	.res_id[45] = RES_IP_SLICE_SEC_HPI_CLK600,
	.res_id[46] = RES_IP_SLICE_SEC_MSHC_TIMER,
	.res_id[47] = RES_IP_SLICE_SEC_SYS_CNT,
	.res_id[48] = RES_IP_SLICE_SEC_TRACE,
	.res_id[49] = RES_IP_SLICE_SEC_TIMER3,
	.res_id[50] = RES_IP_SLICE_SEC_OSPI2,
	.res_id[51] = RES_IP_SLICE_SEC_EMMC4,
	.res_id[52] = RES_IP_SLICE_SEC_EMMC3,
	.res_id[53] = RES_IP_SLICE_SEC_EMMC2,
	.res_id[54] = RES_IP_SLICE_SEC_EMMC1,
	.res_id[55] = RES_IP_SLICE_SEC_UART_SEC1,
	.res_id[56] = RES_IP_SLICE_SEC_UART_SEC0,
	.res_id[57] = RES_IP_SLICE_SEC_SPI_SEC1,
	.res_id[58] = RES_IP_SLICE_SEC_SPI_SEC0,
	.res_id[59] = RES_IP_SLICE_SEC_I2C_SEC1,
	.res_id[60] = RES_IP_SLICE_SEC_I2C_SEC0,
	.res_id[61] = RES_IP_SLICE_SEC_ADC,
	.res_id[62] = RES_IP_SLICE_SEC_CE2,
	.res_id[63] = RES_DBG_DISP_CKGEN_MON_MIN_DUTY,
	.res_id[64] = RES_DBG_DISP_CKGEN_MON_MAX_DUTY,
	.res_id[65] = RES_DBG_DISP_CKGEN_MON_MIN_FREQ,
	.res_id[66] = RES_DBG_DISP_CKGEN_MON_AVE_FREQ,
	.res_id[67] = RES_DBG_DISP_CKGEN_MON_MAX_FREQ,
	.res_id[68] = RES_DBG_DISP_CKGEN_MON_CTL,
	.res_id[69] = RES_DBG_DISP_CKGEN_CORE_SLICE_MON_CTL,
	.res_id[70] = RES_DBG_DISP_CKGEN_BUS_SLICE_MON_CTL,
	.res_id[71] = RES_DBG_DISP_CKGEN_IP_SLICE_MON_CTL,
	.res_id[72] = RES_DBG_DISP_CKGEN_DBG_CTL,
	.res_id[73] = RES_DBG_DISP_CKGEN_CORE_SLICE_DBG_CTL,
	.res_id[74] = RES_DBG_DISP_CKGEN_BUS_SLICE_DBG_CTL,
	.res_id[75] = RES_DBG_DISP_CKGEN_IP_SLICE_DBG_CTL,
	.res_id[76] = RES_GATING_EN_DISP_MIPI_CSI3_PIX,
	.res_id[77] = RES_GATING_EN_DISP_MIPI_CSI2_PIX,
	.res_id[78] = RES_GATING_EN_DISP_MIPI_CSI1_PIX,
	.res_id[79] = RES_GATING_EN_DISP_DISP_MUX,
	.res_id[80] = RES_GATING_EN_DISP_LVDS_SS,
	.res_id[81] = RES_GATING_EN_DISP_MIPI_DSI2,
	.res_id[82] = RES_GATING_EN_DISP_MIPI_DSI1,
	.res_id[83] = RES_GATING_EN_DISP_LVDS_CLK_2,
	.res_id[84] = RES_GATING_EN_DISP_LVDS_CLK_1,
	.res_id[85] = RES_DBG_SOC_CKGEN_MON_MIN_DUTY,
	.res_id[86] = RES_DBG_SOC_CKGEN_MON_MAX_DUTY,
	.res_id[87] = RES_DBG_SOC_CKGEN_MON_MIN_FREQ,
	.res_id[88] = RES_DBG_SOC_CKGEN_MON_AVE_FREQ,
	.res_id[89] = RES_DBG_SOC_CKGEN_MON_MAX_FREQ,
	.res_id[90] = RES_DBG_SOC_CKGEN_MON_CTL,
	.res_id[91] = RES_DBG_SOC_CKGEN_CORE_SLICE_MON_CTL,
	.res_id[92] = RES_DBG_SOC_CKGEN_BUS_SLICE_MON_CTL,
	.res_id[93] = RES_DBG_SOC_CKGEN_IP_SLICE_MON_CTL,
	.res_id[94] = RES_DBG_SOC_CKGEN_DBG_CTL,
	.res_id[95] = RES_DBG_SOC_CKGEN_CORE_SLICE_DBG_CTL,
	.res_id[96] = RES_DBG_SOC_CKGEN_BUS_SLICE_DBG_CTL,
	.res_id[97] = RES_DBG_SOC_CKGEN_IP_SLICE_DBG_CTL,
	.res_id[98] = RES_UUU_WRAP_SOC_DDR,
	.res_id[99] = RES_GATING_EN_SOC_VSN_BUS_1_EIC_VSN,
	.res_id[100] = RES_GATING_EN_SOC_VSN_BUS_1_BIPC_VDSP_PCLK,
	.res_id[101] = RES_GATING_EN_SOC_VSN_BUS_0_BIPC_VDSP_ACLK,
	.res_id[102] = RES_GATING_EN_SOC_VSN_BUS_1_PLL_VSN,
	.res_id[103] = RES_GATING_EN_SOC_VSN_BUS_1_NOC_VSP,
	.res_id[104] = RES_GATING_EN_SOC_VSN_BUS_0_VDSP_CLK,
	.res_id[105] = RES_GATING_EN_SOC_EIC_HPI,
	.res_id[106] = RES_GATING_EN_SOC_BIPC_DDR,
	.res_id[107] = RES_GATING_EN_SOC_TBU10,
	.res_id[108] = RES_GATING_EN_SOC_TBU9,
	.res_id[109] = RES_GATING_EN_SOC_TBU8,
	.res_id[110] = RES_GATING_EN_SOC_TBU6,
	.res_id[111] = RES_GATING_EN_SOC_TBU5,
	.res_id[112] = RES_GATING_EN_SOC_TBU4,
	.res_id[113] = RES_GATING_EN_SOC_TBU3,
	.res_id[114] = RES_GATING_EN_SOC_TBU2,
	.res_id[115] = RES_GATING_EN_SOC_TBU1,
	.res_id[116] = RES_GATING_EN_SOC_TBU0,
	.res_id[117] = RES_GATING_EN_SOC_TCU,
	.res_id[118] = RES_CORE_SLICE_SOC_DDR,
	.res_id[119] = RES_BUS_SLICE_SOC_NOC_BUS_CLOCK_GASKET,
	.res_id[120] = RES_BUS_SLICE_SOC_NOC_BUS_CLOCK_CTL,
	.res_id[121] = RES_BUS_SLICE_SOC_VSN_BUS_GASKET,
	.res_id[122] = RES_BUS_SLICE_SOC_VSN_BUS_CTL,
	.res_id[123] = RES_BUS_SLICE_SOC_VPU_BUS_GASKET,
	.res_id[124] = RES_BUS_SLICE_SOC_VPU_BUS_CTL,
	.res_num = CKGEN_RES_NUM,
 };

static const domain_res_t * g_res_cat[] = {
	NULL, /*iram*/
	&g_mb_res,
	NULL, /*gpu*/
	&g_ddr_res,
	NULL, /*pcie*/
	&g_gic_res,
	&g_ospi_res,
	&g_romc_res,
	&g_platform_res,
	&g_ce_mem_res,
	&g_vdsp_res,
	&g_dma_res,
	&g_mshc_res,
	&g_mac_res,
	&g_gpv_res,
	&g_spi_res,
	&g_ce_reg_res,
	NULL, /*sem*/
	NULL, /*i2s_sc*/
	NULL, /*bipc*/
	NULL, /*eic*/
	NULL, /*pvt_sens*/
	NULL, /*pwrctrl*/
	&g_pll_res,
	NULL, /*rc24m*/
	&g_xtal_res,
	&g_romc_reg_res,
	NULL, /*iram_reg*/
	&g_dma_mux_res,
	&g_watchdog_res,
	NULL, /*pwm*/
	&g_timer_res,
	NULL, /*enet_qos*/
	&g_uart_res,
	&g_i2c_res,
	NULL, /*can*/
	&g_ospi_reg_res,
	&g_efuse_res,
	&g_gpio_res,
	&g_gpv_sec_m_reg_res,
	NULL, /*i2s_mc*/
	NULL, /*spdif*/
	NULL, /*adc*/
	NULL, /*dc*/
	NULL, /*dp*/
	NULL, /*mipi_dsi*/
	&g_disp_mux_res,
	NULL, /*mipi_csi*/
	NULL, /*csi*/
	NULL, /*g2d*/
	&g_lvds_res,
	NULL, /*dma_ch_mux_pcie*/
	&g_usbphy_res,
	&g_usb_res,
	NULL, /*pcie_phy*/
	&g_scr_hpi_res,
	NULL, /*vpu*/
	NULL, /*mjpeg*/
	&g_sys_cnt_res,
	&g_sec_storage_res,
	NULL, /*rstgen_rtc*/
	NULL, /*tm*/
	NULL, /*iomuxc_rtc*/
	NULL, /*pmu*/
	NULL, /*rc_rtc*/
	NULL, /*rtc*/
	&g_ftbu_res,
	&g_cssys_res,
	NULL, /*scr4k_ssid*/
	NULL, /*scr4k_sid*/
	NULL, /*smmu_tcu*/
	&g_ddr_cfg_res,
	NULL, /*pcie_reg*/
	NULL, /*pcie_io*/
	&g_iomuxc_res,
	&g_rstgen_res,
	&g_scr_res,
	&g_ckgen_res,
};

#endif /* _DOMAIN_RES_H */
