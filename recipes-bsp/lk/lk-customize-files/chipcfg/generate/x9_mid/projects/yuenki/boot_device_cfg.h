#include "boot_device.h"

boot_device_cfg_t mmc1 =
{
    .device_type = MMC,
    .res_idex = RES_MSHC_SD1,
    .disk_name = "mmcblk0:",
    .storage_type = "emmc",
    .cfg = {
        .mmc_cfg = {
            .voltage = MMC_VOL_1_8,
            .max_clk_rate = MMC_CLK_200MHZ,
            .bus_width = MMC_BUS_WIDTH_8BIT,
            .hs400_support = 1,
        },
    },
};

boot_device_cfg_t mmc2 =
{
    .device_type = MMC,
    .res_idex = RES_MSHC_SD2,
    .disk_name = "mmcblk0:",
    .storage_type = "emmc",
    .cfg = {
        .mmc_cfg = {
            .voltage = MMC_VOL_1_8,
            .max_clk_rate = MMC_CLK_100MHZ,
            .bus_width = MMC_BUS_WIDTH_8BIT,
        },
    },
};

boot_device_cfg_t mmc3 =
{
    .device_type = MMC,
    .res_idex = RES_MSHC_SD3,
    .disk_name = "mmcblk1:",
    .storage_type = "sd",
    .cfg = {
        .mmc_cfg = {
            .voltage = MMC_VOL_3_3,
            .max_clk_rate = MMC_CLK_25MHZ,
            .bus_width = MMC_BUS_WIDTH_4BIT,
        },
    },
};

boot_device_cfg_t ospi1 =
{
    .device_type = OSPI,
    .res_idex = RES_OSPI_REG_OSPI1,
    .cfg = {
        .ospi_cfg = {
            .cs = SPI_NOR_CS0,
            .bus_clk = SPI_NOR_CLK_25MHZ,
            .octal_ddr_en = 0,
        },
    },
};

boot_device_cfg_t ospi2 =
{
    .device_type = OSPI,
    .res_idex = RES_OSPI_REG_OSPI2,
    .cfg = {
        .ospi_cfg = {
            .cs = SPI_NOR_CS0,
            .bus_clk = SPI_NOR_CLK_25MHZ,
            .octal_ddr_en = 0,
        },
    },
};

btdev_pin_cfg_t btdev_pin_mapping[] = {
    [0] = {
        .pin_val = BOOT_PIN_0,
        .ap = &mmc1,
        .safety = &ospi1,
    },
    [1] = {
        .pin_val = BOOT_PIN_1,
        .ap = &mmc1,
        .safety = &ospi1,
    },
    [2] = {
        .pin_val = BOOT_PIN_2,
        .ap = &mmc2,
        .safety = &ospi1,
    },
    [3] = {
        .pin_val = BOOT_PIN_3,
        .ap = &mmc3,
        .safety = &ospi1,
    },
    [4] = {
        .pin_val = BOOT_PIN_4,
        .ap = &ospi2,
        .safety = &ospi1,
    },
    [5] = {
        .pin_val = BOOT_PIN_5,
        .ap = NULL,
        .safety = &ospi1,
    },
    [6] = {
        .pin_val = BOOT_PIN_6,
        .ap = NULL,
        .safety = NULL,
    },
    [7] = {
        .pin_val = BOOT_PIN_7,
        .ap = NULL,
        .safety = NULL,
    },
    [8] = {
        .pin_val = BOOT_PIN_8,
        .ap = NULL,
        .safety = NULL,
    },
    [9] = {
        .pin_val = BOOT_PIN_9,
        .ap = &mmc1,
        .safety = NULL,
    },
    [10] = {
        .pin_val = BOOT_PIN_10,
        .ap = &mmc2,
        .safety = NULL,
    },
    [11] = {
        .pin_val = BOOT_PIN_11,
        .ap = &mmc3,
        .safety = NULL,
    },
    [12] = {
        .pin_val = BOOT_PIN_12,
        .ap = &ospi2,
        .safety = NULL,
    },
    [13] = {
        .pin_val = BOOT_PIN_13,
        .ap = NULL,
        .safety = NULL,
    },
    [14] = {
        .pin_val = BOOT_PIN_14,
        .ap = NULL,
        .safety = NULL,
    },
    [15] = {
        .pin_val = BOOT_PIN_15,
        .ap = NULL,
        .safety = NULL,
    },
};

