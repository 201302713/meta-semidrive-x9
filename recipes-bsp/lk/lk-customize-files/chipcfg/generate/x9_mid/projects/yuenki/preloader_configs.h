
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef __PRELOADER_CONFIGS_H__
#define __PRELOADER_CONFIGS_H__

#include "image_cfg.h"

#define CPU_ID_CURRENT CPU_ID_AP1
#define CONFIGS \
            PT_LOAD_CONFIG_ITEM(PT_SML, 0, ECO_ATF_MEMBASE, ECO_ATF_MEMSIZE, CPU_ID_AP1, PT_SAVE_F, atf) \
            /*TODO: Enable TOS when new memmap config applied.
	     * PT_LOAD_CONFIG_ITEM(PT_TOS, 1, ECO_TEE_MEMBASE, ECO_TEE_MEMSIZE, CPU_ID_AP1, PT_SAVE_F, tos) \
            */PT_LOAD_CONFIG_ITEM(PT_BL,  1, AP1_BOOTLOADER_MEMBASE, AP1_BOOTLOADER_MEMSIZE, CPU_ID_AP1, PT_KICK_F|PT_SAVE_F, bootloader) \

#endif
