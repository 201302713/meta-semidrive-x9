
//*****************************************************************************
//
// WARNING: Automatically generated file, don't modify anymore!!!
//
// Copyright (c) 2019-2029 Semidrive Incorporated.  All rights reserved.
// Software License Agreement
//
//*****************************************************************************

#ifndef _OS_RES_H
#define _OS_RES_H
#include "os_res_cnt.h"

/* Generated os resource declarations.
 * DO NOT MODIFY!
 */

// os res define from Resource Allocation sheet
const domain_res_t g_ddr_res =
{
	.res_id[0] = RES_DDR_DDR_MEM_VBMETA,
	.res_id[1] = RES_DDR_DDR_MEM_VDSP,
	.res_id[2] = RES_DDR_DDR_MEM_VDSP_SHARE,
	.res_id[3] = RES_DDR_DDR_MEM_SERVICES,
	.res_id[4] = RES_DDR_DDR_MEM_SAF_ECO,
	.res_id[5] = RES_DDR_DDR_MEM_SEC_ECO,
	.res_id[6] = RES_DDR_DDR_MEM_CLU,
	.res_num = DDR_RES_NUM,
 };

const domain_res_t g_gic_res =
{
	.res_id[0] = RES_GIC_GIC4,
	.res_num = GIC_RES_NUM,
 };

const domain_res_t g_ce_mem_res =
{
	.res_id[0] = RES_CE_MEM_CE2_VCE4,
	.res_num = CE_MEM_RES_NUM,
 };

const domain_res_t g_dma_res =
{
	.res_id[0] = RES_DMA_DMA4,
	.res_id[1] = RES_DMA_DMA3,
	.res_num = DMA_RES_NUM,
 };

const domain_res_t g_mshc_res =
{
	.res_id[0] = RES_MSHC_SD4,
	.res_num = MSHC_RES_NUM,
 };

const domain_res_t g_spi_res =
{
	.res_id[0] = RES_SPI_SPI8,
	.res_num = SPI_RES_NUM,
 };

const domain_res_t g_ce_reg_res =
{
	.res_id[0] = RES_CE_REG_CE2_VCE4,
	.res_num = CE_REG_RES_NUM,
 };

const domain_res_t g_i2s_sc_res =
{
	.res_id[0] = RES_I2S_SC_I2S_SC8,
	.res_num = I2S_SC_RES_NUM,
 };

const domain_res_t g_pll_res =
{
	.res_id[0] = RES_PLL_PLL_LVDS4,
	.res_num = PLL_RES_NUM,
 };

const domain_res_t g_dma_mux_res =
{
	.res_id[0] = RES_DMA_MUX_DMA_MUX4,
	.res_id[1] = RES_DMA_MUX_DMA_MUX3,
	.res_num = DMA_MUX_RES_NUM,
 };

const domain_res_t g_pwm_res =
{
	.res_id[0] = RES_PWM_PWM8,
	.res_id[1] = RES_PWM_PWM7,
	.res_num = PWM_RES_NUM,
 };

const domain_res_t g_timer_res =
{
	.res_id[0] = RES_TIMER_TIMER8,
	.res_num = TIMER_RES_NUM,
 };

const domain_res_t g_uart_res =
{
	.res_id[0] = RES_UART_UART16,
	.res_id[1] = RES_UART_UART15,
	.res_id[2] = RES_UART_UART14,
	.res_id[3] = RES_UART_UART9,
	.res_num = UART_RES_NUM,
 };

const domain_res_t g_i2c_res =
{
	.res_id[0] = RES_I2C_I2C5,
	.res_num = I2C_RES_NUM,
 };

const domain_res_t g_gpio_res =
{
	.res_id[0] = RES_GPIO_GPIO5,
	.res_num = GPIO_RES_NUM,
 };

const domain_res_t g_spdif_res =
{
	.res_id[0] = RES_SPDIF_SPDIF4,
	.res_num = SPDIF_RES_NUM,
 };

const domain_res_t g_dc_res =
{
	.res_id[0] = RES_DC_DC2,
	.res_num = DC_RES_NUM,
 };

const domain_res_t g_dp_res =
{
	.res_id[0] = RES_DP_DP2,
	.res_num = DP_RES_NUM,
 };

const domain_res_t g_lvds_res =
{
	.res_id[0] = RES_LVDS_LVDS3,
	.res_num = LVDS_RES_NUM,
 };

const domain_res_t g_sys_cnt_res =
{
	.res_id[0] = RES_SYS_CNT_SYS_CNT_RO,
	.res_num = SYS_CNT_RES_NUM,
 };

const domain_res_t g_scr4k_ssid_res =
{
	.res_id[0] = RES_SCR4K_SSID_SCR4K_SSID,
	.res_num = SCR4K_SSID_RES_NUM,
 };


// os res define from Rpc sheet
const domain_res_t g_rstgen_res =
{
	.res_id[0] = RES_MODULE_RST_SEC_GPU2_SS,
	.res_id[1] = RES_MODULE_RST_SEC_GPU2_CORE,
	.res_id[2] = RES_MODULE_RST_SEC_I2S_SC8,
	.res_id[3] = RES_GENERAL_REG_SEC_GENERAL_REG8,
	.res_id[4] = RES_GENERAL_REG_SEC_GENERAL_REG7,
	.res_id[5] = RES_GENERAL_REG_SEC_GENERAL_REG6,
	.res_id[6] = RES_GENERAL_REG_SEC_GENERAL_REG5,
	.res_id[7] = RES_GENERAL_REG_SEC_GENERAL_REG4,
	.res_id[8] = RES_GENERAL_REG_SEC_GENERAL_REG3,
	.res_num = RSTGEN_RES_NUM,
 };

const domain_res_t g_ckgen_res =
{
	.res_id[0] = RES_GATING_EN_SEC_WDT5,
	.res_id[1] = RES_GATING_EN_SEC_GPIO4,
	.res_id[2] = RES_GATING_EN_SEC_GPIO3,
	.res_id[3] = RES_GATING_EN_SEC_I2S_SC8,
	.res_id[4] = RES_GATING_EN_SEC_TIMER8,
	.res_id[5] = RES_GATING_EN_SEC_SPDIF4,
	.res_id[6] = RES_GATING_EN_SEC_UART14,
	.res_id[7] = RES_GATING_EN_SEC_UART12,
	.res_id[8] = RES_GATING_EN_SEC_UART10,
	.res_id[9] = RES_GATING_EN_SEC_SPI8,
	.res_id[10] = RES_GATING_EN_SEC_I2C15,
	.res_id[11] = RES_GATING_EN_SEC_I2C14,
	.res_id[12] = RES_GATING_EN_SEC_I2C13,
	.res_id[13] = RES_GATING_EN_SEC_IRAM4,
	.res_id[14] = RES_GATING_EN_SEC_IRAM3,
	.res_id[15] = RES_GATING_EN_SEC_IRAM2,
	.res_id[16] = RES_GATING_EN_SEC_OSPI2,
	.res_id[17] = RES_GATING_EN_SEC_ENET2_TIMER_SEC,
	.res_id[18] = RES_GATING_EN_SEC_ENET2_TX,
	.res_id[19] = RES_GATING_EN_SEC_EMMC4,
	.res_id[20] = RES_GATING_EN_SEC_EMMC3,
	.res_id[21] = RES_GATING_EN_SEC_EMMC2,
	.res_id[22] = RES_GATING_EN_SEC_MSHC_TIMER,
	.res_id[23] = RES_GATING_EN_SEC_EMMC1,
	.res_id[24] = RES_IP_SLICE_SEC_I2S_SC8,
	.res_id[25] = RES_IP_SLICE_SEC_SPDIF4,
	.res_id[26] = RES_GATING_EN_DISP_MIPI_CSI3_PIX,
	.res_id[27] = RES_GATING_EN_DISP_DC5,
	.res_id[28] = RES_UUU_WRAP_SOC_GPU2,
	.res_id[29] = RES_GATING_EN_SOC_HIS_BUS_3_NOC_HIS_PERCLK,
	.res_id[30] = RES_GATING_EN_SOC_HIS_BUS_2_NOC_HIS_MAINCLK,
	.res_id[31] = RES_GATING_EN_SOC_GPU2_2,
	.res_id[32] = RES_GATING_EN_SOC_GPU2_1,
	.res_id[33] = RES_GATING_EN_SOC_GPU2_0,
	.res_id[34] = RES_GATING_EN_SOC_CPU2_PCLK_ATCLK_GICCLK,
	.res_id[35] = RES_GATING_EN_SOC_CPU2_1,
	.res_id[36] = RES_GATING_EN_SOC_CPU2_0,
	.res_id[37] = RES_GATING_EN_SOC_CPU1A_2_PLL_CPU1A_PLL_CPU1B_PCLK,
	.res_id[38] = RES_GATING_EN_SOC_GIC4,
	.res_id[39] = RES_GATING_EN_SOC_GIC4_GIC5,
	.res_id[40] = RES_GATING_EN_SOC_SCR4K_SSID,
	.res_id[41] = RES_GATING_EN_SOC_SCR4K_SID,
	.res_id[42] = RES_CORE_SLICE_SOC_GPU2,
	.res_id[43] = RES_IP_SLICE_SOC_VPU1,
	.res_num = CKGEN_RES_NUM,
 };

static const domain_res_t * g_res_cat[] = {
	NULL, /*iram*/
	NULL, /*mb*/
	NULL, /*gpu*/
	&g_ddr_res,
	NULL, /*pcie*/
	&g_gic_res,
	NULL, /*ospi*/
	NULL, /*romc*/
	NULL, /*platform*/
	&g_ce_mem_res,
	NULL, /*vdsp*/
	&g_dma_res,
	&g_mshc_res,
	NULL, /*mac*/
	NULL, /*gpv*/
	&g_spi_res,
	&g_ce_reg_res,
	NULL, /*sem*/
	&g_i2s_sc_res,
	NULL, /*bipc*/
	NULL, /*eic*/
	NULL, /*pvt_sens*/
	NULL, /*pwrctrl*/
	&g_pll_res,
	NULL, /*rc24m*/
	NULL, /*xtal*/
	NULL, /*romc_reg*/
	NULL, /*iram_reg*/
	&g_dma_mux_res,
	NULL, /*watchdog*/
	&g_pwm_res,
	&g_timer_res,
	NULL, /*enet_qos*/
	&g_uart_res,
	&g_i2c_res,
	NULL, /*can*/
	NULL, /*ospi_reg*/
	NULL, /*efuse*/
	&g_gpio_res,
	NULL, /*gpv_sec_m_reg*/
	NULL, /*i2s_mc*/
	&g_spdif_res,
	NULL, /*adc*/
	&g_dc_res,
	&g_dp_res,
	NULL, /*mipi_dsi*/
	NULL, /*disp_mux*/
	NULL, /*mipi_csi*/
	NULL, /*csi*/
	NULL, /*g2d*/
	&g_lvds_res,
	NULL, /*dma_ch_mux_pcie*/
	NULL, /*usbphy*/
	NULL, /*usb*/
	NULL, /*pcie_phy*/
	NULL, /*scr_hpi*/
	NULL, /*vpu*/
	NULL, /*mjpeg*/
	&g_sys_cnt_res,
	NULL, /*sec_storage*/
	NULL, /*rstgen_rtc*/
	NULL, /*tm*/
	NULL, /*iomuxc_rtc*/
	NULL, /*pmu*/
	NULL, /*rc_rtc*/
	NULL, /*rtc*/
	NULL, /*ftbu*/
	NULL, /*cssys*/
	&g_scr4k_ssid_res,
	NULL, /*scr4k_sid*/
	NULL, /*smmu_tcu*/
	NULL, /*ddr_cfg*/
	NULL, /*pcie_reg*/
	NULL, /*pcie_io*/
	NULL, /*iomuxc*/
	&g_rstgen_res,
	NULL, /*scr*/
	&g_ckgen_res,
};

#endif /* _OS_RES_H */
