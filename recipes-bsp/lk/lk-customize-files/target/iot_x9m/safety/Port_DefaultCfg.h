#ifndef PORT_DEAULTCFG_H
#define PORT_DEAULTCFG_H

#include "Port.h"

/*
 * Container : PortPinConfiguration
 */

/*
Configuration Options: Physical pin level
-LOW  (Low Volatage Level)
-HIGH (High Voltage Level)
*/

#define PORT_PIN_LEVEL_LOW    (0x00U)
#define PORT_PIN_LEVEL_HIGH   (0x01U)

/*
Configuration Options: Pin driver strength
*/

#define RFAST_PORT_PIN_STRONG_DRIVER_SHARP_EDGE  (0x0U)
#define RFAST_PORT_PIN_STRONG_DRIVER_MEDIUM_EDGE (0x1U)
#define RFAST_PORT_PIN_MEDIUM_DRIVER             (0x2U)
#define RFAST_PORT_PIN_RGMII_DRIVER              (0x3U)
#define RFAST_PORT_PIN_DEFAULT_DRIVER            (0x0U)

#define FAST_PORT_PIN_STRONG_DRIVER_SHARP_EDGE  (0x0U)
#define FAST_PORT_PIN_STRONG_DRIVER_MEDIUM_EDGE (0x1U)
#define FAST_PORT_PIN_MEDIUM_DRIVER             (0x2U)
#define FAST_PORT_PIN_DEFAULT_DRIVER            (0x0U)

#define SLOW_PORT_PIN_MEDIUM_DRIVER_SHARP_EDGE  (0x0U)
#define SLOW_PORT_PIN_MEDIUM_DRIVER             (0x1U)
#define SLOW_PORT_PIN_DEFAULT_DRIVER            (0x0U)

/* Pin driver strength value for the non available pins*/

#define  PORT_PIN_PAD_STRENGTH_DEFAULT      (0x0U)
#define  PORT_PIN_PAD_LEVEL_DEFAULT         (0x0U)
#define  PORT_PIN_PAD_DEFAULT               (0x0U)

/*
Configuration Options: Pin Pad Level
*/

#define PORT_INPUT_LEVEL_CMOS_AUTOMOTIVE  (0x0U)
#define PORT_INPUT_LEVEL_TTL_3_3V       (0xCU)
#define PORT_INPUT_LEVEL_TTL_5_0V       (0x8U)

#define PORT_GPIO_DUMMY			(0xEFFFFFFFU)
#define PORT_PIN_LEVEL_DUMMY    PORT_PIN_LEVEL_LOW
#define PORT_PIN_MODE_DUMMY    PORT_PIN_MODE_NOT_CHANGEABLE

static const Port_n_ConfigType Port_kConfiguration[] = {
    /*                              Port0                       */
    {
        {
            /* IO_PAD_CONFIG: Port Parametric output, input select, slew rate, driver select, pull enable configuration */

            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_PULL_UP ),/*Pin 0, OSPI1_SCLK*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_PULL_UP ),/*Pin 1, OSPI1_SS0*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_NO_PULL ),/*Pin 2, OSPI1_DATA0*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_NO_PULL ),/*Pin 3, OSPI1_DATA1*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_NO_PULL ),/*Pin 4, OSPI1_DATA2*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_NO_PULL ),/*Pin 5, OSPI1_DATA3*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_NO_PULL ),/*Pin 6, OSPI1_DATA4*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_NO_PULL ),/*Pin 7, OSPI1_DATA5*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_NO_PULL ),/*Pin 8, OSPI1_DATA6*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_NO_PULL ),/*Pin 9, OSPI1_DATA7*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_PULL_DOWN ),/*Pin 10, OSPI1_DQS*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__HIGH | PORT_PIN_IN_PULL_UP ),/*Pin 11, OSPI1_SS1*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 12, RGMII1_TXC*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 13, RGMII1_TXD0*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 14, RGMII1_TXD1*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 15, RGMII1_TXD2*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 16, RGMII1_TXD3*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 17, RGMII1_TX_CTL*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 18, RGMII1_RXC*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 19, RGMII1_RXD0*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*Pin 20, RGMII1_RXD1*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*Pin 21, RGMII1_RXD2*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*Pin 22, RGMII1_RXD3*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*Pin 23, RGMII1_RX_CTL*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*Pin 24, GPIO_A0*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*Pin 25, GPIO_A1*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*Pin 26, GPIO_A2*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*Pin 27, GPIO_A3*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*Pin 28, GPIO_A4*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*Pin 29, GPIO_A5*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 30, GPIO_A6*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 31, GPIO_A7*/


        },

        {
            /* PIN_MUX_CONFIG: Port pins mode, open drain configuration */

            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 0, OSPI1_SCLK*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 1*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 2*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 3*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 4*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 5*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 6*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 7*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 8*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 9*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 10*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 11*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 12, RGMII1_TXC*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 13*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 14*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 15*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 16*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 17*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 18*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 19*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 20*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 21*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 22*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 23*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 24, GPIO_A0*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 25*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 26*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT3),/*Pin 27*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 28*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT1),/*Pin 29*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),/*Pin 30*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 31*/
        },

        {
            /* GPIO id for each pin */
            ((uint32_t)PORT_GPIO_1 ),/*Pin 0, OSPI1_SCLK*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 1, OSPI1_SS0*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 2, OSPI1_DATA0*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 3, OSPI1_DATA1*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 4, OSPI1_DATA2*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 5, OSPI1_DATA3*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 6, OSPI1_DATA4*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 7, OSPI1_DATA5*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 8, OSPI1_DATA6*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 9, OSPI1_DATA7*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 10, OSPI1_DQS*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 11, OSPI1_SS1*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 12, RGMII1_TXC*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 13, RGMII1_TXD0*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 14, RGMII1_TXD1*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 15, RGMII1_TXD2*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 16, RGMII1_TXD3*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 17, RGMII1_TX_CTL*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 18, RGMII1_RXC*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 19, RGMII1_RXD0*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 20, RGMII1_RXD1*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 21, RGMII1_RXD2*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 22, RGMII1_RXD3*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 23, RGMII1_RX_CTL*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 24, GPIO_A0*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 25, GPIO_A1*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 26, GPIO_A2*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 27, GPIO_A3*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 28, GPIO_A4*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 29, GPIO_A5*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 30, GPIO_A6*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 31, GPIO_A7*/

        },

        {
            /* Port pins initial level configuration */

            PORT_PIN_LEVEL_LOW,/* Pin 0 */
            PORT_PIN_LEVEL_LOW,/* Pin 1 */
            PORT_PIN_LEVEL_LOW,/* Pin 2 */
            PORT_PIN_LEVEL_LOW,/* Pin 3 */
            PORT_PIN_LEVEL_LOW,/* Pin 4 */
            PORT_PIN_LEVEL_LOW,/* Pin 5 */
            PORT_PIN_LEVEL_LOW,/* Pin 6 */
            PORT_PIN_LEVEL_LOW,/* Pin 7 */
            PORT_PIN_LEVEL_LOW,/* Pin 8 */
            PORT_PIN_LEVEL_LOW,/* Pin 9 */
            PORT_PIN_LEVEL_LOW,/* Pin 10 */
            PORT_PIN_LEVEL_LOW,/* Pin 11 */
            PORT_PIN_LEVEL_LOW,/* Pin 12 */
            PORT_PIN_LEVEL_LOW,/* Pin 13 */
            PORT_PIN_LEVEL_LOW,/* Pin 14 */
            PORT_PIN_LEVEL_LOW,/* Pin 15 */
            PORT_PIN_LEVEL_LOW,/* Pin 16 */
            PORT_PIN_LEVEL_LOW,/* Pin 17 */
            PORT_PIN_LEVEL_LOW,/* Pin 18 */
            PORT_PIN_LEVEL_LOW,/* Pin 19 */
            PORT_PIN_LEVEL_LOW,/* Pin 20 */
            PORT_PIN_LEVEL_LOW,/* Pin 21 */
            PORT_PIN_LEVEL_LOW,/* Pin 22 */
            PORT_PIN_LEVEL_LOW,/* Pin 23 */
            PORT_PIN_LEVEL_LOW,/* Pin 24 */
            PORT_PIN_LEVEL_LOW,/* Pin 25 */
            PORT_PIN_LEVEL_LOW,/* Pin 26 */
            PORT_PIN_LEVEL_LOW,/* Pin 27 */
            PORT_PIN_LEVEL_LOW,/* Pin 28 */
            PORT_PIN_LEVEL_LOW,/* Pin 29 */
            PORT_PIN_LEVEL_LOW,/* Pin 30 */
            PORT_PIN_LEVEL_LOW,/* Pin 31 */

        },

        {/* Port pin run time mode changeable or not configuration */

            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 0 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 1 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 2 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 3 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 4 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 5 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 6 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 7 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 8 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 9 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 10 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 11 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 12 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 13 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 14 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 15 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 16 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 17 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 18 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 19 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 20 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 21 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 22 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 23 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 24 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 25 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 26 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 27 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 28 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 29 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 30 */
            PORT_PIN_MODE_NOT_CHANGEABLE  /* Pin 31 */

        },

    },  // end of PORT0

    /*                              Port1                       */
    {
        {
            /* IO_PAD_CONFIG: Port Parametric output, input select, slew rate, driver select, pull enable configuration */

            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 32, GPIO_A8*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*Pin 33, GPIO_A9*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 34, GPIO_A10*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*Pin 35, GPIO_A11*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 36, GPIO_B0*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 37, GPIO_B1*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 38, GPIO_B2*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 39, GPIO_B3*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*Pin 40, GPIO_B4*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 41, GPIO_B5*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 42, GPIO_B6*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN ),/*Pin 43, GPIO_B7*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*Pin 44, GPIO_B8*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*Pin 45, GPIO_B9*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_NO_PULL ),/*Pin 46, GPIO_B10*/
            ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__OUT | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_UP ),/*Pin 47, GPIO_B11*/
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),

        },

        {
            /* PIN_MUX_CONFIG: Port pins mode, open drain configuration */

            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 32*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 33*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 34*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_OPENDRAIN | PORT_PIN_MODE_GPIO),/*Pin 35*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 36, GPIO_B0*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 37*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 38*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 39*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_OPENDRAIN | PORT_PIN_MODE_GPIO),/*Pin 40*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT4),/*Pin 41*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT5),/*Pin 42*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT5),/*Pin 43*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 44*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 45*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 46*/
            ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_ALT2),/*Pin 47*/
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
        },

        {
            /* GPIO Controller id for each pin */
            ((uint32_t)PORT_GPIO_1 ),/*Pin 32, GPIO_A8*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 33, GPIO_A9*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 34, GPIO_A10*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 35, GPIO_A11*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 36, GPIO_B0*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 37, GPIO_B1*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 38, GPIO_B2*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 39, GPIO_B3*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 40, GPIO_B4*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 41, GPIO_B5*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 42, GPIO_B6*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 43, GPIO_B7*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 44, GPIO_B8*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 45, GPIO_B9*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 46, GPIO_B10*/
            ((uint32_t)PORT_GPIO_1 ),/*Pin 47, GPIO_B11*/
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),
            ((uint32_t)PORT_GPIO_DUMMY),

        },

        {
            /* Port pins initial level configuration */

            PORT_PIN_LEVEL_LOW,/* Pin 32 */
            PORT_PIN_LEVEL_LOW,/* Pin 33 */
            PORT_PIN_LEVEL_LOW,/* Pin 34 */
            PORT_PIN_LEVEL_LOW,/* Pin 35 */
            PORT_PIN_LEVEL_LOW,/* Pin 36 */
            PORT_PIN_LEVEL_LOW,/* Pin 37 */
            PORT_PIN_LEVEL_LOW,/* Pin 38 */
            PORT_PIN_LEVEL_LOW,/* Pin 39 */
            PORT_PIN_LEVEL_LOW,/* Pin 40 */
            PORT_PIN_LEVEL_LOW,/* Pin 41 */
            PORT_PIN_LEVEL_LOW,/* Pin 42 */
            PORT_PIN_LEVEL_LOW,/* Pin 43 */
            PORT_PIN_LEVEL_LOW,/* Pin 44 */
            PORT_PIN_LEVEL_LOW,/* Pin 45 */
            PORT_PIN_LEVEL_LOW,/* Pin 46 */
            PORT_PIN_LEVEL_LOW,/* Pin 47 */
            PORT_PIN_LEVEL_DUMMY,
            PORT_PIN_LEVEL_DUMMY,
            PORT_PIN_LEVEL_DUMMY,
            PORT_PIN_LEVEL_DUMMY,
            PORT_PIN_LEVEL_DUMMY,
            PORT_PIN_LEVEL_DUMMY,
            PORT_PIN_LEVEL_DUMMY,
            PORT_PIN_LEVEL_DUMMY,
            PORT_PIN_LEVEL_DUMMY,
            PORT_PIN_LEVEL_DUMMY,
            PORT_PIN_LEVEL_DUMMY,
            PORT_PIN_LEVEL_DUMMY,
            PORT_PIN_LEVEL_DUMMY,
            PORT_PIN_LEVEL_DUMMY,
            PORT_PIN_LEVEL_DUMMY,
            PORT_PIN_LEVEL_DUMMY,

        },

        {/* Port pin run time mode changeable or not configuration */

            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 32 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 33 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 34 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 35 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 36 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 37 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 38 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 39 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 40 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 41 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 42 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 43 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 44 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 45 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 46 */
            PORT_PIN_MODE_NOT_CHANGEABLE, /* Pin 47 */
            PORT_PIN_MODE_DUMMY,
            PORT_PIN_MODE_DUMMY,
            PORT_PIN_MODE_DUMMY,
            PORT_PIN_MODE_DUMMY,
            PORT_PIN_MODE_DUMMY,
            PORT_PIN_MODE_DUMMY,
            PORT_PIN_MODE_DUMMY,
            PORT_PIN_MODE_DUMMY,
            PORT_PIN_MODE_DUMMY,
            PORT_PIN_MODE_DUMMY,
            PORT_PIN_MODE_DUMMY,
            PORT_PIN_MODE_DUMMY,
            PORT_PIN_MODE_DUMMY,
            PORT_PIN_MODE_DUMMY,
            PORT_PIN_MODE_DUMMY,
            PORT_PIN_MODE_DUMMY

        },

    },  // end of PORT1

};


#endif  /* PORT_DEAULTCFG_H */
