/*
 * Copyright (C) Semidrive Semiconductor Ltd.
 * All rights reserved.
 */

#include "tca9539.h"
#include <touch_device.h>
#include "boardinfo_hwid_usr.h"

static struct ts_board_config tsc[] = {
#if SERDES_TP_X9H
#if !TOUCH_SERDES_DIVIDED
    {
        TS_ENABLE, "goodix", RES_I2C_I2C16, 0x5d, TS_SUPPORT_ANRDOID_MAIN,
        {1920, 720, 10, 0, 0, 0},
        {false, 12, 0x76, TCA9539_P01},
        {true, TI941_DUAL, 0x0c, 0x2c, 3, 2},
        {
            //reset-pin:0 not used yet
            0
        },
        {
            //irq-pin:144
            PortConf_PIN_EMMC2_CLK,
            {
                ((uint32_t)PORT_PAD_MMC_SP__MIN | PORT_PAD_MMC_SN__MIN | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MIN | PORT_PAD_MMC_TXPREN__MIN | PORT_PAD_MMC_PULL__DOWN),
                ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),
            }
        }
    },
    {
        TS_ENABLE, "goodix", RES_I2C_I2C16, 0x14, TS_SUPPORT_ANRDOID_AUX1,
        {1920, 720, 10, 0, 0, 0},
        {false, 12, 0x76, TCA9539_P02},
        {true, TI941_DUAL, 0x0d, 0x3c, 3, 2},
        {
            //reset-pin:0 not used yet
            0
        },
        {
            //irq-pin:145
            PortConf_PIN_EMMC2_CMD,
            {
                ((uint32_t)PORT_PAD_MMC_SP__MIN | PORT_PAD_MMC_SN__MIN | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MIN | PORT_PAD_MMC_TXPREN__MIN | PORT_PAD_MMC_PULL__UP),
                ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),
            }
        }
    },
#ifdef ENABLE_CONTROLPANEL
    {
        TS_ENABLE, "goodix", RES_I2C_I2C15, 0x5d, TS_SUPPORT_CTRLPANEL_MAIN,
        {1920, 720, 10, 0, 0, 0},
        {false, 12, 0x75, TCA9539_P07},
        {true, TI947_SINGLE, 0x1a, 0x2c, 3, 2},
        {
            //reset-pin:0 not used yet
            0
        },
        {
            //irq-pin:131
            PortConf_PIN_I2S_MC_SD7,
            {
                ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN),
                ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_OPENDRAIN | PORT_PIN_MODE_GPIO),
            }
        }
    },
    {
        TS_DISABLE, "goodix", RES_I2C_I2C14, 0x5d, TS_SUPPORT_CTRLPANEL_AUX1,
        {1920, 720, 10, 0, 0, 0},
        {false, 12, 0x75, TCA9539_P06},
        {false, TI947_SINGLE, 0x0, 0x0, 0, 0},
        {
            //reset-pin:0 not used yet
            0
        },
        {
            //irq-pin:130
            PortConf_PIN_I2S_MC_SD6,
            {
                ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN),
                ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_OPENDRAIN | PORT_PIN_MODE_GPIO),
            }
        }
    },
#endif
#else
    {
        TS_ENABLE, "goodix", RES_I2C_I2C16, 0x5d, TS_SUPPORT_ANRDOID_MAIN | TS_SUPPORT_CTRLPANEL_MAIN,
        {1920, 720, 10, 0, 0, 0},
        {false, 12, 0x76, TCA9539_P01},
        {true, TI941_SINGLE, 0x0c, 0x2c, 3, 2},
        {
            //reset-pin:0 not used yet
            0
        },
        {
            //irq-pin:144
            PortConf_PIN_EMMC2_CLK,
            {
                ((uint32_t)PORT_PAD_MMC_SP__MIN | PORT_PAD_MMC_SN__MIN | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MIN | PORT_PAD_MMC_TXPREN__MIN | PORT_PAD_MMC_PULL__DOWN),
                ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),
            }
        }
    },
#endif
#elif SERDES_TP_X9M
    {
        TS_ENABLE, "goodix", RES_I2C_I2C16, 0x5d, TS_SUPPORT_ANRDOID_MAIN,
        {1920, 720, 10, 0, 0, 0},
        {false, 12, 0x76, TCA9539_P01},
        {true, TI941_SINGLE, 0x0c, 0x2c, 3, 2},
        {
            //reset-pin:0 not used yet
            0
        },
        {
            //irq-pin:144
            PortConf_PIN_EMMC2_CLK,
            {
                ((uint32_t)PORT_PAD_MMC_SP__MIN | PORT_PAD_MMC_SN__MIN | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MIN | PORT_PAD_MMC_TXPREN__MIN | PORT_PAD_MMC_PULL__DOWN),
                ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),
            }
        }
    },
#ifdef ENABLE_CONTROLPANEL
    {
        TS_ENABLE, "goodix", RES_I2C_I2C15, 0x5d, TS_SUPPORT_CTRLPANEL_MAIN,
        {1920, 720, 10, 0, 0, 0},
        {false, 12, 0x75, TCA9539_P07},
        {true, TI947_SINGLE, 0x1a, 0x2c, 3, 2},
        {
            //reset-pin:0 not used yet
            0
        },
        {
            //irq-pin:131
            PortConf_PIN_I2S_MC_SD7,
            {
                ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN),
                ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_OPENDRAIN | PORT_PIN_MODE_GPIO),
            }
        }
    },
#endif
#endif
};

static struct ts_board_config tsc_core[] = {
#if SERDES_TP_X9H
#if !TOUCH_SERDES_DIVIDED
    {
        TS_ENABLE, "goodix", RES_I2C_I2C16, 0x5d, TS_SUPPORT_ANRDOID_MAIN,
        {1920, 720, 10, 0, 0, 0},
        {false, 12, 0x76, TCA9539_P01},
        {true, TI941_DUAL, 0x0c, 0x2c, 3, 2},
        {
            //reset-pin:0 not used yet
            0
        },
        {
            //irq-pin:144
            PortConf_PIN_EMMC2_CLK,
            {
                ((uint32_t)PORT_PAD_MMC_SP__MIN | PORT_PAD_MMC_SN__MIN | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MIN | PORT_PAD_MMC_TXPREN__MIN | PORT_PAD_MMC_PULL__DOWN),
                ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),
            }
        }
    },
    {
        TS_ENABLE, "goodix", RES_I2C_I2C16, 0x14, TS_SUPPORT_ANRDOID_AUX1,
        {1920, 720, 10, 0, 0, 0},
        {false, 12, 0x76, TCA9539_P02},
        {true, TI941_DUAL, 0x0d, 0x3c, 3, 2},
        {
            //reset-pin:0 not used yet
            0
        },
        {
            //irq-pin:145
            PortConf_PIN_EMMC2_CMD,
            {
                ((uint32_t)PORT_PAD_MMC_SP__MIN | PORT_PAD_MMC_SN__MIN | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MIN | PORT_PAD_MMC_TXPREN__MIN | PORT_PAD_MMC_PULL__UP),
                ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),
            }
        }
    },
#ifdef ENABLE_CONTROLPANEL
    {
        TS_ENABLE, "goodix", RES_I2C_I2C15, 0x5d, TS_SUPPORT_CTRLPANEL_MAIN,
        {1920, 720, 10, 0, 0, 0},
        {false, 12, 0x75, TCA9539_P07},
        {true, TI947_SINGLE, 0x1a, 0x2c, 3, 2},
        {
            //reset-pin:0 not used yet
            0
        },
        {
            //irq-pin:130
            PortConf_PIN_I2S_MC_SD6,
            {
                ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN),
                ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_OPENDRAIN | PORT_PIN_MODE_GPIO),
            }
        }
    },
    {
        TS_DISABLE, "goodix", RES_I2C_I2C14, 0x5d, TS_SUPPORT_CTRLPANEL_AUX1,
        {1920, 720, 10, 0, 0, 0},
        {false, 12, 0x75, TCA9539_P06},
        {false, TI947_SINGLE, 0x0, 0x0, 0, 0},
        {
            //reset-pin:0 not used yet
            0
        },
        {
            //irq-pin:130
            PortConf_PIN_I2S_MC_SD6,
            {
                ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN),
                ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_OPENDRAIN | PORT_PIN_MODE_GPIO),
            }
        }
    },
#endif
#else
    {
        TS_ENABLE, "goodix", RES_I2C_I2C16, 0x5d, TS_SUPPORT_ANRDOID_MAIN | TS_SUPPORT_CTRLPANEL_MAIN,
        {1920, 720, 10, 0, 0, 0},
        {false, 12, 0x76, TCA9539_P01},
        {true, TI941_SINGLE, 0x0c, 0x2c, 3, 2},
        {
            //reset-pin:0 not used yet
            0
        },
        {
            //irq-pin:144
            PortConf_PIN_EMMC2_CLK,
            {
                ((uint32_t)PORT_PAD_MMC_SP__MIN | PORT_PAD_MMC_SN__MIN | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MIN | PORT_PAD_MMC_TXPREN__MIN | PORT_PAD_MMC_PULL__DOWN),
                ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),
            }
        }
    },
#endif
#elif SERDES_TP_X9M
    {
        TS_ENABLE, "goodix", RES_I2C_I2C16, 0x5d, TS_SUPPORT_ANRDOID_MAIN,
        {1920, 720, 10, 0, 0, 0},
        {false, 12, 0x76, TCA9539_P01},
        {true, TI941_SINGLE, 0x0c, 0x2c, 3, 2},
        {
            //reset-pin:0 not used yet
            0
        },
        {
            //irq-pin:144
            PortConf_PIN_EMMC2_CLK,
            {
                ((uint32_t)PORT_PAD_MMC_SP__MIN | PORT_PAD_MMC_SN__MIN | PORT_PAD_MMC_RXSEL__IN | PORT_PAD_MMC_TXPREP__MIN | PORT_PAD_MMC_TXPREN__MIN | PORT_PAD_MMC_PULL__DOWN),
                ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_PUSHPULL | PORT_PIN_MODE_GPIO),
            }
        }
    },
#ifdef ENABLE_CONTROLPANEL
    {
        TS_ENABLE, "goodix", RES_I2C_I2C15, 0x5d, TS_SUPPORT_CTRLPANEL_MAIN,
        {1920, 720, 10, 0, 0, 0},
        {false, 12, 0x75, TCA9539_P07},
        {true, TI947_SINGLE, 0x1a, 0x2c, 3, 2},
        {
            //reset-pin:0 not used yet
            0
        },
        {
            //irq-pin:130
            PortConf_PIN_I2S_MC_SD6,
            {
                ((uint32_t)PORT_PAD_POE__DISABLE | PORT_PAD_IS__IN | PORT_PAD_SR__FAST | PORT_PAD_DS__MID1 | PORT_PIN_IN_PULL_DOWN),
                ((uint32_t)PORT_PIN_MUX_FV__MIN | PORT_PIN_MUX_FIN__MIN | PORT_PIN_OUT_OPENDRAIN | PORT_PIN_MODE_GPIO),
            }
        }
    },
#endif
#endif
};

void get_touch_device(struct ts_board_config **_tsc, int *_tsc_num)
{
    static bool achieved = false;
    static bool core_board = false;

    if (achieved) {
        if (core_board) {
            *_tsc = tsc_core;
            *_tsc_num = sizeof(tsc_core) / sizeof(tsc_core[0]);
        }
        else {
            *_tsc = tsc;
            *_tsc_num = sizeof(tsc) / sizeof(tsc[0]);
        }
    }
    else {
        if (get_part_id(PART_BOARD_TYPE) == BOARD_TYPE_MS) {
            *_tsc = tsc_core;
            *_tsc_num = sizeof(tsc_core) / sizeof(tsc_core[0]);
            core_board = true;
        }
        else {
            *_tsc = tsc;
            *_tsc_num = sizeof(tsc) / sizeof(tsc[0]);
        }

        achieved = true;
    }
}

