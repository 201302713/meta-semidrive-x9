##########################
#preload build
##########################
LOCAL_DIR := $(GET_LOCAL_DIR)
CHIPVERSION := x9_mid
DOMAIN := ecockpit
TARGET := yuenki_x9m
PROJECT := yuenki

DEBUG :=0

MEMBASE ?= $(AP1_PRELOADER_MEMBASE)
MEMSIZE ?= $(AP1_PRELOADER_MEMSIZE)

##########################
#driver modules config
##########################
SUPPORT_TIMER_SDDRV := true
SUPPORT_RSTGEN_SDDRV := true
SUPPORT_UART_DWDRV := true
SUPPORT_PLL_SDDRV := true
SUPPORT_SCR_SDDRV := true
SUPPORT_CLKGEN_SDDRV := true
SUPPORT_MMC_SDDRV := true
SYSTEM_TIMER ?= sdrv_timer
SUPPORT_PORT_SDDRV := true
SUPPORT_DIO_SDDRV := true
SUPPORT_FUSE_CTRL :=true

MODULES += application/system/preloader

include $(LOCAL_DIR)/../project/common/preloader.mk
