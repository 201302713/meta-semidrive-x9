##########################
#dloader build
##########################
LOCAL_DIR := $(GET_LOCAL_DIR)
CHIPVERSION := x9_mid
DOMAIN := secure
TARGET := iot_x9m
PROJECT := iot

DEBUG :=0

##########################
#driver modules config
##########################
SUPPORT_SCR_SDDRV := true
SUPPORT_WDG_SDDRV := true
SYSTEM_TIMER ?= sdrv_timer
SUPPORT_TIMER_SDDRV := true
SUPPORT_UART_DWDRV := true
SUPPORT_PLL_SDDRV := true
SUPPORT_DMA_SDDRV := false
SUPPORT_PORT_SDDRV := true
SUPPORT_USB_SDDRV := true
SUPPORT_SPINOR_SDDRV := true
SUPPORT_MMC_SDDRV := true
SUPPORT_RSTGEN_SDDRV := true
SUPPORT_CLKGEN_SDDRV := true
SUPPORT_FUSE_CTRL := true

#for is25x nor flash, rom needs to reset ospi before reading it
NORFLASH_DEVICE_TYPE := is25x
TOGGLE_OSPI_RESET_ENABLE := true

include $(LOCAL_DIR)/../project/common/dloader.mk
