##########################
#ivi bootloader build
##########################
LOCAL_DIR := $(GET_LOCAL_DIR)
CHIPVERSION := x9_mid
DOMAIN := ecockpit
TARGET := yuenki_x9m
PROJECT := yuenki

DEBUG :=0

DDR_SIZE ?= 2G
MEMBASE ?= $(AP1_BOOTLOADER_MEMBASE)
MEMSIZE ?= $(AP1_BOOTLOADER_MEMSIZE)

##########################
#driver modules config
##########################
SUPPORT_UART_DWDRV := true
SUPPORT_TIMER_SDDRV := true
SUPPORT_WDG_SDDRV := true
SUPPORT_RSTGEN_SDDRV := true
SYSTEM_TIMER ?= sdrv_timer
SUPPORT_MBOX_SDDRV := false
SUPPORT_CLKGEN_SDDRV :=true
SUPPORT_SCR_SDDRV := true
SUPPORT_PORT_SDDRV := true
SUPPORT_DIO_SDDRV := true
SUPPORT_MMC_SDDRV := true
SUPPORT_FUSE_CTRL :=true

include $(LOCAL_DIR)/../project/common/bootloader.mk
