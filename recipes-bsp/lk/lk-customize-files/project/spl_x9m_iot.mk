##########################
# spl build
##########################
LOCAL_DIR := $(GET_LOCAL_DIR)
CHIPVERSION := x9_mid
DOMAIN := secure
TARGET := iot_x9m
PROJECT := iot

DEBUG := 0

##########################
#driver modules config
##########################
SUPPORT_I2C_SDDRV := true
SUPPORT_SCR_SDDRV := true
SUPPORT_WDG_SDDRV := true
SYSTEM_TIMER ?= sdrv_timer
SUPPORT_MBOX_SDDRV := true
SUPPORT_TIMER_SDDRV := true
SUPPORT_UART_DWDRV := true
SUPPORT_PORT_SDDRV := true
SUPPORT_DIO_SDDRV := true
SUPPORT_USB_SDDRV := true
SUPPORT_MMC_SDDRV := true
SUPPORT_CLKGEN_SDDRV := true
SUPPORT_MODULE_HELPER_SDDRV :=true
NEED_CHANGE_VOLTAGE := true
SUPPORT_FUSE_CTRL :=true
DDR_FREQ := 3200
DDR_SIZE := 2G

include $(LOCAL_DIR)/../project/common/spl.mk
