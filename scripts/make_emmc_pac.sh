#!/bin/bash

usage()
{
    echo -e "\nUsage: make_emmc_pac.sh
    Optional parameters: [-y yocto-top] [-b yocto-build-name] [-h] [-m yocto machine]"
echo "
    * [-y yocto-top-path]:   Yocto top directory, default:$YOCTO_TOP
    * [-b yocto-build]:      Main build dir, default env:$YOCTO_BUILD_DIR
    * [-d 2rd-build]:        Second build absolut path, default:$SECOND_BUILD_DIR
    * [-m yocto-machine]:    Yocto machine [x9h_evb|gx9_evb|x9h_ref], default:$YOCTO_MACHINE
    * [-s signed-spl]:       Signed spl file, default:`pwd`/$SIGNED_SPL
    * [-p package]:          Global Package [x9hplus_evb], default:$PACKAGE
    * [-e]:                  All images in eMMC,use the bpt file with suffix '_emmc_only'
    * [-h]:                  This help message
"
}

clean_up()
{
    unset make_pac_help make_pac_error make_pac_flag
    unset usage clean_up
}

# Now, there is a dtbo partition in every global.bpt,
# but there is no dtbo image in some machine, so using a fake dtbo.
# If there is a real dtbo image in some machine in the future,
# it needs to remove the call for creating fake dtbo image and add the real dtbo image
# to the ${VBMTEA_DESCIPTOR} and ${SPECIFIED_PARTITION_PACK}
function make_fake_dtbo_for_vbmeta()
{
    DTBO_PATH=${YOCTO_IMAGE_DIR}/dtbo_fake.img
	echo "make_fake_dtbo_for_vbmeta use $DTBO_PATH"
    dd if=/dev/zero of=${DTBO_PATH} bs=1024 count=4
    VBMTEA_DESCIPTOR+=" ${DTBO_PATH}:dtbo:${HASH_ALG}:hash"
    SPECIFIED_PARTITION_PACK+=" --image dtbo_a:${DTBO_PATH} "
}

source ./sign_tool/sign_helper.sh
export PATH=.:${PWD}/sign_tool/:${PWD}/sign_tool/fec:${PATH}
export LD_LIBRARY_PATH=.:${PWD}/sign_tool/fec:${LD_LIBRARY_PATH}

# get command line options
OLD_OPTIND=$OPTIND

YOCTO_TOP=`realpath ../..`
YOCTO_BUILD_DIR=${BUILDDIR}
KERNEL_PREFIX=x9_evb
PRODUCT=x9
YOCTO_ROOTFS_IMAGE=core-image-base
SECOND_MACHINE=""
SECOND_KERNEL_DTS=""
SECOND_BUILD_DIR=""

GLOBAL_BPT_SUFFIX=
UNIFIED_BOOT_EXTRA=
while getopts "y:s:m:b:d:p:eh" make_pac_flag
do
    case $make_pac_flag in
        y) YOCTO_TOP="$OPTARG";
           ;;
        b) YOCTO_BUILD_DIR="${YOCTO_TOP}/$OPTARG";
           ;;
        d) SECOND_BUILD_DIR="$OPTARG";
           ;;
        m) YOCTO_MACHINE="$OPTARG";
           PACKAGE=${YOCTO_MACHINE}
           ;;
        s) SIGNED_SPL="$OPTARG";
           ;;
        p) PACKAGE="$OPTARG";
           ;;
        e) GLOBAL_BPT_SUFFIX="_emmc_only";
           ;;
        h) make_pac_help='true';
           ;;
        \?) make_pac_error='true';
           ;;
    esac
done

shift $((OPTIND-1))
if [ $# -ne 0 ]; then
    echo -e "Invalid command line ending: '$@'"
fi
OPTIND=$OLD_OPTIND
if test $make_pac_help; then
    usage && clean_up && exit 1
elif test $make_pac_error; then
    clean_up && exit 1
fi

# Package may contain one or more machines, if not defined, try the default machine
if [ "x$PACKAGE" == "x" ]; then
    PACKAGE=$YOCTO_MACHINE
fi

if [ "g9x_ref" == $PACKAGE ]; then
    KERNEL_PREFIX=g9x_ref
    PRODUCT=g9
fi

if [ "g9x_k1" == $PACKAGE ]; then
    KERNEL_PREFIX=g9x_ref
    PRODUCT=g9
fi

if [ "g9x_ii4" == $PACKAGE ]; then
    KERNEL_PREFIX=g9x_ii4_ref
    PRODUCT=g9
fi

if [ "g9q_ref" == $PACKAGE ]; then
    KERNEL_PREFIX=g9q_ref
    PRODUCT=g9
fi

if [ "x9h_ref" == $PACKAGE -o "x9h_ref_cluster" == $PACKAGE \
        -o "x9h_ref_controlpanel" == $PACKAGE \
        -o "x9h_ref_cluster_serdes" == $PACKAGE  \
        -o "x9h_ref_bt" == $PACKAGE ]; then
    KERNEL_PREFIX=x9_high_ref
    PRODUCT=x9
fi

if [ "x9h_ref_serdes" == $PACKAGE ]; then
    KERNEL_PREFIX=x9_high_ref_native_ivi_serdes_nobt
    PRODUCT=x9
fi

if [ "x9h_refa04_serdes" == $PACKAGE ]; then
    KERNEL_PREFIX=x9_high_refa04_native_ivi_serdes_nobt
    PRODUCT=x9
fi

if [ "x9h_ref_cluster_serdes" == $PACKAGE ]; then
    KERNEL_PREFIX=x9_high_ref_native_cluster_serdes
    PRODUCT=x9
fi

if [ "x9m_ref_serdes" == $PACKAGE ]; then
    KERNEL_PREFIX=x9_mid_ref_native_ivi_serdes
    PRODUCT=x9
fi

if [ "x9m_refa04_serdes" == $PACKAGE ]; then
    KERNEL_PREFIX=x9_mid_refa04_native_ivi_serdes
    PRODUCT=x9
fi

if [ "x9m_ref" == $PACKAGE ]; then
    KERNEL_PREFIX=x9_mid_ref_native_ivi
    PRODUCT=x9
fi

if [ "x9m_yuenki" == $PACKAGE -o "x9m_iot" == $PACKAGE -o "x9m_iot_ubuntu12g" == $PACKAGE -o "x9m_iot_ubuntu6g" == $PACKAGE ]; then
    KERNEL_PREFIX=x9_mid_ref_native_ivi
    PRODUCT=x9
fi

if [ "d9_ref" == $PACKAGE ]; then
    KERNEL_PREFIX=d9_ref
    PRODUCT=d9
fi

if [ "x9h_ref_controlpanel_serdes" == $PACKAGE ]; then
    KERNEL_PREFIX=x9_high_ref_native_controlpanel
    PRODUCT=x9
fi

if [ "v9f_ref" == $PACKAGE ]; then
    KERNEL_PREFIX=v9f_ref
    PRODUCT=v9
fi

if [ "v9f_ref_7inch" == $PACKAGE ]; then
    KERNEL_PREFIX=v9f_ref_7inch
    PRODUCT=v9
fi


if [ "v9ts_ref_a" == $PACKAGE ]; then
    KERNEL_PREFIX=v9ts_ref_a
    PRODUCT=v9
fi

if [ "v9ts_ref_b" == $PACKAGE ]; then
    KERNEL_PREFIX=v9ts_ref_b
    PRODUCT=v9
fi

if [ "x9hplus_evb" == $PACKAGE ]; then
    KERNEL_PREFIX=x9_high-plus_evb_native_ivi
    PRODUCT=x9
    YOCTO_MACHINE=x9h-plus_evb_ivi
    SECOND_MACHINE=x9h-plus_evb_cluster
    SECOND_KERNEL_DTS=x9_high-plus_evb_native_cluster

    SECOND_IMAGE_DIR=${SECOND_BUILD_DIR}/tmp/deploy/images/${SECOND_MACHINE}
    SECOND_IMAGE=${SECOND_IMAGE_DIR}/Image
    SECOND_DTB=${SECOND_IMAGE_DIR}/${SECOND_KERNEL_DTS}.dtb
    SECOND_ROOTFS=${SECOND_IMAGE_DIR}/${YOCTO_ROOTFS_IMAGE}-${SECOND_MACHINE}.cpio.gz
fi

if [ "x9h_icl02" == $PACKAGE ]; then
    KERNEL_PREFIX=x9_high_icl02_native_ivi_nobt
    PRODUCT=x9
fi

if [ "x9h_ms" == $PACKAGE ]; then
    KERNEL_PREFIX=x9_high_ms_native_linux_serdes_nobt
    PRODUCT=x9
fi

if [ "x9h_classic" == $PACKAGE ]; then
    KERNEL_PREFIX=x9_high_ref_native_linux_serdes_classic
    PRODUCT=x9
fi

if [ "x9h_iot" == $PACKAGE -o "x9h_iot_ubuntu12g" == $PACKAGE -o "x9h_iot_ubuntu6g" == $PACKAGE ]; then
    KERNEL_PREFIX=x9_high_ref
    PRODUCT=x9
fi

if [ "x9m_ms" == $PACKAGE ]; then
    KERNEL_PREFIX=x9_mid_ms_native_ivi_serdes_avm
    PRODUCT=x9
fi

if [ "x9hp_ms_cluster" == $PACKAGE ]; then
    KERNEL_PREFIX=x9_high-plus_ms_native_cluster_8g
    PRODUCT=x9
fi

if [ "x9e_ms" == $PACKAGE ]; then
    KERNEL_PREFIX=x9e_ms_native_ivi_avm_linux
    PRODUCT=x9
fi

if [ "x9e_ref" == $PACKAGE ]; then
    KERNEL_PREFIX=x9e_ref_native_linux_avm
    PRODUCT=x9
fi

if [ "v9t_a_ref" == $PACKAGE ]; then
    KERNEL_PREFIX=v9t_a_ref
    PRODUCT=v9
fi

if [ "v9t_b_ref" == $PACKAGE ]; then
    KERNEL_PREFIX=v9t_b_ref
    PRODUCT=v9
fi

ATB_SIGN_KEY=./sign_tool/vbmeta/keys/root-key.pem
SIGNED_SPL=${PACKAGE}/prebuilts/spl.bin.signed.rsa1024
SIGNED_HANDOVER=${PACKAGE}/prebuilts/ospihandover.bin.safe.signed
ATF_FILE=${PACKAGE}/prebuilts/sml.bin

echo -e " TOP directory  : " $YOCTO_TOP
echo -e " Build directory: " $YOCTO_BUILD_DIR $SECOND_BUILD_DIR
echo -e " Build Package  : " $PACKAGE
echo -e " Build machine  : " $YOCTO_MACHINE $SECOND_MACHINE
echo -e " Linux dts      : " $KERNEL_PREFIX $SECOND_KERNEL_DTS
echo -e " Signed spl path: "`pwd`/$SIGNED_SPL
echo -e " ATF binary path: "`pwd`/$ATF_FILE

## check the necessary files
YOCTO_IMAGE_DIR=${YOCTO_BUILD_DIR}/tmp/deploy/images/${YOCTO_MACHINE}
if [ ! -e $YOCTO_IMAGE_DIR ]; then
    echo -e "\n ERROR - No yocto Image found in the path" $YOCTO_IMAGE_DIR
    echo -e "\n use 'bitbake virtual/kernel -C compile' command to build"
    exit 1
fi

if [ ! -e $YOCTO_IMAGE_DIR/ssystem.bin ]; then
    echo -e "\n ERROR - No bootloaders found in the path" $YOCTO_IMAGE_DIR
    echo -e "\n try 'bitbake lk' command to build"
    exit 1
fi
if [ ! -e $YOCTO_IMAGE_DIR/preloader.bin ]; then
    echo -e "\n ERROR - No preloader found in the path" $YOCTO_IMAGE_DIR
    echo -e "\n try 'bitbake lk' command to build"
    exit 1
fi

## when use ext4 real rootfs, the ramdisk is not used. for compatible only, set a fake file
YOCTO_ROOTFS=${YOCTO_IMAGE_DIR}/${YOCTO_ROOTFS_IMAGE}-${YOCTO_MACHINE}.manifest
## YOCTO_ROOTFS=${YOCTO_IMAGE_DIR}/${YOCTO_ROOTFS_IMAGE}-${YOCTO_MACHINE}.cpio.gz
YOCTO_ROOTFS_EXT4=${YOCTO_IMAGE_DIR}/${YOCTO_ROOTFS_IMAGE}-${YOCTO_MACHINE}.ext4

if [ "x9h_iot_ubuntu12g" == $PACKAGE -o "x9m_iot_ubuntu12g" == $PACKAGE ]; then
	echo -e "\n use ${YOCTO_IMAGE_DIR}/ubuntu18-rootfs-12g.ext4"
    YOCTO_ROOTFS_EXT4=${YOCTO_IMAGE_DIR}/ubuntu18-rootfs-12g.ext4
	GLOBAL_BPT_SUFFIX="_emmc_only_ubuntu_16g";
fi
if [ "x9h_iot_ubuntu6g" == $PACKAGE -o "x9m_iot_ubuntu6g" == $PACKAGE ]; then
	echo -e "\n use ${YOCTO_IMAGE_DIR}/ubuntu18-rootfs-6g.ext4"
    YOCTO_ROOTFS_EXT4=${YOCTO_IMAGE_DIR}/ubuntu18-rootfs-6g.ext4
	GLOBAL_BPT_SUFFIX="_emmc_only_ubuntu_8g";
fi
if [ ! -e $YOCTO_ROOTFS_EXT4 ]; then
    echo -e "\n No ${YOCTO_ROOTFS_IMAGE} rootfs not found in the path" $YOCTO_ROOTFS_EXT4
    echo -e "Try the minimal rootfs"
    YOCTO_ROOTFS_EXT4=`find ${YOCTO_IMAGE_DIR} -name *${YOCTO_MACHINE}.ext4`
    YOCTO_ROOTFS=`find ${YOCTO_IMAGE_DIR} -name *${YOCTO_MACHINE}.manifest`
    if [ ! -e $YOCTO_ROOTFS_EXT4 ]; then
        echo -e "\n ERROR - No rootfs not found in the path" $YOCTO_ROOTFS_EXT4
        echo -e "\n Please use bitbake core-image-base or bitbake core-image-minimal to make a rootfs"
        echo -e "\n"
        exit 1
    fi
fi

YOCTO_IMAGE=${YOCTO_IMAGE_DIR}/Image
if [ ! -e $YOCTO_IMAGE ]; then
    echo -e "\n ERROR - No kernel image found in the path" $YOCTO_IMAGE
    echo -e "\n"
    exit 1
fi

YOCTO_DTB=${YOCTO_IMAGE_DIR}/${KERNEL_PREFIX}.dtb
if [ ! -e $YOCTO_DTB ]; then
    echo -e "\n ${KERNEL_PREFIX}.dtb not found in the path" ${YOCTO_IMAGE_DIR}
    YOCTO_DTB=`find ${YOCTO_IMAGE_DIR} -name ${PRODUCT}_*.dtb | head -n 1`
    echo -e "Try variants: " $YOCTO_DTB
    if [ ! -e $YOCTO_DTB ]; then
        echo -e "\n ERROR - No dtb found in the path" $YOCTO_DTB
        exit 1
    fi
fi

if [ -e sign_tool/run_sign_sec ]; then
    ./sign_tool/run_sign_sec -i ${YOCTO_IMAGE_DIR}/spl.bin -k ${ATB_SIGN_KEY}
    SIGNED_SPL=${YOCTO_IMAGE_DIR}/spl.bin.sec.signed
    SIGNED_HANDOVER=${YOCTO_IMAGE_DIR}/ospihandover.bin.signed
else
    SIGNED_SPL=${YOCTO_MACHINE}/prebuilts/spl.bin.signed.rsa1024
    SIGNED_HANDOVER=${YOCTO_MACHINE}/prebuilts/ospihandover.bin.safe.signed
fi
echo -e "Use Signed SPL image" $SIGNED_SPL

DA_COMMON_ARGS=" --product ${PRODUCT} --da FDA:$SIGNED_SPL --da OPSIDA:$SIGNED_HANDOVER --da DLOADER:${YOCTO_IMAGE_DIR}/dloader.bin "
SPECIFIED_PARTITION_PACK+=" --allow_empty_partitions "

BPT_ORIG_FILE=
BPT_OUT_FILE=
BPT_OUT_IMAGE=
HASH_ALG=sha256
KEY_FILE="./sign_tool/vbmeta/keys/vbmeta-user-key.pem"
KEY_META="./sign_tool/vbmeta/keys/vbmeta-user-rsa2048whitsha256-romtest.cer.chain"
VBMETA_SIGNED_ALG=SHA256_RSA2048
VBMETA_SIGNED_ARGS="--algorithm ${VBMETA_SIGNED_ALG} --key ${KEY_FILE} --public_key_meta ${KEY_META}"
VBMTEA_DESCIPTOR=

#atf file will be add footer, so copy it to temp dir
cp -f ${PACKAGE}/prebuilts/sml.bin  ${YOCTO_IMAGE_DIR}/sml.bin
ATF_FILE=${YOCTO_IMAGE_DIR}/sml.bin

rm out/*.bpt -rf
mkdir out -p
GLOBAL_OUT=$YOCTO_BUILD_DIR/global.pac

SYSTEM_CONFIG_BIN=
if [ "x9h_evb" == $PACKAGE -o "x9h_evb_cluster" == $PACKAGE  ]; then
    BPT_ORIG_FILE=$YOCTO_MACHINE/global$GLOBAL_BPT_SUFFIX.bpt
    BPT_OUT_FILE=out/GPT_global_output.bpt
    BPT_OUT_IMAGE=out/GPT_global_partition.img
    SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/x9_high/projects/default/system_config.bin
    echo "making a ext4 spare data partition"
    dd if=/dev/zero of=out/data.ext4 bs=1024 count=131072
    mkfs.ext4 out/data.ext4

    make_fake_dtbo_for_vbmeta

    SPECIFIED_PARTITION_PACK+=" --image dtb_a:${YOCTO_DTB}
                                --image kernel_a:${YOCTO_IMAGE}
                                --image rootfs_a:${YOCTO_ROOTFS_EXT4}
                                --image userdata:out/data.ext4
                                --image atf_a:${ATF_FILE}
                                --image bootloader_a:${YOCTO_IMAGE_DIR}/ivi_bootloader.bin
                                --version 2020W02 "

    VBMTEA_DESCIPTOR+=" ${YOCTO_DTB}:dtb:${HASH_ALG}:hash
                        ${YOCTO_IMAGE}:kernel:${HASH_ALG}:hash
                        ${ATF_FILE}:atf:${HASH_ALG}:hash
                        ${YOCTO_IMAGE_DIR}/ivi_bootloader.bin:bootloader:${HASH_ALG}:hash
                        ${YOCTO_ROOTFS_EXT4}:rootfs:${HASH_ALG}:hashtree"
fi

if [ "x9hplus_evb" == $PACKAGE ]; then
    echo -e "\n>>> Generating bpt files"
    BPT_ORIG_FILE=$PACKAGE/global$GLOBAL_BPT_SUFFIX.bpt
    BPT_OUT_FILE=out/GPT_global_output.bpt
    BPT_OUT_IMAGE=out/GPT_global_partition.img
    SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/x9_high/projects/default/system_config.bin

    echo -e "\n>>> Making a ext4 spare data partition"
    dd if=/dev/zero of=out/data.ext4 bs=1024 count=131072
    mkfs.ext4 out/data.ext4

    if [ ! -e $SECOND_DTB ]; then
        echo -e "\n ${SECOND_DTB} not found in the path" ${SECOND_IMAGE_DIR}
        SECOND_DTB=`find ${SECOND_IMAGE_DIR} -name ${PRODUCT}_*.dtb`
        echo -e "Try variants: " $SECOND_DTB
        if [ ! -e $SECOND_DTB ]; then
            echo -e "\n ERROR - No dtb found in the path" $SECOND_DTB
            exit 1
        fi
    fi

    echo -e "\n>>> Making global pac" ${GLOBAL_OUT}

    make_fake_dtbo_for_vbmeta

    SPECIFIED_PARTITION_PACK+=" --image dtb_a:${YOCTO_DTB}
                                --image kernel_a:${YOCTO_IMAGE}
                                --image rootfs_a:${YOCTO_ROOTFS_EXT4}
                                --image userdata:out/data.ext4
                                --image cluster_dtb_a:${SECOND_DTB}
                                --image cluster_bootloader_a:${SECOND_IMAGE_DIR}/ivi_bootloader.bin
                                --image cluster_kernel_a:${SECOND_IMAGE}
                                --image cluster_ramdisk_a:${SECOND_ROOTFS}
                                --image atf_a:${ATF_FILE}
                                --image bootloader_a:${YOCTO_IMAGE_DIR}/ivi_bootloader.bin
                                --version 2020W02 "

    VBMTEA_DESCIPTOR+=" ${YOCTO_DTB}:dtb:${HASH_ALG}:hash
                        ${YOCTO_IMAGE}:kernel:${HASH_ALG}:hash
                        ${SECOND_DTB}:cluster_dtb:${HASH_ALG}:hash
                        ${SECOND_IMAGE_DIR}/ivi_bootloader.bin:cluster_bootloader:${HASH_ALG}:hash
                        ${SECOND_IMAGE}:cluster_kernel:${HASH_ALG}:hash
                        ${SECOND_ROOTFS}:cluster_ramdisk:${HASH_ALG}:hash
                        ${ATF_FILE}:atf:${HASH_ALG}:hash
                        ${YOCTO_IMAGE_DIR}/ivi_bootloader.bin:bootloader:${HASH_ALG}:hash
                        ${YOCTO_ROOTFS_EXT4}:rootfs:${HASH_ALG}:hashtree"
fi

if [ "g9x_ref" == $PACKAGE -o "g9x_ii4" == $PACKAGE  -o "g9x_k1" == $PACKAGE ]; then
    BPT_ORIG_FILE=$YOCTO_MACHINE/G9-global$GLOBAL_BPT_SUFFIX.bpt
    BPT_OUT_FILE=out/G9_global_output.bpt
    BPT_OUT_IMAGE=out/G9_global_partition.img

    echo "making a ext4 spare data partition"
    dd if=/dev/zero of=out/data.ext4 bs=1024 count=131072
    mkfs.ext4 out/data.ext4

    make_fake_dtbo_for_vbmeta

    SPECIFIED_PARTITION_PACK+=" --image dtb_a:${YOCTO_DTB}
                                --image atf_a:${ATF_FILE}
                                --image bootloader_a:${YOCTO_IMAGE_DIR}/ivi_bootloader.bin
                                --image kernel_a:${YOCTO_IMAGE}
                                --image rootfs_a:${YOCTO_ROOTFS_EXT4}
                                --image userdata:out/data.ext4
                                --version 2020W02 "

    VBMTEA_DESCIPTOR+=" ${YOCTO_DTB}:dtb:${HASH_ALG}:hash
                        ${ATF_FILE}:atf:${HASH_ALG}:hash
                        ${YOCTO_IMAGE}:kernel:${HASH_ALG}:hash
                        ${YOCTO_IMAGE_DIR}/ivi_bootloader.bin:bootloader:${HASH_ALG}:hash
                        ${YOCTO_ROOTFS_EXT4}:rootfs:${HASH_ALG}:hashtree"
fi

if [ "g9q_ref" == $PACKAGE ]; then
    BPT_ORIG_FILE=$YOCTO_MACHINE/G9-global$GLOBAL_BPT_SUFFIX.bpt
    BPT_OUT_FILE=out/G9_global_output.bpt
    BPT_OUT_IMAGE=out/G9_global_partition.img

    echo "making a ext4 spare data partition"
    dd if=/dev/zero of=out/data.ext4 bs=1024 count=131072
    mkfs.ext4 out/data.ext4

    make_fake_dtbo_for_vbmeta

    SPECIFIED_PARTITION_PACK+=" --image dtb_a:${YOCTO_DTB}
                                --image atf_a:${ATF_FILE}
                                --image bootloader_a:${YOCTO_IMAGE_DIR}/ivi_bootloader.bin
                                --image kernel_a:${YOCTO_IMAGE}
                                --image rootfs_a:${YOCTO_ROOTFS_EXT4}
                                --image userdata:out/data.ext4
                                --version 2020W02 "

    VBMTEA_DESCIPTOR+=" ${YOCTO_DTB}:dtb:${HASH_ALG}:hash
                        ${ATF_FILE}:atf:${HASH_ALG}:hash
                        ${YOCTO_IMAGE}:kernel:${HASH_ALG}:hash
                        ${YOCTO_IMAGE_DIR}/ivi_bootloader.bin:bootloader:${HASH_ALG}:hash
                        ${YOCTO_ROOTFS_EXT4}:rootfs:${HASH_ALG}:hashtree"
fi

if [ "v9f_ref" == $PACKAGE -o "v9f_ref_7inch" == $PACKAGE ]; then
    SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/v9f/projects/default/system_config.bin
    if [ "v9f_ref_7inch" == $PACKAGE ]; then
        SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/v9f/projects/default/system_config_7inch.bin
    fi

    BPT_ORIG_FILE=$YOCTO_MACHINE/global$GLOBAL_BPT_SUFFIX.bpt
    BPT_OUT_FILE=out/v9_global_output.bpt
    BPT_OUT_IMAGE=out/v9_global_partition.img

    echo "making a ext4 spare data partition"
    dd if=/dev/zero of=out/data.ext4 bs=1024 count=131072
    mkfs.ext4 out/data.ext4

    make_fake_dtbo_for_vbmeta

    SPECIFIED_PARTITION_PACK+=" --image dtb_a:${YOCTO_DTB}
                                --image bootloader_a:${YOCTO_IMAGE_DIR}/ivi_bootloader.bin
                                --image kernel_a:${YOCTO_IMAGE}
                                --image rootfs_a:${YOCTO_ROOTFS_EXT4}
                                --image userdata:out/data.ext4
                                --version 2020W02 "

    VBMTEA_DESCIPTOR+=" ${YOCTO_DTB}:dtb:${HASH_ALG}:hash
                        ${YOCTO_IMAGE}:kernel:${HASH_ALG}:hash
                        ${YOCTO_IMAGE_DIR}/ivi_bootloader.bin:bootloader:${HASH_ALG}:hash
                        ${YOCTO_ROOTFS_EXT4}:rootfs:${HASH_ALG}:hashtree"
fi

if [ "v9ts_ref_a" == $PACKAGE -o "v9ts_ref_b" == $PACKAGE ]; then
    BPT_ORIG_FILE=$YOCTO_MACHINE/global$GLOBAL_BPT_SUFFIX.bpt
    BPT_OUT_FILE=out/v9ts_global_output.bpt
    BPT_OUT_IMAGE=out/v9ts_global_partition.img

    echo "making a ext4 spare data partition"
    dd if=/dev/zero of=out/data.ext4 bs=1024 count=131072
    mkfs.ext4 out/data.ext4

    make_fake_dtbo_for_vbmeta

    SPECIFIED_PARTITION_PACK+=" --image dtb_a:${YOCTO_DTB}
                                --image bootloader_a:${YOCTO_IMAGE_DIR}/ivi_bootloader.bin
                                --image kernel_a:${YOCTO_IMAGE}
                                --image rootfs_a:${YOCTO_ROOTFS_EXT4}
                                --image userdata:out/data.ext4
                                --version 2020W02 "

    VBMTEA_DESCIPTOR+=" ${YOCTO_DTB}:dtb:${HASH_ALG}:hash
                        ${YOCTO_IMAGE}:kernel:${HASH_ALG}:hash
                        ${YOCTO_IMAGE_DIR}/ivi_bootloader.bin:bootloader:${HASH_ALG}:hash
                        ${YOCTO_ROOTFS_EXT4}:rootfs:${HASH_ALG}:hashtree"
fi

if [ "v9t_a_ref" == $PACKAGE -o "v9t_b_ref" == $PACKAGE ]; then
    BPT_ORIG_FILE=$YOCTO_MACHINE/global$GLOBAL_BPT_SUFFIX.bpt
    BPT_OUT_FILE=out/v9t_global_output.bpt
    BPT_OUT_IMAGE=out/v9t_global_partition.img

    echo "making a ext4 spare data partition"
    dd if=/dev/zero of=out/data.ext4 bs=1024 count=131072
    mkfs.ext4 out/data.ext4

    make_fake_dtbo_for_vbmeta

    SPECIFIED_PARTITION_PACK+=" --image dtb_a:${YOCTO_DTB}
                                --image bootloader_a:${YOCTO_IMAGE_DIR}/ivi_bootloader.bin
                                --image kernel_a:${YOCTO_IMAGE}
                                --image rootfs_a:${YOCTO_ROOTFS_EXT4}
                                --image userdata:out/data.ext4
                                --version 2020W02 "

    VBMTEA_DESCIPTOR+=" ${YOCTO_DTB}:dtb:${HASH_ALG}:hash
                        ${YOCTO_IMAGE}:kernel:${HASH_ALG}:hash
                        ${YOCTO_IMAGE_DIR}/ivi_bootloader.bin:bootloader:${HASH_ALG}:hash
                        ${YOCTO_ROOTFS_EXT4}:rootfs:${HASH_ALG}:hashtree"
fi

if [ "x9h_ref" == $PACKAGE -o "x9h_ref_cluster" == $PACKAGE  -o "x9h_ref_cluster_serdes" == $PACKAGE \
        -o "x9h_ref_serdes" == $PACKAGE \
        -o "x9h_refa04_serdes" == $PACKAGE \
        -o "x9h_ref_controlpanel" == $PACKAGE \
        -o "x9h_ref_controlpanel_serdes" == $PACKAGE \
        -o "x9h_ref_bt" == $PACKAGE \
        -o "x9h_icl02" == $PACKAGE \
        -o "x9h_ms" == $PACKAGE \
        -o "x9h_classic" == $PACKAGE -o "x9h_iot" == $PACKAGE -o "x9h_iot_ubuntu12g" == $PACKAGE -o "x9h_iot_ubuntu6g" == $PACKAGE ]; then

    SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/x9_high/projects/default/system_config.bin

    if [ "x9h_ref_cluster_serdes" == $YOCTO_MACHINE -o "x9h_ref_controlpanel_serdes" == $YOCTO_MACHINE \
           -o  "x9h_ref_serdes" == $YOCTO_MACHINE -o  "x9h_refa04_serdes" == $YOCTO_MACHINE ]; then
        SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/x9_high/projects/controlpanel/system_config.bin
    fi

    if [ "x9h_classic" == $YOCTO_MACHINE ]; then
        SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/x9_high/projects/classic/system_config.bin
    fi

    if [ "x9h_ms" == $YOCTO_MACHINE ]; then
        SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/x9_high/projects/ms_serdes/system_config.bin
    fi
    
    if [ "x9h_iot" == $PACKAGE -o "x9h_iot_ubuntu12g" == $PACKAGE -o "x9h_iot_ubuntu6g" == $PACKAGE ]; then
		echo "SYSTEM_CONFIG_BIN use lk_customize/chipcfg/generate/x9_high/projects/iot/system_config.bin!"
        SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/x9_high/projects/iot/system_config.bin
    fi
    
    if [ "x9h_icl02" == $YOCTO_MACHINE ]; then
        SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/x9_high/projects/icl02/system_config.bin
    fi

    BPT_ORIG_FILE=$YOCTO_MACHINE/global$GLOBAL_BPT_SUFFIX.bpt
    BPT_OUT_FILE=out/GPT_global_output.bpt
    BPT_OUT_IMAGE=out/GPT_global_partition.img

    echo "making a ext4 spare data partition"
    dd if=/dev/zero of=out/data.ext4 bs=1024 count=131072
    mkfs.ext4 out/data.ext4

    make_fake_dtbo_for_vbmeta

	if [ "x9h_iot_ubuntu12g" == $PACKAGE -o "x9h_iot_ubuntu6g" == $PACKAGE ]; then
		SPECIFIED_PARTITION_PACK+=" --image dtb_a:${YOCTO_DTB}
                                --image kernel_a:${YOCTO_IMAGE}
                                --image rootfs:${YOCTO_ROOTFS_EXT4}
                                --image userdata:out/data.ext4
                                --image atf_a:${ATF_FILE}
                                --image bootloader_a:${YOCTO_IMAGE_DIR}/ivi_bootloader.bin
                                --version 2020W02 "
	else
		SPECIFIED_PARTITION_PACK+=" --image dtb_a:${YOCTO_DTB}
                                --image kernel_a:${YOCTO_IMAGE}
                                --image rootfs_a:${YOCTO_ROOTFS_EXT4}
                                --image userdata:out/data.ext4
                                --image atf_a:${ATF_FILE}
                                --image bootloader_a:${YOCTO_IMAGE_DIR}/ivi_bootloader.bin
                                --version 2020W02 "
	fi							

    VBMTEA_DESCIPTOR+=" ${YOCTO_DTB}:dtb:${HASH_ALG}:hash
                        ${YOCTO_IMAGE}:kernel:${HASH_ALG}:hash
                        ${ATF_FILE}:atf:${HASH_ALG}:hash
                        ${YOCTO_IMAGE_DIR}/ivi_bootloader.bin:bootloader:${HASH_ALG}:hash
                        ${YOCTO_ROOTFS_EXT4}:rootfs:${HASH_ALG}:hashtree"
fi

if [ "x9m_ref_serdes" == $PACKAGE -o "x9m_refa04_serdes" == $PACKAGE -o "x9m_ref" == $PACKAGE -o "x9m_ms" == $PACKAGE -o "x9m_yuenki" == $PACKAGE -o "x9m_iot" == $PACKAGE  -o "x9m_iot_ubuntu12g" == $PACKAGE -o "x9m_iot_ubuntu6g" == $PACKAGE ]; then
    SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/x9_mid/projects/serdes/system_config.bin

    if [ "x9m_ref" == $PACKAGE  ]; then
        SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/x9_mid/projects/default/system_config.bin
    fi

    if [ "x9m_ms" == $PACKAGE  ]; then
        SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/x9_mid/projects/ms_serdes/system_config.bin
    fi
    
    if [ "x9m_yuenki" == $PACKAGE ]; then
        SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/x9_mid/projects/yuenki/system_config.bin
    fi
	
	if [ "x9m_iot" == $PACKAGE -o "x9m_iot_ubuntu12g" == $PACKAGE -o "x9m_iot_ubuntu6g" == $PACKAGE ]; then
        SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/x9_mid/projects/iot/system_config.bin
    fi

    BPT_ORIG_FILE=$YOCTO_MACHINE/global$GLOBAL_BPT_SUFFIX.bpt
    BPT_OUT_FILE=out/GPT_global_output.bpt
    BPT_OUT_IMAGE=out/GPT_global_partition.img

    echo "making a ext4 spare data partition"
    dd if=/dev/zero of=out/data.ext4 bs=1024 count=131072
    mkfs.ext4 out/data.ext4

    make_fake_dtbo_for_vbmeta

	if [ "x9m_iot_ubuntu12g" == $PACKAGE -o "x9m_iot_ubuntu6g" == $PACKAGE ]; then
		SPECIFIED_PARTITION_PACK+=" --image dtb_a:${YOCTO_DTB}
                                --image kernel_a:${YOCTO_IMAGE}
                                --image rootfs:${YOCTO_ROOTFS_EXT4}
                                --image userdata:out/data.ext4
                                --image atf_a:${ATF_FILE}
                                --image bootloader_a:${YOCTO_IMAGE_DIR}/ivi_bootloader.bin
                                --version 2020W02 "
	else
		SPECIFIED_PARTITION_PACK+=" --image kernel_a:${YOCTO_IMAGE}
                                --image dtb_a:${YOCTO_DTB}
                                --image rootfs_a:${YOCTO_ROOTFS_EXT4}
                                --image userdata:out/data.ext4
                                --image atf_a:${ATF_FILE}
                                --image bootloader_a:${YOCTO_IMAGE_DIR}/ivi_bootloader.bin
                                --version 2021W02 "
	fi

    VBMTEA_DESCIPTOR+=" ${YOCTO_DTB}:dtb:${HASH_ALG}:hash
                        ${YOCTO_IMAGE}:kernel:${HASH_ALG}:hash
                        ${ATF_FILE}:atf:${HASH_ALG}:hash
                        ${YOCTO_IMAGE_DIR}/ivi_bootloader.bin:bootloader:${HASH_ALG}:hash
                        ${YOCTO_ROOTFS_EXT4}:rootfs:${HASH_ALG}:hashtree"
fi

if [ "d9_ref" == $PACKAGE ]; then
    SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/d9/projects/default/system_config.bin
    BPT_ORIG_FILE=$YOCTO_MACHINE/global$GLOBAL_BPT_SUFFIX.bpt
    BPT_OUT_FILE=out/GPT_global_output.bpt
    BPT_OUT_IMAGE=out/GPT_global_partition.img

    echo "making a ext4 spare data partition"
    dd if=/dev/zero of=out/data.ext4 bs=1024 count=131072
    mkfs.ext4 out/data.ext4

    make_fake_dtbo_for_vbmeta

    SPECIFIED_PARTITION_PACK+=" --image kernel_a:${YOCTO_IMAGE}
                                --image dtb_a:${YOCTO_DTB}
                                --image rootfs_a:${YOCTO_ROOTFS_EXT4}
                                --image userdata:out/data.ext4
                                --image atf_a:${ATF_FILE}
                                --image bootloader_a:${YOCTO_IMAGE_DIR}/ivi_bootloader.bin
                                --version 2021W02 "

    VBMTEA_DESCIPTOR+=" ${YOCTO_DTB}:dtb:${HASH_ALG}:hash
                        ${YOCTO_IMAGE}:kernel:${HASH_ALG}:hash
                        ${ATF_FILE}:atf:${HASH_ALG}:hash
                        ${YOCTO_IMAGE_DIR}/ivi_bootloader.bin:bootloader:${HASH_ALG}:hash
                        ${YOCTO_ROOTFS_EXT4}:rootfs:${HASH_ALG}:hashtree"
fi

if [ "x9e_ref" == $PACKAGE -o "x9e_ms" == $PACKAGE ]; then
    SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/x9_eco/projects/serdes/system_config.bin
    if [[ "x9e_ms" == $PACKAGE ]];then
        SYSTEM_CONFIG_BIN=${YOCTO_TOP}/source/lk_customize/chipcfg/generate/x9_eco/projects/ms_serdes/system_config.bin
    fi

    BPT_ORIG_FILE=$YOCTO_MACHINE/global$GLOBAL_BPT_SUFFIX.bpt
    BPT_OUT_FILE=out/GPT_global_output.bpt
    BPT_OUT_IMAGE=out/GPT_global_partition.img

    echo "making a ext4 spare data partition"
    dd if=/dev/zero of=out/data.ext4 bs=1024 count=131072
    mkfs.ext4 out/data.ext4

    make_fake_dtbo_for_vbmeta

    SPECIFIED_PARTITION_PACK+=" --image kernel_a:${YOCTO_IMAGE}
                                --image dtb_a:${YOCTO_DTB}
                                --image rootfs_a:${YOCTO_ROOTFS_EXT4}
                                --image userdata:out/data.ext4
                                --image atf_a:${ATF_FILE}
                                --image bootloader_a:${YOCTO_IMAGE_DIR}/ivi_bootloader.bin
                                --version 2021W02 "

    VBMTEA_DESCIPTOR+=" ${YOCTO_DTB}:dtb:${HASH_ALG}:hash
                        ${YOCTO_IMAGE}:kernel:${HASH_ALG}:hash
                        ${ATF_FILE}:atf:${HASH_ALG}:hash
                        ${YOCTO_IMAGE_DIR}/ivi_bootloader.bin:bootloader:${HASH_ALG}:hash
                        ${YOCTO_ROOTFS_EXT4}:rootfs:${HASH_ALG}:hashtree"
fi

#sign dloader
image_add_hash_footer_no_bpt "${YOCTO_IMAGE_DIR}/dloader.bin" "dloader" "262144" "${HASH_ALG}" "${VBMETA_SIGNED_ARGS}"

if [ x$GLOBAL_BPT_SUFFIX != x"" ];then

    RES_IMAGE_SIZE=$(get_image_max_size "${BPT_ORIG_FILE}" "res" "hash" )
    RES_IMAGE_SIZE=$((RES_IMAGE_SIZE/1048576))

    mkdir -p ${YOCTO_IMAGE_DIR}/res_img/early_app
    cp -r ../../source/lk_safety/res/early_app/BootAnimation ${YOCTO_IMAGE_DIR}/res_img/early_app/
    ./dir2fat.sh -f -F 16 -S 512 ${YOCTO_IMAGE_DIR}/fat_res.img ${RES_IMAGE_SIZE} ${YOCTO_IMAGE_DIR}/res_img;

    ./sign_tool/run_sign_sec -i ${YOCTO_IMAGE_DIR}/dil.bin -l 0x1C0000 -e 0x1C0000 -k ${ATB_SIGN_KEY}
    ./sign_tool/run_sign_sec -i ${YOCTO_IMAGE_DIR}/ssystem.bin -l 0x140000 -e 0x140000 -k ${ATB_SIGN_KEY}
    ./sign_tool/run_sign_safe -i ${YOCTO_IMAGE_DIR}/dil2-unsigned.bin -l 0x100000 -e 0x100000 -k ${ATB_SIGN_KEY}
    mv ${YOCTO_IMAGE_DIR}/dil2-unsigned.bin.safe.signed ${YOCTO_IMAGE_DIR}/dil2.bin

    UNIFIED_BOOT_EXTRA+=" --preload spl:${YOCTO_IMAGE_DIR}/dil.bin.sec.signed
                          --image dil2_a:${YOCTO_IMAGE_DIR}/dil2.bin
                          --image safety_os_a:${YOCTO_IMAGE_DIR}/safety.bin
                          --image preloader_a:${YOCTO_IMAGE_DIR}/preloader.bin
                          --image ddr_fw_a:${YOCTO_IMAGE_DIR}/ddr_fw.bin
                          --image ddr_init_seq_a:${YOCTO_IMAGE_DIR}/ddr_init_seq.bin
                          --image ssystem_a:${YOCTO_IMAGE_DIR}/ssystem.bin.sec.signed
                          --image res_a:${YOCTO_IMAGE_DIR}/fat_res.img "

    VBMTEA_DESCIPTOR+=" ${YOCTO_IMAGE_DIR}/dil2.bin:dil2:${HASH_ALG}:hash
                        ${YOCTO_IMAGE_DIR}/ddr_init_seq.bin:ddr_init_seq:${HASH_ALG}:hash
                        ${YOCTO_IMAGE_DIR}/ddr_fw.bin:ddr_fw:${HASH_ALG}:hash
                        ${YOCTO_IMAGE_DIR}/preloader.bin:preloader:${HASH_ALG}:hash
                        ${YOCTO_IMAGE_DIR}/safety.bin:safety_os:${HASH_ALG}:hash"
    # for ssystem warm boot, use avb2.0 to verify ssystem image, so add its info to vbmeta
    VBMTEA_DESCIPTOR+=" ${YOCTO_IMAGE_DIR}/ssystem.bin.sec.signed:ssystem:${HASH_ALG}:hash"
    VBMTEA_DESCIPTOR+=" ${YOCTO_IMAGE_DIR}/fat_res.img:res:${HASH_ALG}:hash"

    SPECIFIED_PARTITION_PACK+=" --image system_config_a:${SYSTEM_CONFIG_BIN} "
    VBMTEA_DESCIPTOR+=" ${SYSTEM_CONFIG_BIN}:system_config:${HASH_ALG}:hash"

    if [ "x9m_ref_serdes" == $PACKAGE -o "x9m_refa04_serdes" == $PACKAGE -o "x9m_ref" == $PACKAGE ]; then
        UNIFIED_BOOT_EXTRA+=" --image fda_spl_a:${SIGNED_SPL} "
        VBMTEA_DESCIPTOR+=" ${SIGNED_SPL}:fda_spl:${HASH_ALG}:hash"
    fi
else
    UNIFIED_BOOT_EXTRA+=" --preload spl:$SIGNED_SPL "
fi

echo "BPT_ORIG_FILE:"${BPT_ORIG_FILE}

VBMETA_IMAGE_EMMC=${YOCTO_IMAGE_DIR}/vbmeta-emmc.img
VBMTEA_DESCIPTOR=$(images_add_footer ${BPT_ORIG_FILE} "${VBMTEA_DESCIPTOR}")
echo "VBMTEA_DESCIPTOR:"${VBMTEA_DESCIPTOR}


make_vbmeta_image "${VBMTEA_DESCIPTOR}" "${VBMETA_SIGNED_ARGS}" "${VBMETA_IMAGE_EMMC}" || exit 1
SPECIFIED_PARTITION_PACK+=" --image vbmeta_a:${VBMETA_IMAGE_EMMC} "

python ./bpttool make_table --input ${BPT_ORIG_FILE} --ab_suffixes "_a,_b" --output_json ${BPT_OUT_FILE} --output_gpt ${BPT_OUT_IMAGE}
test $? -eq 0 || exit 1

python ./pactool make_pac_image --output $GLOBAL_OUT --input ${BPT_OUT_FILE} \
        ${DA_COMMON_ARGS} \
        ${UNIFIED_BOOT_EXTRA} \
        ${SPECIFIED_PARTITION_PACK}
test $? -eq 0 || exit 1

./gen_pack_crc ${GLOBAL_OUT}

echo -e "\n File $GLOBAL_OUT is generated Size:" `stat -L -c %s $GLOBAL_OUT`
