#@TYPE: Machine
#@NAME: X9 machine
#@DESCRIPTION: Machine configuration for running X9

SOC_FAMILY = "x9high"
MACHAINE_NAME = "x9h_iot"
LK_PROJ_NAME = "x9h_iot"
SAFETY_PROJ_NAME = "x9h_iot"


DEFAULTTUNE ?= "cortexa57-cortexa53"
require conf/machine/include/tune-cortexa57-cortexa53.inc
require conf/machine/include/${SOC_FAMILY}.inc

# 32BIT package install (default is disable)
# This variables can be used only in multilib.
USE_32BIT_PKGS ?= "0"
USE_32BIT_WAYLAND ?= "0"
USE_32BIT_MMP ?= "0"

KBUILD_DEFCONFIG_${MACHAINE_NAME} = "x9_iot_linux_defconfig"
MACHINE_FEATURES = ""

require conf/machine/include/verified-boot.inc
KERNEL_IMAGETYPE = "Image"
IMAGE_FSTYPES_append = " ext4"
IMAGE_FSTYPES_remove = "tar.gz"
# use 256 MB according to bpt
IMAGE_ROOTFS_SIZE = "262144"

SERIAL_CONSOLE = "115200 ttyS0"

# Configuration for kernel
PREFERRED_PROVIDER_virtual/kernel = "linux-semidrive-dev"

KERNEL_DEVICETREE = "semidrive/x9_high_iot.dtb"
PREFERRED_PROVIDER_virtual/drm = "drm-dev"
PREFERRED_PROVIDER_virtual/g2d = "g2d-dev"
MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS = " kernel-module-g2d kernel-module-pvrsrvkm kernel-module-drm kernel-module-hci-uart kernel-module-crypto-engine kernel-module-ecdh-generic kernel-module-bluetooth kernel-module-brcmutil kernel-module-brcmfmac"
KERNEL_MODULE_AUTOLOAD = "pvrsrvkm kunlun-drm kunlun-drm-crtc kunlun-drm-lvds crypto_engine ecdh_generic bluetooth hci_uart brcmutil brcmfmac"
MACHINE_GPU_REV_${MACHAINE_NAME} = "gm9446"

# Configuration for ARM Trusted Firmware
#EXTRA_IMAGEDEPENDS += " arm-trusted-firmware"
# Configuration for bootloaders
EXTRA_IMAGEDEPENDS += "lk"
MACHINE_SPL_${MACHAINE_NAME} = "spl_${LK_PROJ_NAME}"
MACHINE_SPLARG_${MACHAINE_NAME} = ""
MACHINE_SSYSTEM_${MACHAINE_NAME} = "ssystem_${LK_PROJ_NAME}"
MACHINE_SSYSTEMARG_${MACHAINE_NAME} = "ENABLE_BT=false"
MACHINE_DLOADER_${MACHAINE_NAME} = "dloader_${LK_PROJ_NAME}"
MACHINE_PRELOADER_${MACHAINE_NAME} = "preloader_${LK_PROJ_NAME}"
MACHINE_BOOTLOADER_${MACHAINE_NAME} = "bootloader_${LK_PROJ_NAME}"
MACHINE_MP_${MACHAINE_NAME} = ""
MACHINE_BOOT_EXTRA_OPTION_${MACHAINE_NAME} = "BOOTDEVICE=EMMC"

# safety
EXTRA_IMAGEDEPENDS += "safety"
MACHINE_SAFETY_${MACHAINE_NAME} = "safety-${SAFETY_PROJ_NAME}"
MACHINE_SAFETYARG_x9h_ref_serdes = "ENABLE_BOOT_ANIMATION=true ENABLE_FASTAVM=false"
MACHINE_OSPIHANDOVER_${MACHAINE_NAME} = "ospi_handover-${SAFETY_PROJ_NAME}"

#
# u-boot

# graphics and drm
PREFERRED_PROVIDER_virtual/libgles1 = "pvr-lib"
PREFERRED_PROVIDER_virtual/libgles2 = "pvr-lib"
PREFERRED_PROVIDER_virtual/egl = "pvr-lib"
##MACHINE_EXTRA_RRECOMMENDS += "pvr-lib"
PREFERRED_VERSION_libdrm = "2.4.91"

PACKAGECONFIG_append_pn-qtbase = " examples fontconfig"

# Add variable to Build Configuration in build log
BUILDCFG_VARS_append = " SOC_FAMILY"

MACHINEOVERRIDES .= ":x9high"

DISTRO_FEATURES_append = " bluetooth bluez5 wpa-supplicant"
DISTRO_FEATURES_NATIVESDK_append = " wayland opengl"
IMAGE_INSTALL_append = " bluez5 sdrvbtwifi wpa-supplicant fuse-exfat exfat-utils i2c-tools-misc testtools lirc audio-ref alsa-lib alsa-utils quectelCM quectel-gobinet-mod brcm-firmware memtester"
# Command using to build kernel-module-gles
HOSTTOOLS += "sync seq"
#TOOLCHAIN_HOST_TASK_append = ' nativesdk-qtbase-tools '

IMAGE_INSTALL_append = " qt5everywheredemo qt5-demo-extrafiles"
#IMAGE_INSTALL_remove += "qtbase qtserialport qtdeclarative qtquickcontrols qtquickcontrols2 qtgraphicaleffects"
BAREMETAL_TGT = "sec"
PRELOAD_RES_SIZE = "0x1800000"
MACHINE_SAFETYARG_${MACHAINE_NAME} += "PRELOAD_RES_SIZE=${PRELOAD_RES_SIZE}"
MACHINE_DLOADERARG_${MACHAINE_NAME} += "BOOT_TYPE=emmc"
MACHINE_BAREMETALARG_${MACHAINE_NAME} = "BOARD=x9h_iot DDR_FW=lpddr4x DDR_TYPE=lpddr4x DDR_SIZE=2GB DDR_FREQ=3200  PRELOAD_RES_SIZE=${PRELOAD_RES_SIZE}"
