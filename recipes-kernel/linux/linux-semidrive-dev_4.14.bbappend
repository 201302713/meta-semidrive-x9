DESCRIPTION = "Linux kernel append for the Semidrive x9 based board"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}/:"

LINUXCUSTOMIZEDIR := "${THISDIR}/${PN}-${PV}"
TARGETDIR := "${TOPDIR}/../source/linux"

do_customize() {
	bbplain "linux-semidrive-dev_4.14.bbappend will copy defconfig files to ${TARGETDIR}/arch/arm64/configs"
	cp -f ${LINUXCUSTOMIZEDIR}/configs/* ${TARGETDIR}/arch/arm64/configs
	bbplain "linux-semidrive-dev_4.14.bbappend will copy dts files to ${TARGETDIR}/arch/arm64/boot/dts/semidrive"
	cp -f ${LINUXCUSTOMIZEDIR}/dts/* ${TARGETDIR}/arch/arm64/boot/dts/semidrive

	bbplain "linux-semidrive-dev_4.14.bbappend will add lan743x driver"
	cp -f ${LINUXCUSTOMIZEDIR}/drivers/net/ethernet/microchip/* ${TARGETDIR}/drivers/net/ethernet/microchip/
	bbplain "linux-semidrive-dev_4.14.bbappend check ethernet/microchip/Kconfig"
	if [ `grep -c "config\ LAN743X" ${TARGETDIR}/drivers/net/ethernet/microchip/Kconfig` -eq '1' ]; then
		bbnote "already added menuconfig for LAN743X!"
	else
		bbnote "will add menuconfig for LAN743X!"
		sed -i '/endif\ \#\ NET_VENDOR_MICROCHIP/i\config LAN743X \
       tristate "LAN743x support" \
       depends on PCI \
       select PHYLIB \
       select CRC16 \
       ---help--- \
         Support for the Microchip LAN743x PCI Express Gigabit Ethernet chip \
		\
         To compile this driver as a module, choose M here. The module will be \
         called lan743x. \
		' ${TARGETDIR}/drivers/net/ethernet/microchip/Kconfig
	fi
	if [ `grep -c "CONFIG_LAN743X" ${TARGETDIR}/drivers/net/ethernet/microchip/Makefile` -eq '1' ]; then
		bbnote "already modified Makefile for CONFIG_LAN743X!"
	else
		bbnote "will modify Makefile for CONFIG_LAN743X!"
		BUILD_LAN743X="obj-\$(CONFIG_LAN743X) += lan743x.o \nlan743x-objs := lan743x_main.o lan743x_ethtool.o lan743x_ptp.o\n"
		echo $BUILD_LAN743X >> ${TARGETDIR}/drivers/net/ethernet/microchip/Makefile
	fi
	
	bbplain "linux-semidrive-dev_4.14.bbappend will modify x9-ref-match-tlv320aic23 sound"
	cp -f ${LINUXCUSTOMIZEDIR}/sound/soc/semidrive/x9-ref-mach-tlv320aic23.c ${TARGETDIR}/sound/soc/semidrive/x9-ref-mach-tlv320aic23.c
	
	bbplain "linux-semidrive-dev_4.14.bbappend will do modify ${TARGETDIR}/drivers/net/phy/mv_switch.c for machine:${MACHAINE_NAME}!"
	if [ "${MACHAINE_NAME}" = "x9m_yuenki" ]; then
		if [ `grep -c 'phydev->speed = SPEED_100;'  ${TARGETDIR}/drivers/net/phy/mv_switch.c` -eq '1' ]; then
			bbnote "mv_switch.c already changed to SPEED_100!"
		else
			bbnote "will do modify for ${TARGETDIR}/drivers/net/phy/mv_switch.c change to SPEED_100!"
			sed -i 's/phydev->speed = SPEED_1000;/phydev->speed = SPEED_100;/' ${TARGETDIR}/drivers/net/phy/mv_switch.c
		fi
	elif [ "${MACHAINE_NAME}" = "x9h_iot"  -o "${MACHAINE_NAME}" = "x9m_iot" ]; then
		if [ `grep -c 'phydev->speed = SPEED_1000;'  ${TARGETDIR}/drivers/net/phy/mv_switch.c` -eq '1' ]; then
			bbnote "mv_switch.c already resumed to SPEED_1000!"
		else
			bbnote "will do resume for ${TARGETDIR}/drivers/net/phy/mv_switch.c change to SPEED_1000!"
			sed -i 's/phydev->speed = SPEED_100;/phydev->speed = SPEED_1000;/' ${TARGETDIR}/drivers/net/phy/mv_switch.c
		fi
	fi
	
#	bbplain "linux-semidrive-dev_4.14.bbappend will change KERNEL MEMORY as 2GB!"
#	if [ `grep -c "\#define\ ECO_REE_MEMSIZE\ 0x60000000" ${TARGETDIR}/include/dt-bindings/memmap/x9_high/projects/default/image_cfg.h` -eq '1' ]; then
#		bbnote "already changed ECO_REE_MEMSIZE as 0x60000000!"
#	else
#		bbnote "will changed ECO_REE_MEMSIZE as 0x60000000!"
#		sed -ie 's/define\ ECO_REE_MEMSIZE.*$/define\ ECO_REE_MEMSIZE\ 0x60000000/g' ${TARGETDIR}/include/dt-bindings/memmap/x9_high/projects/default/image_cfg.h
#	fi
#	if [ `grep -c "\#define\ AP1_KERNEL_MEMSIZE\ 0x60000000" ${TARGETDIR}/include/dt-bindings/memmap/x9_high/projects/default/image_cfg.h` -eq '1' ]; then
#		bbnote "already changed AP1_KERNEL_MEMSIZE as 0x60000000!"
#	else
#		bbnote "will changed AP1_KERNEL_MEMSIZE as 0x60000000!"
#		sed -ie 's/define\ AP1_KERNEL_MEMSIZE.*$/define\ AP1_KERNEL_MEMSIZE\ 0x60000000/g' ${TARGETDIR}/include/dt-bindings/memmap/x9_high/projects/default/image_cfg.h
#	fi
}
do_customize[nostamp] += "1"
addtask do_customize before do_configure

addtask showvars
do_showvars[nostamp] = "1"
python do_showvars(){
    # emit only the metadata that are variables and not functions
    isfunc = lambda key: bool(d.getVarFlag(key, 'func'))
    vars = sorted((key for key in bb.data.keys(d) \
        if not key.startswith('__')))
    for var in vars:
        if not isfunc(var):
            try:
                val = d.getVar(var, True)
            except Exception as exc:
                bb.plain('Expansion of %s threw %s: %s' % \
                    (var, exc.__class__.__name__, str(exc)))
            bb.plain('%s="%s"' % (var, val))
}
##can't use do_unpack_append on externalsrc 
